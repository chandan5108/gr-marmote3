INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_MARMOTE3 marmote3)

FIND_PATH(
    MARMOTE3_INCLUDE_DIRS
    NAMES marmote3/api.h
    HINTS $ENV{MARMOTE3_DIR}/include
        ${PC_MARMOTE3_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    MARMOTE3_LIBRARIES
    NAMES gnuradio-marmote3
    HINTS $ENV{MARMOTE3_DIR}/lib
        ${PC_MARMOTE3_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/marmote3Target.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MARMOTE3 DEFAULT_MSG MARMOTE3_LIBRARIES MARMOTE3_INCLUDE_DIRS)
MARK_AS_ADVANCED(MARMOTE3_LIBRARIES MARMOTE3_INCLUDE_DIRS)
