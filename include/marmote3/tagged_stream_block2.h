/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_TAGGED_STREAM_BLOCK2_H
#define INCLUDED_MARMOTE3_TAGGED_STREAM_BLOCK2_H

#include <gnuradio/block.h>
#include <gnuradio/gr_complex.h>
#include <marmote3/api.h>

namespace gr {
namespace marmote3 {

#ifndef SWIG

/*!
 * \brief
 * This block replaces the tagged_stream_block. It handles whole packets
 * at a time, each marked with a PACKET_DICT dictionary that must contain the
 * PACKET_LEN entry and possibly others. You should implement the work()
 * function. The PACKET_LEN tag is automatically updated (by the return value of
 * work), and all tags are propagated.
 *
 * \ingroup marmote3
 */
class MARMOTE3_API tagged_stream_block2 : public gr::block {
protected:
  const pmt::pmt_t block_name_sym;
  const int max_output_len;

  std::vector<int> input_lengths;
  std::vector<pmt::pmt_t> input_dicts;
  pmt::pmt_t output_dict;

protected:
  // allows pure virtual interface sub-classes
  tagged_stream_block2(void) : max_output_len(0) {}
  tagged_stream_block2(const std::string &name,
                       gr::io_signature::sptr input_signature,
                       gr::io_signature::sptr output_signature,
                       int max_output_len);

  // reads input dictionaries
  bool has_input_long(int input, const pmt::pmt_t &key);
  long get_input_long(int input, const pmt::pmt_t &key, long val);
  bool has_input_float(int input, const pmt::pmt_t &key);
  float get_input_float(int input, const pmt::pmt_t &key, float val);

  // writes output dictionary
  void set_output_long(const pmt::pmt_t &key, long val);
  void set_output_float(const pmt::pmt_t &key, float val);
  void set_output_complex_vector(const pmt::pmt_t &key,
                                 const std::vector<gr_complex> &val);

  void produce_manually(int output, int produced);
  void print_input_dict(int input);

public:
  void forecast(int noutput_items, gr_vector_int &ninput_items_required);

  int general_work(int noutput_items, gr_vector_int &ninput_items,
                   gr_vector_const_void_star &input_items,
                   gr_vector_void_star &output_items);

  // you have to implement this
  virtual int work(int noutput_items, gr_vector_int &ninput_items,
                   gr_vector_const_void_star &input_items,
                   gr_vector_void_star &output_items) = 0;

  bool start();
};

#else

class MARMOTE3_API tagged_stream_block2 : public gr::block {
protected:
  tagged_stream_block2(const std::string &name,
                       gr::io_signature::sptr input_signature,
                       gr::io_signature::sptr output_signature,
                       const std::string &length_tag_key);
};

#endif

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_TAGGED_STREAM_BLOCK2_H */
