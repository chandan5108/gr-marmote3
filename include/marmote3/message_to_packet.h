/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_MESSAGE_TO_PACKET_H
#define INCLUDED_MARMOTE3_MESSAGE_TO_PACKET_H

#include <marmote3/api.h>
#include <marmote3/tagged_stream_block2.h>

namespace gr {
namespace marmote3 {

/*!
 * \brief <+description of block+>
 * \ingroup marmote3
 */
class MARMOTE3_API message_to_packet : virtual public tagged_stream_block2 {
public:
  typedef boost::shared_ptr<message_to_packet> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of
   * marmote3::message_to_packet.
   *
   * To avoid accidental use of raw pointers, marmote3::message_to_packet's
   * constructor is in a private implementation
   * class. marmote3::message_to_packet::make is the public interface for
   * creating new instances.
   */
  static sptr make(int itemsize, int max_packet_len, int max_queue_size);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_MESSAGE_TO_PACKET_H */
