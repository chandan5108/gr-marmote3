/* -*- c++ -*- */
/*
 * Copyright 2013-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_FEC_RA_XCODER_H
#define INCLUDED_MARMOTE3_FEC_RA_XCODER_H

#include <emmintrin.h>
#include <marmote3/api.h>
#include <memory>
#include <random>
#include <vector>

namespace gr {
namespace marmote3 {

/*!
 *\brief <+description of block+>
 *\ingroup marmote3
 */
class MARMOTE3_API fec_ra_permutation {
public:
  // returns four data_len2 size permutations packed
  static std::vector<int> generate_permutation(int data_len2);

  // returns code_len2 sized vector of ints between 0 and 4 * data_len2 - 1
  static std::vector<int> generate_puncturing(int data_len2, int code_len2);

  // returns 4 * data_len2 sized vector of ints between -1 and code_len2 - 1
  static std::vector<int> generate_punc_inv(int data_len2, int code_len2);

private:
  const int data_len2;
  const int repeat;

  std::mt19937 random;
  typedef std::vector<int> intvec_t;
  std::vector<intvec_t> perm;
  std::vector<intvec_t> invs;

public:
  fec_ra_permutation(int data_len2, int repeat);
  bool generate(int min_dist, int max_iteration);
  std::vector<int> get_permutation() const;

private:
  int get_random_int(int mod);
  int get_unused_index(int block, int unused_count);
  void generate_random();
  int get_modulo_distance(int a, int b);
  bool is_min_distance_violated(int min_dist, int block, int j1, int j2);
  int improve(int min_dist);
};

/*!
 *\brief <+description of block+>
 *\ingroup marmote3
 */
class MARMOTE3_API fec_ra_encoder {
private:
  const int data_len;
  const int code_len;
  const int data_len2; // size in words
  const int code_len2; // size in words
  const std::vector<int> perm;
  const std::vector<int> punc_inv;

public:
  fec_ra_encoder(int data_len, int code_len);

  // returns the number of data bytes
  int get_data_len() const { return data_len; }

  // returns the number of code bytes
  int get_code_len() const { return code_len; }

  // converts data bytes to code bytes
  void encode(const uint8_t *data_bytes, uint8_t *code_bytes) const;
  std::vector<uint8_t> encode(const std::vector<uint8_t> &data_bytes) const;
};

class MARMOTE3_API fec_ra_decoder {
private:
  const int data_len;
  const int code_len;
  const int data_len2; // size in words
  const int code_len2; // size in words
  const int full_passes;
  const int half_passes;
  const std::vector<int> perm;
  const std::vector<int> punc_inv;

  __m128i *data; // data_len2 times 16 bytes (soft bits)
  __m128i *code; // 4 * data_len2 times 16 bytes (soft bits)
  __m128i *sums; // data_len2 times 16 bytes (soft bits)

private:
  fec_ra_decoder(const fec_ra_decoder &other); // unimplemented

public:
  fec_ra_decoder(int data_len, int code_len, int full_passes, int half_passes);
  ~fec_ra_decoder();

  // returns the number of data bytes
  int get_data_len() const { return data_len; }

  // returns the number of code bytes
  int get_code_len() const { return code_len; }

  // converts soft bits to data bytes, bits are LSB, negative soft bit = bit set
  void decode(const int8_t *code_bits, uint8_t *data_bytes);
  std::vector<uint8_t> decode(const std::vector<int8_t> &code_bits);

protected:
  void load_punctured_code(const uint64_t *soft_bits);
  void store_packed_data(uint8_t *data_bytes);
  void full_pass_block(const int *perm_block, const __m128i *code_block);
  void half_pass_block(const int *perm_block, const __m128i *code_block);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_RA_ENCODER_H */
