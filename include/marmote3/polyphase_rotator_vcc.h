/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_POLYPHASE_ROTATOR_VCC_H
#define INCLUDED_MARMOTE3_POLYPHASE_ROTATOR_VCC_H

#include <gnuradio/sync_block.h>
#include <marmote3/api.h>

namespace gr {
namespace marmote3 {

/*!
 * \brief <+description of block+>
 * \ingroup marmote3
 *
 */
class MARMOTE3_API polyphase_rotator_vcc : virtual public gr::sync_block {
public:
  typedef boost::shared_ptr<polyphase_rotator_vcc> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of
   * marmote3::polyphase_rotator_vcc.
   *
   * To avoid accidental use of raw pointers, marmote3::polyphase_rotator_vcc's
   * constructor is in a private implementation
   * class. marmote3::polyphase_rotator_vcc::make is the public interface for
   * creating new instances.
   */
  static sptr make(int vlen, const std::vector<int> &map, int stride);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_POLYPHASE_ROTATOR_VCC_H */
