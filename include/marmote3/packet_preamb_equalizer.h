/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PACKET_PREAMB_EQUALIZER_H
#define INCLUDED_MARMOTE3_PACKET_PREAMB_EQUALIZER_H

#include <marmote3/api.h>
#include <marmote3/constants.h>
#include <marmote3/modem_monitor.h>
#include <marmote3/tagged_stream_block2.h>

namespace gr {
namespace marmote3 {

/*!
 * \brief <+description of block+>
 * \ingroup marmote3
 *
 */
class MARMOTE3_API packet_preamb_equalizer
    : virtual public tagged_stream_block2,
      virtual public modem_monitored {
public:
  typedef boost::shared_ptr<packet_preamb_equalizer> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of
   * marmote3::packet_preamb_equalizer.
   *
   * To avoid accidental use of raw pointers,
   * marmote3::packet_preamb_equalizer's
   * constructor is in a private implementation
   * class. marmote3::packet_preamb_equalizer::make is the public interface for
   * creating new instances.
   */
  static sptr make(const std::vector<gr_complex> &preamble, int samp_per_symb,
                   int padding_len, header_t header, bool postamble,
                   int max_output_len, float energy_limit, bool save_taps,
                   bool frame_debug);

  gr::block *get_this_block() override { return this; }
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_PREAMB_EQUALIZER_H */
