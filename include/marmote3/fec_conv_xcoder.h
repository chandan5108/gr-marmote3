/* -*- c++ -*- */
/*
 * Copyright 2013-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_FEC_CONV_XCODER_H
#define INCLUDED_MARMOTE3_FEC_CONV_XCODER_H

#include <marmote3/api.h>
#include <memory>
#include <vector>

namespace gr {
namespace marmote3 {

/*!
 *\brief <+description of block+>
 *\ingroup marmote3
 */
class MARMOTE3_API fec_conv_encoder {
private:
  const std::vector<int> polys;
  const int k; // constraint length

public:
  static std::vector<int> get_standard_polys(int rate, int k);

public:
  fec_conv_encoder(const std::vector<int> &polys, int k);

  // returns the number of data bits from the number of code bits
  int get_data_len(int code_len) const;

  // returns the number of code bits from the number of data bits
  int get_code_len(int data_len) const;

  // converts data bits to code bits
  void encode(const uint8_t *data_bits, int data_len, uint8_t *code_bits) const;
  std::vector<uint8_t> encode(const std::vector<uint8_t> &data_bits) const;

  // tailbiting versions of the previous methods
  int get_data_len_tb(int code_len) const;
  int get_code_len_tb(int data_len) const;
  void encode_tb(const uint8_t *data_bits, int data_len, uint8_t *code_bits) const;
  std::vector<uint8_t> encode_tb(const std::vector<uint8_t> &data_bits) const;
};

/*!
 *\brief <+description of block+>
 *\ingroup marmote3
 */
class MARMOTE3_API fec_conv_decoder {
private:
  const int rate;
  const int k;         // constraint length
  const int numstates; // 1 << (k - 1)
  const int max_data_len;

  float *metrics1;    // 16-byte aligned
  float *metrics2;    // 16-byte aligned
  float *branchtab;   // 16-byte aligned
  uint8_t *decisions; // 8-byte aligned

  float *old_metrics; // metrics1 or metrics2
  float *new_metrics; // metrics2 or metrics1

private:
  fec_conv_decoder(const fec_conv_decoder &other); // unimplemented

public:
  fec_conv_decoder(const std::vector<int> &polys, int k, int max_data_len);
  ~fec_conv_decoder();

  int get_max_data_len() const { return max_data_len; }

  // returns the number of code bits from the number of data bits
  int get_code_len(int data_len) const;

  // returns the number of data bits from the number of code bits
  int get_data_len(int code_len) const;

  // converts soft bits to data bits, negative soft bit = bit set
  void decode(const float *code_bits, int data_len, uint8_t *data_bits);
  std::vector<uint8_t> decode(const std::vector<float> &code_bits);

  // tailbiting version of the previous methods
  int get_code_len_tb(int data_len) const;
  int get_data_len_tb(int code_len) const;
  void decode_tb(const float *code_bits, int data_len, uint8_t *data_bits);
  std::vector<uint8_t> decode_tb(const std::vector<float> &code_bits);

protected:
  void butterfly(const float *symbols, uint8_t *cur_decision);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_CONV_ENCODER_H */
