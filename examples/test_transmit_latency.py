#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Test Transmit Latency
# GNU Radio version: 3.8.2.0

from gnuradio import blocks
from gnuradio import fft
from gnuradio.fft import window
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import marmote3
import math
import numpy as np
import random


class test_transmit_latency(gr.top_block):

    def __init__(self, ampl=0.3, freq=1e9, samp_rate=25000000):
        gr.top_block.__init__(self, "Test Transmit Latency")

        ##################################################
        # Parameters
        ##################################################
        self.ampl = ampl
        self.freq = freq
        self.samp_rate = samp_rate

        ##################################################
        # Variables
        ##################################################
        self.data_len = data_len = 1514
        self.chirp_len = chirp_len = 128
        self.padding_len = padding_len = 8
        self.code_len = code_len = data_len * 3 // 2 - 53
        self.chirp = chirp = marmote3.get_chirp_taps(chirp_len)
        self.preamble = preamble = np.concatenate((chirp[-padding_len:],chirp,chirp[:padding_len]))
        self.payload_len = payload_len = 8 * code_len // 2
        self.subcarrier_map = subcarrier_map = [0,1,2,3,4,5,6,7,12,13,14,15,16,17,18,19]
        self.packet_len = packet_len = len(preamble) + payload_len
        self.fbmc_tx_stride = fbmc_tx_stride = 22
        self.fbmc_fft_len = fbmc_fft_len = 20
        self.subcarrier_inv = subcarrier_inv = [subcarrier_map.index(x) if x in subcarrier_map else -1 for x in range(fbmc_fft_len)]
        self.packet_time = packet_time = float(packet_len) / (samp_rate / fbmc_tx_stride)
        self.num_channels = num_channels = len(subcarrier_map)
        self.fbmc_tx_taps = fbmc_tx_taps = marmote3.get_fbmc_taps4(fbmc_fft_len, fbmc_tx_stride, "tx")
        self.fbmc_rx_taps = fbmc_rx_taps = marmote3.get_fbmc_taps4(fbmc_fft_len, fbmc_tx_stride, "rx")
        self.fbmc_rx_stride = fbmc_rx_stride = fbmc_tx_stride/2
        self.chirp_fft_len = chirp_fft_len = 32
        self.active_channels = active_channels = [0,1,3] if True else random.sample(range(subcarriers), 1)

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_test_message_source_0 = marmote3.test_message_source(data_len - 14, data_len - 14, 1, 20000, 200, 1, 101, 102)
        self.marmote3_test_message_source_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_subcarrier_remapper_0 = marmote3.subcarrier_remapper(num_channels, subcarrier_inv, 0)
        self.marmote3_subcarrier_allocator_0 = marmote3.subcarrier_allocator(num_channels, active_channels, [], 1, getattr(self, '') if '' else marmote3.vector_peak_probe_sptr(), subcarrier_map, getattr(self, '') if '' else marmote3.modem_sender2_sptr(), False)
        self.marmote3_subcarrier_allocator_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_polyphase_synt_filter_ccf_0 = marmote3.polyphase_synt_filter_ccf(fbmc_fft_len, fbmc_tx_taps, fbmc_tx_stride or fbmc_fft_len)
        self.marmote3_polyphase_synt_filter_ccf_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_polyphase_rotator_vcc_0 = marmote3.polyphase_rotator_vcc(fbmc_fft_len, subcarrier_map, fbmc_tx_stride)
        self.marmote3_periodic_multiply_const_cc_1 = marmote3.periodic_multiply_const_cc(1, ampl * np.exp(1.0j * np.pi / fbmc_fft_len * np.arange(0, 2 * fbmc_fft_len)))
        self.marmote3_packet_symb_mod_0 = marmote3.packet_symb_mod(marmote3.MOD_QPSK or 1, payload_len)
        self.marmote3_packet_ra_encoder_0 = marmote3.packet_ra_encoder(-1 if False else data_len, code_len)
        self.marmote3_packet_preamb_insert_0 = marmote3.packet_preamb_insert(preamble, False, 0.0, marmote3.HDR_NONE, False, packet_len)
        self.marmote3_packet_debug_0_0_0_0 = marmote3.packet_debug(gr.sizeof_gr_complex, 2, 'packet_id', 100, 10)
        self.marmote3_packet_debug_0_0_0 = marmote3.packet_debug(gr.sizeof_gr_complex, 2, 'packet_id', 100, 10)
        self.marmote3_packet_debug_0_0 = marmote3.packet_debug(gr.sizeof_char, 2, 'packet_id', 100, 10)
        self.marmote3_packet_debug_0 = marmote3.packet_debug(gr.sizeof_char, 2, 'packet_id', 100, 10)
        self.marmote3_message_to_packet_0 = marmote3.message_to_packet(gr.sizeof_char, data_len, 0)
        self.fft_vxx_0 = fft.fft_vcc(fbmc_fft_len, False, window.rectangular(fbmc_fft_len), False, 1)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_probe_rate_0 = blocks.probe_rate(gr.sizeof_gr_complex*1, 1000.0, 0.15)
        self.blocks_message_debug_0 = blocks.message_debug()



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_probe_rate_0, 'rate'), (self.blocks_message_debug_0, 'print'))
        self.msg_connect((self.marmote3_test_message_source_0, 'out'), (self.marmote3_message_to_packet_0, 'in'))
        self.connect((self.blocks_throttle_0, 0), (self.blocks_probe_rate_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.marmote3_polyphase_synt_filter_ccf_0, 0))
        self.connect((self.marmote3_message_to_packet_0, 0), (self.marmote3_packet_debug_0, 0))
        self.connect((self.marmote3_message_to_packet_0, 0), (self.marmote3_packet_ra_encoder_0, 0))
        self.connect((self.marmote3_packet_preamb_insert_0, 0), (self.marmote3_packet_debug_0_0_0_0, 0))
        self.connect((self.marmote3_packet_preamb_insert_0, 0), (self.marmote3_subcarrier_allocator_0, 0))
        self.connect((self.marmote3_packet_ra_encoder_0, 0), (self.marmote3_packet_debug_0_0, 0))
        self.connect((self.marmote3_packet_ra_encoder_0, 0), (self.marmote3_packet_symb_mod_0, 0))
        self.connect((self.marmote3_packet_symb_mod_0, 0), (self.marmote3_packet_debug_0_0_0, 0))
        self.connect((self.marmote3_packet_symb_mod_0, 0), (self.marmote3_packet_preamb_insert_0, 0))
        self.connect((self.marmote3_periodic_multiply_const_cc_1, 0), (self.blocks_throttle_0, 0))
        self.connect((self.marmote3_polyphase_rotator_vcc_0, 0), (self.marmote3_subcarrier_remapper_0, 0))
        self.connect((self.marmote3_polyphase_synt_filter_ccf_0, 0), (self.marmote3_periodic_multiply_const_cc_1, 0))
        self.connect((self.marmote3_subcarrier_allocator_0, 0), (self.marmote3_polyphase_rotator_vcc_0, 0))
        self.connect((self.marmote3_subcarrier_remapper_0, 0), (self.fft_vxx_0, 0))


    def get_ampl(self):
        return self.ampl

    def set_ampl(self, ampl):
        self.ampl = ampl
        self.marmote3_periodic_multiply_const_cc_1.set_vals(self.ampl * np.exp(1.0j * np.pi / self.fbmc_fft_len * np.arange(0, 2 * self.fbmc_fft_len)))

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_packet_time(float(self.packet_len) / (self.samp_rate / self.fbmc_tx_stride))
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_data_len(self):
        return self.data_len

    def set_data_len(self, data_len):
        self.data_len = data_len
        self.set_code_len(self.data_len * 3 // 2 - 53)

    def get_chirp_len(self):
        return self.chirp_len

    def set_chirp_len(self, chirp_len):
        self.chirp_len = chirp_len
        self.set_chirp(marmote3.get_chirp_taps(self.chirp_len))

    def get_padding_len(self):
        return self.padding_len

    def set_padding_len(self, padding_len):
        self.padding_len = padding_len
        self.set_preamble(np.concatenate((self.chirp[-self.padding_len:],self.chirp,self.chirp[:self.padding_len])))

    def get_code_len(self):
        return self.code_len

    def set_code_len(self, code_len):
        self.code_len = code_len
        self.set_payload_len(8 * self.code_len // 2)

    def get_chirp(self):
        return self.chirp

    def set_chirp(self, chirp):
        self.chirp = chirp
        self.set_preamble(np.concatenate((self.chirp[-self.padding_len:],self.chirp,self.chirp[:self.padding_len])))

    def get_preamble(self):
        return self.preamble

    def set_preamble(self, preamble):
        self.preamble = preamble
        self.set_packet_len(len(self.preamble) + self.payload_len)

    def get_payload_len(self):
        return self.payload_len

    def set_payload_len(self, payload_len):
        self.payload_len = payload_len
        self.set_packet_len(len(self.preamble) + self.payload_len)

    def get_subcarrier_map(self):
        return self.subcarrier_map

    def set_subcarrier_map(self, subcarrier_map):
        self.subcarrier_map = subcarrier_map
        self.set_num_channels(len(self.subcarrier_map))
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len
        self.set_packet_time(float(self.packet_len) / (self.samp_rate / self.fbmc_tx_stride))

    def get_fbmc_tx_stride(self):
        return self.fbmc_tx_stride

    def set_fbmc_tx_stride(self, fbmc_tx_stride):
        self.fbmc_tx_stride = fbmc_tx_stride
        self.set_fbmc_rx_stride(self.fbmc_tx_stride/2)
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps4(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps4(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.set_packet_time(float(self.packet_len) / (self.samp_rate / self.fbmc_tx_stride))

    def get_fbmc_fft_len(self):
        return self.fbmc_fft_len

    def set_fbmc_fft_len(self, fbmc_fft_len):
        self.fbmc_fft_len = fbmc_fft_len
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps4(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps4(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])
        self.marmote3_periodic_multiply_const_cc_1.set_vals(self.ampl * np.exp(1.0j * np.pi / self.fbmc_fft_len * np.arange(0, 2 * self.fbmc_fft_len)))

    def get_subcarrier_inv(self):
        return self.subcarrier_inv

    def set_subcarrier_inv(self, subcarrier_inv):
        self.subcarrier_inv = subcarrier_inv

    def get_packet_time(self):
        return self.packet_time

    def set_packet_time(self, packet_time):
        self.packet_time = packet_time

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels

    def get_fbmc_tx_taps(self):
        return self.fbmc_tx_taps

    def set_fbmc_tx_taps(self, fbmc_tx_taps):
        self.fbmc_tx_taps = fbmc_tx_taps

    def get_fbmc_rx_taps(self):
        return self.fbmc_rx_taps

    def set_fbmc_rx_taps(self, fbmc_rx_taps):
        self.fbmc_rx_taps = fbmc_rx_taps

    def get_fbmc_rx_stride(self):
        return self.fbmc_rx_stride

    def set_fbmc_rx_stride(self, fbmc_rx_stride):
        self.fbmc_rx_stride = fbmc_rx_stride

    def get_chirp_fft_len(self):
        return self.chirp_fft_len

    def set_chirp_fft_len(self, chirp_fft_len):
        self.chirp_fft_len = chirp_fft_len

    def get_active_channels(self):
        return self.active_channels

    def set_active_channels(self, active_channels):
        self.active_channels = active_channels
        self.marmote3_subcarrier_allocator_0.set_active_channels(self.active_channels)




def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-a", "--ampl", dest="ampl", type=eng_float, default="300.0m",
        help="Set amplitude [default=%(default)r]")
    parser.add_argument(
        "-f", "--freq", dest="freq", type=eng_float, default="1.0G",
        help="Set frequency [default=%(default)r]")
    parser.add_argument(
        "-r", "--samp-rate", dest="samp_rate", type=intx, default=25000000,
        help="Set sample rate [default=%(default)r]")
    return parser


def main(top_block_cls=test_transmit_latency, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(ampl=options.ampl, freq=options.freq, samp_rate=options.samp_rate)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
