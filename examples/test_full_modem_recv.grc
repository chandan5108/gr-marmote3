options:
  parameters:
    author: ''
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: ''
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: test_full_modem_recv
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: prompt
    sizing_mode: fixed
    thread_safe_setters: ''
    title: ''
    window_size: (1600,2400)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 32]
    rotation: 0
    state: enabled

blocks:
- name: chirp
  id: variable
  parameters:
    comment: ''
    value: marmote3.get_chirp_taps(chirp_len)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [384, 292]
    rotation: 0
    state: enabled
- name: chirp_fft_len
  id: variable
  parameters:
    comment: ''
    value: '64'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [744, 292]
    rotation: 0
    state: enabled
- name: chirp_len
  id: variable
  parameters:
    comment: ''
    value: '128'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [288, 292]
    rotation: 0
    state: enabled
- name: dc_chirp
  id: variable
  parameters:
    comment: ''
    value: chirp + np.ones(chirp_len) * 0.4
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [568, 292]
    rotation: 0
    state: enabled
- name: fbmc_fft_len
  id: variable
  parameters:
    comment: ''
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [912, 220]
    rotation: 0
    state: enabled
- name: fbmc_rx_stride
  id: variable
  parameters:
    comment: ''
    value: fbmc_tx_stride//2
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [792, 220]
    rotation: 0
    state: enabled
- name: fbmc_rx_taps
  id: variable
  parameters:
    comment: ''
    value: marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "rx")
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [480, 220]
    rotation: 0
    state: enabled
- name: fbmc_tx_stride
  id: variable
  parameters:
    comment: ''
    value: '22'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [672, 220]
    rotation: 0
    state: enabled
- name: fbmc_tx_taps
  id: variable
  parameters:
    comment: ''
    value: marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "tx")
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [288, 220]
    rotation: 0
    state: enabled
- name: frame_delay
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: frame delay
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: int
    start: '-100'
    step: '1'
    stop: '100'
    value: '0'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1040, 1092]
    rotation: 180
    state: enabled
- name: freq_offset
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: freq offset
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: '-50000'
    step: '100'
    stop: '50000'
    value: '0'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [488, 516]
    rotation: 180
    state: enabled
- name: max_code_len
  id: variable
  parameters:
    comment: ''
    value: marmote3.get_mcscheme(0).get_code_len(max_data_len)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 364]
    rotation: 0
    state: enabled
- name: max_data_len
  id: variable
  parameters:
    comment: ''
    value: '1506'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [288, 364]
    rotation: 0
    state: enabled
- name: max_frame_len
  id: variable
  parameters:
    comment: ''
    value: 40 + padding_len + chirp_len + padding_len + max_symb_len
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [648, 364]
    rotation: 0
    state: enabled
- name: max_symb_len
  id: variable
  parameters:
    comment: ''
    value: marmote3.get_mcscheme(0).get_symb_len(max_data_len)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [528, 364]
    rotation: 0
    state: enabled
- name: padding_len
  id: variable
  parameters:
    comment: ''
    value: '8'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [856, 292]
    rotation: 0
    state: enabled
- name: subcarrier_inv
  id: variable
  parameters:
    comment: ''
    value: '[subcarrier_map.index(x) if x in subcarrier_map else -1 for x in range(fbmc_fft_len)]'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [528, 148]
    rotation: 0
    state: enabled
- name: subcarrier_map
  id: variable
  parameters:
    comment: ''
    value: list(range(fbmc_fft_len-num_channels//2, fbmc_fft_len)) + list(range(0,
      num_channels//2))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [352, 148]
    rotation: 0
    state: enabled
- name: variable_marmote3_modem_monitor_0
  id: variable_marmote3_modem_monitor
  parameters:
    command_addr: '"tcp://127.0.0.1:7555"'
    comment: ''
    print_mode: '4'
    report_addr: '"tcp://127.0.0.1:7557"'
    report_rate: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 368]
    rotation: 0
    state: enabled
- name: analog_sig_source_x_0
  id: analog_sig_source_x
  parameters:
    affinity: ''
    alias: ''
    amp: '1'
    comment: ''
    freq: freq_offset
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    phase: '0'
    samp_rate: samp_rate
    type: complex
    waveform: analog.GR_COS_WAVE
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [464, 644]
    rotation: 180
    state: enabled
- name: blocks_delay_2_0
  id: blocks_delay
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    delay: frame_delay
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: complex
    vlen: num_channels
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1032, 1228]
    rotation: 180
    state: enabled
- name: blocks_file_source_0
  id: blocks_file_source
  parameters:
    affinity: ''
    alias: ''
    begin_tag: pmt.PMT_NIL
    comment: ''
    file: /home/mmaroti/samples2.dat
    length: '0'
    maxoutbuf: '0'
    minoutbuf: '0'
    offset: '0'
    repeat: 'True'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [96, 488]
    rotation: 0
    state: enabled
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [312, 640]
    rotation: 180
    state: enabled
- name: blocks_throttle_0
  id: blocks_throttle
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    ignoretag: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    samples_per_second: samp_rate
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [312, 500]
    rotation: 0
    state: enabled
- name: chan_fft
  id: fft_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    fft_size: fbmc_fft_len
    forward: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    nthreads: '1'
    shift: 'False'
    type: complex
    window: window.rectangular(fbmc_fft_len)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [584, 796]
    rotation: 0
    state: enabled
- name: chan_periodic_multiply_const
  id: marmote3_periodic_multiply_const_cc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vals: np.exp(-1.0j * np.pi / fbmc_fft_len * np.arange(0, 2 * fbmc_fft_len))
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [104, 780]
    rotation: 0
    state: enabled
- name: chan_polyphase_chan_filter
  id: marmote3_polyphase_chan_filter_ccf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    stride: fbmc_rx_stride
    taps: fbmc_rx_taps
    vlen: fbmc_fft_len
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [328, 808]
    rotation: 0
    state: enabled
- name: chan_polyphase_rotator
  id: marmote3_polyphase_rotator_vcc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    map: subcarrier_map
    maxoutbuf: '0'
    minoutbuf: '0'
    stride: -fbmc_rx_stride
    vlen: fbmc_fft_len
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1040, 808]
    rotation: 0
    state: enabled
- name: chan_subcarrier_remapper
  id: marmote3_subcarrier_remapper
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    map: subcarrier_map
    maxoutbuf: '0'
    minoutbuf: '0'
    output_multiple: '0'
    vlen: fbmc_fft_len
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [816, 812.0]
    rotation: 0
    state: enabled
- name: chan_vector_peak_probe
  id: marmote3_vector_peak_probe
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    histogram_bins: '0'
    histogram_low_db: '0.0'
    histogram_step_db: '0.0'
    report_type: '1'
    vlen: fbmc_fft_len
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [816, 692.0]
    rotation: 0
    state: enabled
- name: dechirp_block_sum
  id: marmote3_block_sum_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decim: '1'
    length: '2'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: float
    vlen: num_channels
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [592, 1088]
    rotation: 0
    state: enabled
- name: dechirp_chirp_frame_timing
  id: marmote3_chirp_frame_timing
  parameters:
    affinity: ''
    alias: ''
    chirp_len: chirp_len
    comment: ''
    fft_len: chirp_fft_len
    maxoutbuf: '0'
    minoutbuf: '0'
    noise_level: '0.0'
    vlen: num_channels
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [152, 1080]
    rotation: 0
    state: enabled
- name: dechirp_complex_to_mag
  id: blocks_complex_to_mag_squared
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: chirp_fft_len
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [952, 964]
    rotation: 0
    state: enabled
- name: dechirp_fft
  id: fft_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    fft_size: chirp_fft_len
    forward: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    nthreads: '1'
    shift: 'False'
    type: complex
    window: window.rectangular(chirp_fft_len)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [712, 940]
    rotation: 0
    state: enabled
- name: dechirp_keep_one_in_n
  id: blocks_keep_one_in_n
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    n: '2'
    type: complex
    vlen: num_channels
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [128, 956]
    rotation: 0
    state: enabled
- name: dechirp_periodic_multiply_const
  id: marmote3_periodic_multiply_const_cc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    vals: np.conj(chirp)
    vlen: num_channels
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [280, 956]
    rotation: 0
    state: enabled
- name: dechirp_transpose_0
  id: marmote3_transpose_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    in_vlen: num_channels
    maxoutbuf: '0'
    minoutbuf: '0'
    out_vlen: chirp_fft_len
    type: complex
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [496, 956]
    rotation: 0
    state: enabled
- name: dechirp_transpose_1
  id: marmote3_transpose_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    in_vlen: chirp_fft_len
    maxoutbuf: '0'
    minoutbuf: '0'
    out_vlen: num_channels
    type: float
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [360, 1092]
    rotation: 0
    state: enabled
- name: decode_ampl_corrector_0
  id: marmote3_packet_ampl_corrector
  parameters:
    affinity: ''
    alias: ''
    block_len: '512'
    comment: ''
    max_output_len: max_symb_len
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [576, 1412]
    rotation: 0
    state: enabled
- name: decode_freq_corrector_0
  id: marmote3_packet_freq_corrector
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    index: '1'
    max_output_len: max_symb_len
    maxoutbuf: '0'
    minoutbuf: '0'
    mod: MCS
    postamble: 'False'
    span: '64'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [784, 1400]
    rotation: 0
    state: enabled
- name: decode_preamb_equalizer_0
  id: marmote3_packet_preamb_equalizer
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    energy_limit: '0.2'
    frame_debug: 'False'
    header: SrcMcsLenCrc
    max_output_len: max_symb_len
    maxoutbuf: '0'
    minoutbuf: '0'
    padding_len: padding_len
    postamble: 'False'
    preamble: marmote3.get_expected_signal(dc_chirp, 41)
    samp_per_symb: '2'
    save_taps: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [312, 1388.0]
    rotation: 0
    state: enabled
- name: decode_preamb_freqcorr_0
  id: marmote3_packet_preamb_freqcorr
  parameters:
    affinity: ''
    alias: ''
    block_length: '32'
    block_stride: '32'
    comment: ''
    max_output_len: 2 * (padding_len + max_frame_len + padding_len)
    maxoutbuf: '0'
    minoutbuf: '0'
    preamb_length: chirp_len  * 2 + 32
    preamb_start: (padding_len + 40 + padding_len) * 2 - 16
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [96, 1396]
    rotation: 0
    state: enabled
- name: decode_ra_decoder_0
  id: marmote3_packet_ra_decoder
  parameters:
    affinity: ''
    alias: ''
    code_len: '0'
    comment: ''
    data_len: max_data_len
    full_passes: '1'
    half_passes: '15'
    maxoutbuf: '0'
    mcs_config: 'True'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1200, 1400]
    rotation: 0
    state: enabled
- name: decode_symb_demod_0
  id: marmote3_packet_symb_demod
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    index: '1'
    max_output_len: max_code_len * 8
    maxoutbuf: '0'
    minoutbuf: '0'
    mod: MCS
    scaling: '36.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [992, 1408]
    rotation: 0
    state: enabled
- name: header_decompressor
  id: marmote3_header_decompressor
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    mode: '4'
    print_headers: 'True'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [464, 1564]
    rotation: 180
    state: enabled
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import marmote3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [24, 124]
    rotation: 0
    state: enabled
- name: import_0_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import numpy as np
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 180]
    rotation: 0
    state: enabled
- name: import_0_0_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import random
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [32, 236]
    rotation: 0
    state: enabled
- name: import_0_2
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [32, 292]
    rotation: 0
    state: enabled
- name: modem_receiver
  id: marmote3_modem_receiver
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    debug_scheduler: 'False'
    maxoutbuf: '0'
    minoutbuf: '0'
    modem_sender_name: ''
    payload_mode: '4'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [896, 1564]
    rotation: 180
    state: enabled
- name: num_channels
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: number of channels (must be even)
    short_id: n
    type: intx
    value: '16'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [216, 28]
    rotation: 0
    state: enabled
- name: qtgui_const_sink_x_0_0
  id: qtgui_const_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'True'
    axislabels: 'True'
    color1: '"blue"'
    color10: '"red"'
    color2: '"red"'
    color3: '"red"'
    color4: '"red"'
    color5: '"red"'
    color6: '"red"'
    color7: '"red"'
    color8: '"red"'
    color9: '"red"'
    comment: ''
    grid: 'False'
    gui_hint: ''
    label1: ''
    label10: ''
    label2: ''
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    legend: 'True'
    marker1: '0'
    marker10: '0'
    marker2: '0'
    marker3: '0'
    marker4: '0'
    marker5: '0'
    marker6: '0'
    marker7: '0'
    marker8: '0'
    marker9: '0'
    name: '""'
    nconnections: '1'
    size: 2 * (padding_len + 40 + padding_len)
    style1: '0'
    style10: '0'
    style2: '0'
    style3: '0'
    style4: '0'
    style5: '0'
    style6: '0'
    style7: '0'
    style8: '0'
    style9: '0'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '"packet_len"'
    type: complex
    update_time: '0.9'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    xmax: '1.5'
    xmin: '-1.5'
    ymax: '1.5'
    ymin: '-1.5'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [320, 1300]
    rotation: 0
    state: enabled
- name: qtgui_freq_sink_x_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '0.05'
    axislabels: 'True'
    bw: samp_rate
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'False'
    fc: '0'
    fftsize: '1024'
    freqhalf: 'True'
    grid: 'False'
    gui_hint: ''
    label: Relative Gain
    label1: ''
    label10: ''
    label2: ''
    label3: ''
    label4: ''
    label5: ''
    label6: ''
    label7: ''
    label8: ''
    label9: ''
    legend: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'True'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.05'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: firdes.WIN_BLACKMAN_hARRIS
    ymax: '-10'
    ymin: '-120'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [48, 608.0]
    rotation: 180
    state: enabled
- name: samp_rate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: sample rate
    short_id: r
    type: intx
    value: '6250000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [384, 28]
    rotation: 0
    state: enabled
- name: subcarrier_serializer2
  id: marmote3_subcarrier_serializer2
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    enabled: list(range(num_channels))
    max_frame_len: 2 * (padding_len + max_frame_len + padding_len)
    maxoutbuf: '0'
    min_frame_len: 2 * (padding_len + 40 + padding_len + chirp_len + padding_len)
    minoutbuf: '0'
    postamb_offset: '0'
    postamble: 'False'
    preamb_offset: 2 * (padding_len + 40 + padding_len + chirp_len) + 3
    trig_level: '12.0'
    trig_rate: 2 * chirp_len // chirp_fft_len
    vlen: num_channels
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [776, 1176]
    rotation: 180
    state: enabled

connections:
- [analog_sig_source_x_0, '0', blocks_multiply_xx_0, '0']
- [blocks_delay_2_0, '0', subcarrier_serializer2, '0']
- [blocks_file_source_0, '0', blocks_throttle_0, '0']
- [blocks_multiply_xx_0, '0', chan_periodic_multiply_const, '0']
- [blocks_multiply_xx_0, '0', qtgui_freq_sink_x_0, '0']
- [blocks_throttle_0, '0', blocks_multiply_xx_0, '1']
- [chan_fft, '0', chan_subcarrier_remapper, '0']
- [chan_fft, '0', chan_vector_peak_probe, '0']
- [chan_periodic_multiply_const, '0', chan_polyphase_chan_filter, '0']
- [chan_polyphase_chan_filter, '0', chan_fft, '0']
- [chan_polyphase_rotator, '0', blocks_delay_2_0, '0']
- [chan_polyphase_rotator, '0', dechirp_keep_one_in_n, '0']
- [chan_subcarrier_remapper, '0', chan_polyphase_rotator, '0']
- [dechirp_block_sum, '0', subcarrier_serializer2, '1']
- [dechirp_chirp_frame_timing, '0', dechirp_transpose_1, '0']
- [dechirp_complex_to_mag, '0', dechirp_chirp_frame_timing, '0']
- [dechirp_fft, '0', dechirp_complex_to_mag, '0']
- [dechirp_keep_one_in_n, '0', dechirp_periodic_multiply_const, '0']
- [dechirp_periodic_multiply_const, '0', dechirp_transpose_0, '0']
- [dechirp_transpose_0, '0', dechirp_fft, '0']
- [dechirp_transpose_1, '0', dechirp_block_sum, '0']
- [decode_ampl_corrector_0, '0', decode_freq_corrector_0, '0']
- [decode_freq_corrector_0, '0', decode_symb_demod_0, '0']
- [decode_preamb_equalizer_0, '0', decode_ampl_corrector_0, '0']
- [decode_preamb_freqcorr_0, '0', decode_preamb_equalizer_0, '0']
- [decode_preamb_freqcorr_0, '0', qtgui_const_sink_x_0_0, '0']
- [decode_ra_decoder_0, '0', modem_receiver, '0']
- [decode_symb_demod_0, '0', decode_ra_decoder_0, '0']
- [modem_receiver, out, header_decompressor, in]
- [subcarrier_serializer2, '0', decode_preamb_freqcorr_0, '0']

metadata:
  file_format: 1
