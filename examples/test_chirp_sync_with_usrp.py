#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Test Chirp Sync With Usrp
# GNU Radio version: 3.8.2.0

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import analog
from gnuradio import blocks
import numpy
from gnuradio import digital
from gnuradio import fft
from gnuradio.fft import window
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time
from gnuradio.qtgui import Range, RangeWidget
import marmote3
import numpy as np
import pmt

from gnuradio import qtgui

class test_chirp_sync_with_usrp(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Test Chirp Sync With Usrp")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Test Chirp Sync With Usrp")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "test_chirp_sync_with_usrp")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################
        self.num_channels = num_channels = 16
        self.prototype_taps = prototype_taps = firdes.low_pass(num_channels ** 0.5, num_channels, 0.45, 0.15, firdes.WIN_HANN)
        self.length = length = 4096
        self.tx_stride = tx_stride = 19
        self.tx_gain = tx_gain = 0
        self.samp_rate = samp_rate = 12000000
        self.rx_stride = rx_stride = 19
        self.rx_noise = rx_noise = 0.0
        self.rx_gain = rx_gain = 0
        self.prototype_len = prototype_len = len(prototype_taps)
        self.freq = freq = 920e6
        self.delay = delay = 0
        self.chirp_ampl = chirp_ampl = 0.2
        self.chirp = chirp = np.exp(-1.0j * np.pi / length * np.arange(0, length) * np.arange(0, length))
        self.ampl = ampl = 0.02

        ##################################################
        # Blocks
        ##################################################
        self._tx_gain_range = Range(0, 30, 1, 0, 200)
        self._tx_gain_win = RangeWidget(self._tx_gain_range, self.set_tx_gain, 'TX gain', "counter_slider", int)
        self.top_grid_layout.addWidget(self._tx_gain_win, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._rx_noise_range = Range(0.0, 0.03, 0.0001, 0.0, 200)
        self._rx_noise_win = RangeWidget(self._rx_noise_range, self.set_rx_noise, 'RX noise', "counter_slider", float)
        self.top_grid_layout.addWidget(self._rx_noise_win, 3, 1, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._rx_gain_range = Range(0, 30, 1, 0, 200)
        self._rx_gain_win = RangeWidget(self._rx_gain_range, self.set_rx_gain, 'RX gain', "counter_slider", int)
        self.top_grid_layout.addWidget(self._rx_gain_win, 0, 1, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._delay_range = Range(0, length * tx_stride * 4, 1, 0, 200)
        self._delay_win = RangeWidget(self._delay_range, self.set_delay, 'Sample Delay', "counter_slider", int)
        self.top_grid_layout.addWidget(self._delay_win, 3, 2, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._chirp_ampl_range = Range(0, 1.0, 0.01, 0.2, 200)
        self._chirp_ampl_win = RangeWidget(self._chirp_ampl_range, self.set_chirp_ampl, 'chirp amplitude', "counter_slider", float)
        self.top_grid_layout.addWidget(self._chirp_ampl_win, 0, 2, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._ampl_range = Range(0, 0.5, 0.01, 0.02, 200)
        self._ampl_win = RangeWidget(self._ampl_range, self.set_ampl, 'TX amplitude', "counter_slider", float)
        self.top_grid_layout.addWidget(self._ampl_win, 3, 0, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(('""addr=192.168.20.2""', "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
        )
        self.uhd_usrp_source_0.set_center_freq(freq, 0)
        self.uhd_usrp_source_0.set_gain(rx_gain, 0)
        self.uhd_usrp_source_0.set_antenna('TX/RX', 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_time_unknown_pps(uhd.time_spec())
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
            ",".join(("addr=192.168.10.2", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
            '',
        )
        self.uhd_usrp_sink_0.set_center_freq(freq, 0)
        self.uhd_usrp_sink_0.set_gain(tx_gain, 0)
        self.uhd_usrp_sink_0.set_antenna('TX/RX', 0)
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_time_unknown_pps(uhd.time_spec())
        self.qtgui_waterfall_sink_x_0 = qtgui.waterfall_sink_c(
            1024, #size
            firdes.WIN_BLACKMAN_hARRIS, #wintype
            freq, #fc
            samp_rate, #bw
            "", #name
            1 #number of inputs
        )
        self.qtgui_waterfall_sink_x_0.set_update_time(0.1)
        self.qtgui_waterfall_sink_x_0.enable_grid(False)
        self.qtgui_waterfall_sink_x_0.enable_axis_labels(True)



        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        colors = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_waterfall_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_waterfall_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_waterfall_sink_x_0.set_color_map(i, colors[i])
            self.qtgui_waterfall_sink_x_0.set_line_alpha(i, alphas[i])

        self.qtgui_waterfall_sink_x_0.set_intensity_range(-110, -40)

        self._qtgui_waterfall_sink_x_0_win = sip.wrapinstance(self.qtgui_waterfall_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_waterfall_sink_x_0_win, 2, 0, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_f(
            2*length, #size
            samp_rate/num_channels, #samp_rate
            "", #name
            2 #number of inputs
        )
        self.qtgui_time_sink_x_0.set_update_time(0.1)
        self.qtgui_time_sink_x_0.set_y_axis(0, 20)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0.enable_stem_plot(False)


        labels = ['', '', '', '', '',
            '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ['blue', 'red', 'green', 'black', 'cyan',
            'magenta', 'yellow', 'dark red', 'dark green', 'dark blue']
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]
        styles = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1]


        for i in range(2):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_win, 1, 1, 2, 2)
        for r in range(1, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_freq_sink_x_0 = qtgui.freq_sink_c(
            1024, #size
            firdes.WIN_BLACKMAN_hARRIS, #wintype
            freq, #fc
            samp_rate, #bw
            "", #name
            1
        )
        self.qtgui_freq_sink_x_0.set_update_time(0.1)
        self.qtgui_freq_sink_x_0.set_y_axis(-140, 10)
        self.qtgui_freq_sink_x_0.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0.enable_grid(False)
        self.qtgui_freq_sink_x_0.set_fft_average(1.0)
        self.qtgui_freq_sink_x_0.enable_axis_labels(True)
        self.qtgui_freq_sink_x_0.enable_control_panel(False)

        self.qtgui_freq_sink_x_0.disable_legend()


        labels = ['', '', '', '', '',
            '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
            1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
            "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
            1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_freq_sink_x_0_win, 1, 0, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.marmote3_transpose_vxx_0_0_0 = marmote3.transpose_vxx(gr.sizeof_float, length//16, num_channels)
        self.marmote3_transpose_vxx_0_0 = marmote3.transpose_vxx(gr.sizeof_gr_complex, num_channels, length//16)
        self.marmote3_subcarrier_allocator_0 = marmote3.subcarrier_allocator(num_channels, [1], [], 1, getattr(self, '') if '' else marmote3.vector_peak_probe_sptr(), [], getattr(self, '') if '' else marmote3.modem_sender2_sptr(), False)
        self.marmote3_subcarrier_allocator_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_polyphase_synt_filter_ccf_0 = marmote3.polyphase_synt_filter_ccf(num_channels, prototype_taps, tx_stride or num_channels)
        self.marmote3_polyphase_synt_filter_ccf_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_polyphase_chan_filter_ccf_0 = marmote3.polyphase_chan_filter_ccf(num_channels, prototype_taps, rx_stride or num_channels)
        self.marmote3_polyphase_chan_filter_ccf_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_periodic_multiply_const_cc_0 = marmote3.periodic_multiply_const_cc(num_channels, np.conj(chirp))
        self.marmote3_chirp_frame_timing_0 = marmote3.chirp_frame_timing(length, length//16, num_channels, 1e-9)
        self.fft_vxx_1_0 = fft.fft_vcc(length//16, True, window.rectangular(length/16), False, 2)
        self.fft_vxx_0_0 = fft.fft_vcc(num_channels, True, window.rectangular(num_channels), False, 1)
        self.fft_vxx_0 = fft.fft_vcc(num_channels, False, window.rectangular(num_channels), False, 1)
        self.digital_chunks_to_symbols_xx_0 = digital.chunks_to_symbols_bc([1,1j,-1,-1j], 1)
        self.blocks_vector_to_stream_0_0 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, num_channels)
        self.blocks_vector_to_stream_0 = blocks.vector_to_stream(gr.sizeof_float*1, num_channels)
        self.blocks_vector_source_x_0 = blocks.vector_source_c(chirp * chirp_ampl, True, 1, [])
        self.blocks_stream_to_tagged_stream_0 = blocks.stream_to_tagged_stream(gr.sizeof_gr_complex, 1, len(chirp), "packet_len")
        self.blocks_stream_mux_0 = blocks.stream_mux(gr.sizeof_gr_complex*1, len(chirp),len(chirp))
        self.blocks_repeat_0 = blocks.repeat(gr.sizeof_float*num_channels, 16)
        self.blocks_multiply_xx_0_0 = blocks.multiply_vcc(1)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_moving_average_xx_0 = blocks.moving_average_ff(10000, 1.0/10000, 40000, 1)
        self.blocks_keep_m_in_n_1 = blocks.keep_m_in_n(gr.sizeof_float, 1, num_channels, 1)
        self.blocks_keep_m_in_n_0_2 = blocks.keep_m_in_n(gr.sizeof_gr_complex, 1, num_channels, 1)
        self.blocks_divide_xx_1 = blocks.divide_ff(1)
        self.blocks_delay_2_0 = blocks.delay(gr.sizeof_gr_complex*num_channels, length)
        self.blocks_delay_1 = blocks.delay(gr.sizeof_gr_complex*1, delay)
        self.blocks_complex_to_mag_squared_0_1 = blocks.complex_to_mag_squared(length//16)
        self.blocks_complex_to_mag_squared_0_0 = blocks.complex_to_mag_squared(1)
        self.blocks_add_xx_2 = blocks.add_vcc(1)
        self.blocks_add_xx_1 = blocks.add_vcc(1)
        self.blocks_add_xx_0 = blocks.add_vcc(1)
        self.analog_sig_source_x_0_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, -samp_rate / (2 * num_channels), 1, 0, 0)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, samp_rate / (2 * num_channels), ampl, 0, 0)
        self.analog_random_source_x_0 = blocks.vector_source_b(list(map(int, numpy.random.randint(0, 4, 9999))), True)
        self.analog_fastnoise_source_x_0 = analog.fastnoise_source_c(analog.GR_GAUSSIAN, rx_noise, 0, 8192)
        self.analog_const_source_x_1 = analog.sig_source_c(0, analog.GR_CONST_WAVE, 0, 0, 0)
        self.analog_const_source_x_0 = analog.sig_source_c(0, analog.GR_CONST_WAVE, 0, 0, 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_const_source_x_0, 0), (self.blocks_add_xx_1, 0))
        self.connect((self.analog_const_source_x_1, 0), (self.blocks_stream_mux_0, 0))
        self.connect((self.analog_fastnoise_source_x_0, 0), (self.blocks_add_xx_2, 0))
        self.connect((self.analog_random_source_x_0, 0), (self.digital_chunks_to_symbols_xx_0, 0))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.analog_sig_source_x_0_0, 0), (self.blocks_multiply_xx_0_0, 1))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_stream_mux_0, 1))
        self.connect((self.blocks_add_xx_1, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.blocks_add_xx_2, 0), (self.blocks_delay_1, 0))
        self.connect((self.blocks_add_xx_2, 0), (self.qtgui_freq_sink_x_0, 0))
        self.connect((self.blocks_add_xx_2, 0), (self.qtgui_waterfall_sink_x_0, 0))
        self.connect((self.blocks_complex_to_mag_squared_0_0, 0), (self.blocks_divide_xx_1, 0))
        self.connect((self.blocks_complex_to_mag_squared_0_0, 0), (self.blocks_moving_average_xx_0, 0))
        self.connect((self.blocks_complex_to_mag_squared_0_1, 0), (self.marmote3_chirp_frame_timing_0, 0))
        self.connect((self.blocks_delay_1, 0), (self.blocks_multiply_xx_0_0, 0))
        self.connect((self.blocks_delay_2_0, 0), (self.blocks_vector_to_stream_0_0, 0))
        self.connect((self.blocks_divide_xx_1, 0), (self.qtgui_time_sink_x_0, 0))
        self.connect((self.blocks_keep_m_in_n_0_2, 0), (self.blocks_complex_to_mag_squared_0_0, 0))
        self.connect((self.blocks_keep_m_in_n_1, 0), (self.qtgui_time_sink_x_0, 1))
        self.connect((self.blocks_moving_average_xx_0, 0), (self.blocks_divide_xx_1, 1))
        self.connect((self.blocks_multiply_xx_0, 0), (self.blocks_add_xx_1, 1))
        self.connect((self.blocks_multiply_xx_0_0, 0), (self.marmote3_polyphase_chan_filter_ccf_0, 0))
        self.connect((self.blocks_repeat_0, 0), (self.blocks_vector_to_stream_0, 0))
        self.connect((self.blocks_stream_mux_0, 0), (self.blocks_stream_to_tagged_stream_0, 0))
        self.connect((self.blocks_stream_to_tagged_stream_0, 0), (self.marmote3_subcarrier_allocator_0, 0))
        self.connect((self.blocks_vector_source_x_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.blocks_vector_to_stream_0, 0), (self.blocks_keep_m_in_n_1, 0))
        self.connect((self.blocks_vector_to_stream_0_0, 0), (self.blocks_keep_m_in_n_0_2, 0))
        self.connect((self.digital_chunks_to_symbols_xx_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.marmote3_polyphase_synt_filter_ccf_0, 0))
        self.connect((self.fft_vxx_0_0, 0), (self.marmote3_periodic_multiply_const_cc_0, 0))
        self.connect((self.fft_vxx_1_0, 0), (self.blocks_complex_to_mag_squared_0_1, 0))
        self.connect((self.marmote3_chirp_frame_timing_0, 0), (self.marmote3_transpose_vxx_0_0_0, 0))
        self.connect((self.marmote3_periodic_multiply_const_cc_0, 0), (self.blocks_delay_2_0, 0))
        self.connect((self.marmote3_periodic_multiply_const_cc_0, 0), (self.marmote3_transpose_vxx_0_0, 0))
        self.connect((self.marmote3_polyphase_chan_filter_ccf_0, 0), (self.fft_vxx_0_0, 0))
        self.connect((self.marmote3_polyphase_synt_filter_ccf_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.marmote3_subcarrier_allocator_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.marmote3_transpose_vxx_0_0, 0), (self.fft_vxx_1_0, 0))
        self.connect((self.marmote3_transpose_vxx_0_0_0, 0), (self.blocks_repeat_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_add_xx_2, 1))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "test_chirp_sync_with_usrp")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels
        self.set_prototype_taps(firdes.low_pass(self.num_channels ** 0.5, self.num_channels, 0.45, 0.15, firdes.WIN_HANN))
        self.analog_sig_source_x_0.set_frequency(self.samp_rate / (2 * self.num_channels))
        self.analog_sig_source_x_0_0.set_frequency(-self.samp_rate / (2 * self.num_channels))
        self.blocks_keep_m_in_n_0_2.set_n(self.num_channels)
        self.blocks_keep_m_in_n_1.set_n(self.num_channels)
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate/self.num_channels)

    def get_prototype_taps(self):
        return self.prototype_taps

    def set_prototype_taps(self, prototype_taps):
        self.prototype_taps = prototype_taps
        self.set_prototype_len(len(self.prototype_taps))

    def get_length(self):
        return self.length

    def set_length(self, length):
        self.length = length
        self.set_chirp(np.exp(-1.0j * np.pi / self.length * np.arange(0, self.length) * np.arange(0, self.length)))
        self.blocks_delay_2_0.set_dly(self.length)

    def get_tx_stride(self):
        return self.tx_stride

    def set_tx_stride(self, tx_stride):
        self.tx_stride = tx_stride

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        self.uhd_usrp_sink_0.set_gain(self.tx_gain, 0)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.analog_sig_source_x_0.set_frequency(self.samp_rate / (2 * self.num_channels))
        self.analog_sig_source_x_0_0.set_sampling_freq(self.samp_rate)
        self.analog_sig_source_x_0_0.set_frequency(-self.samp_rate / (2 * self.num_channels))
        self.qtgui_freq_sink_x_0.set_frequency_range(self.freq, self.samp_rate)
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate/self.num_channels)
        self.qtgui_waterfall_sink_x_0.set_frequency_range(self.freq, self.samp_rate)
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_rx_stride(self):
        return self.rx_stride

    def set_rx_stride(self, rx_stride):
        self.rx_stride = rx_stride

    def get_rx_noise(self):
        return self.rx_noise

    def set_rx_noise(self, rx_noise):
        self.rx_noise = rx_noise
        self.analog_fastnoise_source_x_0.set_amplitude(self.rx_noise)

    def get_rx_gain(self):
        return self.rx_gain

    def set_rx_gain(self, rx_gain):
        self.rx_gain = rx_gain
        self.uhd_usrp_source_0.set_gain(self.rx_gain, 0)

    def get_prototype_len(self):
        return self.prototype_len

    def set_prototype_len(self, prototype_len):
        self.prototype_len = prototype_len

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.qtgui_freq_sink_x_0.set_frequency_range(self.freq, self.samp_rate)
        self.qtgui_waterfall_sink_x_0.set_frequency_range(self.freq, self.samp_rate)
        self.uhd_usrp_sink_0.set_center_freq(self.freq, 0)
        self.uhd_usrp_source_0.set_center_freq(self.freq, 0)

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay
        self.blocks_delay_1.set_dly(self.delay)

    def get_chirp_ampl(self):
        return self.chirp_ampl

    def set_chirp_ampl(self, chirp_ampl):
        self.chirp_ampl = chirp_ampl
        self.blocks_vector_source_x_0.set_data(self.chirp * self.chirp_ampl, [])

    def get_chirp(self):
        return self.chirp

    def set_chirp(self, chirp):
        self.chirp = chirp
        self.blocks_stream_to_tagged_stream_0.set_packet_len(len(self.chirp))
        self.blocks_stream_to_tagged_stream_0.set_packet_len_pmt(len(self.chirp))
        self.blocks_vector_source_x_0.set_data(self.chirp * self.chirp_ampl, [])
        self.marmote3_periodic_multiply_const_cc_0.set_vals(np.conj(self.chirp))

    def get_ampl(self):
        return self.ampl

    def set_ampl(self, ampl):
        self.ampl = ampl
        self.analog_sig_source_x_0.set_amplitude(self.ampl)





def main(top_block_cls=test_chirp_sync_with_usrp, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        tb.stop()
        tb.wait()

    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()

if __name__ == '__main__':
    main()
