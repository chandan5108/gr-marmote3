#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Test Radar Detector
# GNU Radio version: 3.8.2.0

from gnuradio import blocks
import pmt
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import marmote3
from gnuradio.fft import window


class test_radar_detector(gr.top_block):

    def __init__(self, radar_alpha=0.05, radar_count=10, radar_power=50):
        gr.top_block.__init__(self, "Test Radar Detector")

        ##################################################
        # Parameters
        ##################################################
        self.radar_alpha = radar_alpha
        self.radar_count = radar_count
        self.radar_power = radar_power

        ##################################################
        # Variables
        ##################################################
        self.modem_monitor = marmote3.modem_monitor(1, "tcp://127.0.0.1:7557", 2, "tcp://127.0.0.1:7555")
        self.spectrogram_fft = spectrogram_fft = 256
        self.samp_rate = samp_rate = 12.5e6 * 4

        ##################################################
        # Blocks
        ##################################################
        self.msg_connect((self.modem_monitor, 'unused'), (self.modem_monitor, 'unused'))
        self.marmote3_radar_detector_0 = marmote3.radar_detector(spectrogram_fft, radar_alpha, radar_power, radar_count, 5000, True)
        self.marmote3_radar_detector_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_fft_and_magnitude_0 = marmote3.fft_and_magnitude(spectrogram_fft, spectrogram_fft // 2, True, window.hann(spectrogram_fft), True)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_gr_complex*1, '/home/mmaroti/samples2.dat', False, 0, 0)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_file_source_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.marmote3_fft_and_magnitude_0, 0))
        self.connect((self.marmote3_fft_and_magnitude_0, 0), (self.marmote3_radar_detector_0, 0))


    def get_radar_alpha(self):
        return self.radar_alpha

    def set_radar_alpha(self, radar_alpha):
        self.radar_alpha = radar_alpha

    def get_radar_count(self):
        return self.radar_count

    def set_radar_count(self, radar_count):
        self.radar_count = radar_count

    def get_radar_power(self):
        return self.radar_power

    def set_radar_power(self, radar_power):
        self.radar_power = radar_power

    def get_variable_marmote3_modem_monitor_0(self):
        return self.variable_marmote3_modem_monitor_0

    def set_variable_marmote3_modem_monitor_0(self, variable_marmote3_modem_monitor_0):
        self.variable_marmote3_modem_monitor_0 = variable_marmote3_modem_monitor_0

    def get_spectrogram_fft(self):
        return self.spectrogram_fft

    def set_spectrogram_fft(self, spectrogram_fft):
        self.spectrogram_fft = spectrogram_fft

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)




def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-a", "--radar-alpha", dest="radar_alpha", type=eng_float, default="50.0m",
        help="Set radar alpha [default=%(default)r]")
    parser.add_argument(
        "-c", "--radar-count", dest="radar_count", type=intx, default=10,
        help="Set radar min count [default=%(default)r]")
    parser.add_argument(
        "-p", "--radar-power", dest="radar_power", type=eng_float, default="50.0",
        help="Set radar power [default=%(default)r]")
    return parser


def main(top_block_cls=test_radar_detector, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(radar_alpha=options.radar_alpha, radar_count=options.radar_count, radar_power=options.radar_power)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    tb.wait()


if __name__ == '__main__':
    main()
