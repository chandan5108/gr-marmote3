#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Test Transpose Speed
# GNU Radio version: 3.8.2.0

from gnuradio import blocks
import numpy
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import marmote3


class test_transpose_speed(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Transpose Speed")

        ##################################################
        # Variables
        ##################################################
        self.dim2 = dim2 = 32
        self.dim1 = dim1 = 16

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_transpose_vxx_0 = marmote3.transpose_vxx(gr.sizeof_gr_complex, dim1, dim2)
        self.blocks_vector_to_stream_0 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, dim2)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, dim1)
        self.blocks_probe_rate_0 = blocks.probe_rate(gr.sizeof_gr_complex*1, 1000.0, 0.15)
        self.blocks_message_debug_0 = blocks.message_debug()
        self.blocks_interleaved_short_to_complex_0 = blocks.interleaved_short_to_complex(False, False)
        self.analog_random_source_x_0 = blocks.vector_source_s(list(map(int, numpy.random.randint(0, 256, 10000))), True)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_probe_rate_0, 'rate'), (self.blocks_message_debug_0, 'print'))
        self.connect((self.analog_random_source_x_0, 0), (self.blocks_interleaved_short_to_complex_0, 0))
        self.connect((self.blocks_interleaved_short_to_complex_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.marmote3_transpose_vxx_0, 0))
        self.connect((self.blocks_vector_to_stream_0, 0), (self.blocks_probe_rate_0, 0))
        self.connect((self.marmote3_transpose_vxx_0, 0), (self.blocks_vector_to_stream_0, 0))


    def get_dim2(self):
        return self.dim2

    def set_dim2(self, dim2):
        self.dim2 = dim2

    def get_dim1(self):
        return self.dim1

    def set_dim1(self, dim1):
        self.dim1 = dim1





def main(top_block_cls=test_transpose_speed, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    try:
        input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
