# -*- coding: utf-8 -*-
#
# Copyright 2018 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function
from gnuradio import gr
import marmote3
import pmt


class ModemSetter(gr.feval_p):
    """This class can set an arbitrary parameter of the top block."""

    def __init__(self, top_block):
        gr.feval_p.__init__(self)
        self.top_block = top_block

    def eval(self, data):
        try:
            data = pmt.to_python(data)
        except Exception as e:
            print("modem_setter: pmt decode error {}".format(e))
            return

        try:
            func = data[0]
            args = data[1:]
        except Exception as e:
            print("modem_setter: invalid argument {}".format(e))
            return

        try:
            func = getattr(self.top_block, func)
        except Exception as e:
            print("modem_setter: no such function {}".format(e))
            return

        try:
            func(*args)
            print("modem_setter: command {} succeeded".format(data))
        except Exception as e:
            print("modem_setter: command {} failed {}".format(data, e))
            return
