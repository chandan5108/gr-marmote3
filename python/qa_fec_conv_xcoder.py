#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013-2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr_unittest
import numpy as np
import marmote3_swig as marmote3


class qa_fec_conv_xcoder (gr_unittest.TestCase):

    def test1(self):
        encoder = marmote3.fec_conv_encoder([0x4F, 0x6D], 7)
        decoder = marmote3.fec_conv_decoder([0x4F, 0x6D], 7, 16)

        data = [0, 1]
        code = encoder.encode(data)
        self.assertEqual(len(code), 16)

        # bit zero is 1.0, bit one is -1.0
        soft = 1.0 - np.array(code) * 2.0
        data2 = decoder.decode(soft)
        self.assertEqual(data, list(data2))


if __name__ == '__main__':
    gr_unittest.run(qa_fec_conv_xcoder)
