#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np


class qa_subcarrier_serializer2 (gr_unittest.TestCase):

    def test1(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        serializer = marmote3.subcarrier_serializer2(
            2, 2, 1.0, 0, 0, False, 4, 4, range(2))
        top.connect(
            blocks.vector_source_c(
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], False),
            blocks.stream_to_vector(gr.sizeof_gr_complex, 2),
            (serializer, 0))
        top.connect(
            blocks.vector_source_f([1.0, 0.5, 0.0, 1.0, 0.0, 0.0], False),
            blocks.stream_to_vector(gr.sizeof_float, 2),
            (serializer, 1))
        top.connect(
            serializer,
            sink)
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), [1, 3, 5, 7, 6, 8, 10, 12]))

    def test2(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        serializer = marmote3.subcarrier_serializer2(
            1, 2, 1.0, 0, 0, False, 3, 3, range(1))
        top.connect(
            blocks.vector_source_c(
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], False),
            (serializer, 0))
        top.connect(
            blocks.vector_source_f(
                [1.0, 0.0, 1.0, 0.0, 1.0, 1.1, 0.0, 0.0], False),
            (serializer, 1))
        top.connect(
            serializer,
            sink)
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), [1, 2, 3, 5, 6, 7]))

    def test3(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        serializer = marmote3.subcarrier_serializer2(
            1, 2, 1.0, 0, 0, False, 3, 3, range(1))
        top.connect(
            blocks.vector_source_c(
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], False),
            (serializer, 0))
        top.connect(
            blocks.vector_source_f(
                [1.0, 0.0, 1.0, 0.0, 1.0, 1.1, 0.0, 0.0], False),
            (serializer, 1))
        top.connect(
            serializer,
            sink)
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), [1, 2, 3, 5, 6, 7, 11, 12, 13]))


if __name__ == '__main__':
    gr_unittest.run(qa_subcarrier_serializer2)
