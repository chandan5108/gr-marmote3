#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks, filter
import marmote3_swig as marmote3
import numpy as np


class qa_fast_dc_blocker (gr_unittest.TestCase):

    def test(self):
        top = gr.top_block()
        src = blocks.vector_source_c([0, 1, 2, -2, 1, -2, 0, 0], False)
        sink1 = blocks.vector_sink_c()
        sink2 = blocks.vector_sink_c()
        top.connect(src, marmote3.fast_dc_blocker(1), sink1)
        top.connect(src, filter.dc_blocker_cc(2, False), sink2)
        top.run()
        print(sink1.data())
        print(sink2.data())
        self.assertTrue(np.allclose(sink1.data(), sink2.data()))


if __name__ == '__main__':
    gr_unittest.run(qa_fast_dc_blocker)
