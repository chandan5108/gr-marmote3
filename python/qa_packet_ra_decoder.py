#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013-2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import random


class qa_packet_ra_decoder (gr_unittest.TestCase):

    def gen_test(self, data_len, code_len, full_passes, half_passes):
        data = [random.randint(0, 255) for x in range(data_len)]
        top = gr.top_block()
        sink = blocks.vector_sink_b()
        top.connect(
            blocks.vector_source_b(data, False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_char, 1, data_len, "packet_len"),
            marmote3.packet_ra_encoder(data_len, code_len),
            marmote3.packet_symb_mod(marmote3.MOD_QPSK, 4 * code_len),
            marmote3.packet_symb_demod(marmote3.MOD_QPSK, 200.0, 8 * code_len),
            marmote3.packet_ra_decoder(
                data_len, code_len, full_passes, half_passes),
            sink
        )
        top.run()
        print(data)
        print(sink.data())
        self.assertTrue(list(data) == list(sink.data()))

    def test1(self):
        self.gen_test(100, 200, 1, 0)

    def test2(self):
        self.gen_test(100, 200, 0, 1)

    def test3(self):
        self.gen_test(100, 100, 1, 0)

    def test4(self):
        self.gen_test(100, 100, 0, 1)

    def test5(self):
        self.gen_test(2, 2, 1, 0)

    def test6(self):
        self.gen_test(2, 2, 0, 1)


if __name__ == '__main__':
    gr_unittest.run(qa_packet_ra_decoder)
