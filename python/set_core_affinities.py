#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function
import re

ALLOWED_CPUS = None


def get_allowed_cpus():
    """Returns the list of processor ids that this (virtual) machine can use."""

    global ALLOWED_CPUS
    if ALLOWED_CPUS is not None:
        return ALLOWED_CPUS

    ALLOWED_CPUS = []
    try:
        match = re.search(r'(?m)^Cpus_allowed_list:\s*([\d,-]*)$',
                          open('/proc/self/status').read())
        if match:
            for item in match.group(1).split(','):
                pair = [int(n) for n in item.split('-')]
                if len(pair) == 1:
                    ALLOWED_CPUS.append(pair[0])
                elif len(pair) == 2:
                    ALLOWED_CPUS.extend(range(pair[0], pair[1] + 1))
    except:
        pass

    if len(ALLOWED_CPUS) == 0:
        try:
            import multiprocessing
            ALLOWED_CPUS = range(0, multiprocessing.cpu_count())
        except:
            pass

    print('set_core_affinities: cpus {}'.format(ALLOWED_CPUS))
    return ALLOWED_CPUS


def get_preferred_cpus():
    """Reorders the allowed cpus into best NUMA order assuming a
       2 hyperthread * 12 core * 2 numa node topology. Returns
       a list of list of cpus."""
    allowed = list(get_allowed_cpus())

    preferred = []
    for numa in [0, 1]:
        for core in range(12):
            cpu0 = 2 * core + numa
            cpu1 = 24 + cpu0
            if cpu0 in allowed and cpu1 in allowed:
                preferred.append([cpu0, cpu1])
                allowed.remove(cpu0)
                allowed.remove(cpu1)

    for numa in [1, 0]:
        for core in range(12):
            cpu0 = 2 * core + numa
            cpu1 = 24 + cpu0
            if cpu0 in allowed and cpu1 not in allowed:
                preferred.append([cpu0])
                allowed.remove(cpu0)
            elif cpu0 not in allowed and cpu1 in allowed:
                preferred.append([cpu1])
                allowed.remove(cpu1)

    for cpu in allowed:
        preferred.append([cpu])

    return preferred


def set_core_affinities(top_block, groups):
    preferred = get_preferred_cpus()
    if len(preferred) == 0:
        return

    index = 0
    for group in groups.split('\n'):
        if group.startswith("#"):
            continue
        for name in group.split(','):
            block = getattr(top_block, name.strip(), None)
            if block is None:
                print('set_core_affinities: {} not found'.format(name))
            else:
                try:
                    block.set_processor_affinity(preferred[index])
                    print('set_core_affinities: {} set to {}'.format(
                        name, preferred[index]))
                except Exception as err:
                    print(err)
                    print('set_core_affinities: {} error'.format(name))
        index = (index + 1) % len(preferred)


if __name__ == '__main__':
    print(get_preferred_cpus())
