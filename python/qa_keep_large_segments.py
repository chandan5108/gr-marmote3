#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3


class qa_keep_large_segments (gr_unittest.TestCase):

    def test1(self):
        top = gr.top_block()
        sink = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(range(100), False),
            marmote3.keep_large_segments(4, True, 0, 1, 10, -1, -1),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(list(sink.data()) == list(range(0, 100, 10)))

    def test2(self):
        top = gr.top_block()
        sink = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(range(100), False),
            marmote3.keep_large_segments(4, True, 25, 1, 10, -1, -1),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(list(sink.data()) == list(range(25, 100, 10)))

    def test3(self):
        top = gr.top_block()
        sink = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(range(100), False),
            marmote3.keep_large_segments(4, True, 25, 1, 10, 5, -1),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(list(sink.data()) == [25, 35, 45, 55, 65])

    def test4(self):
        top = gr.top_block()
        sink = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(range(100), False),
            marmote3.keep_large_segments(4, True, 25, 10, 10, 25, -1),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(list(sink.data()) == list(range(25, 50)))

    def test5(self):
        top = gr.top_block()
        sink = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(range(100), False),
            marmote3.keep_large_segments(4, False, 25, 10, 10, 25, -1),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(len(sink.data()) == 0)


if __name__ == '__main__':
    gr_unittest.run(qa_keep_large_segments)
