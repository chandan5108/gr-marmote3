#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np
import random


class qa_polyphase_synt_filter_ccf (gr_unittest.TestCase):

    def test0(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(
                [1, 1, 1, 1, 0, 0, 0, 0], False),
            blocks.stream_to_vector(gr.sizeof_gr_complex, 4),
            marmote3.polyphase_synt_filter_ccf(4, [1, 2, 3, 4, 5, 6, 7], 5),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(np.allclose(sink.data(),
                                    [1, 2, 3, 4, 5, 6, 7, 0, 0, 0]))

    def test1(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(
                [-1, 1, 1, 1, -10, 10, 10, 10, -100, 100, 100, 100], False),
            blocks.stream_to_vector(gr.sizeof_gr_complex, 4),
            marmote3.polyphase_synt_filter_ccf(4, [1, 2, 3, 4, 5, 6, 7], 5),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(np.allclose(sink.data(),
                                    [-1, 2, 3, 4, -5, 6 - 10, 7 + 20, 30, 40, -50, 60 - 100, 70 + 200, 300, 400, -500]))

    def test2(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(
                [-1, 1, 1, 1, -10, 10, 10, 10, -100, 100, 100, 100], False),
            blocks.stream_to_vector(gr.sizeof_gr_complex, 4),
            marmote3.polyphase_synt_filter_ccf(4, [1, 2, 3, 4, 5, 6, 7], 2),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(np.allclose(sink.data(),
                                    [-1, 2, 3 - 10, 4 + 20, -5 + 30 - 100, 6 + 40 + 200]))

    def reference(self, vlen, stride, taps, data):
        assert len(data) % (2 * vlen) == 0

        blocks = int(len(data) / (2 * vlen))
        output = np.zeros([2 * stride * blocks], dtype=np.float)
        for b in range(0, blocks):
            for i in range(0, len(taps)):
                op = b * stride + i
                if 2 * op < len(output):
                    ip = b * vlen + (i % vlen)
                    t = taps[i] if i < len(taps) else 0.0
                    output[2 * op] += data[2 * ip] * t
                    output[2 * op + 1] += data[2 * ip + 1] * t
        return output

    def gen_test(self, vlen, stride, taps_len, data_len):
        taps = np.random.random([taps_len]) - 0.5
        # print(taps)
        data = np.random.random([2 * vlen * data_len]) - 0.5
        # print(data)

        top = gr.top_block()
        sink = blocks.vector_sink_f()
        top.connect(
            blocks.vector_source_f(list(data), False),
            blocks.stream_to_vector(gr.sizeof_float, 2 * vlen),
            marmote3.polyphase_synt_filter_ccf(vlen, list(taps), stride),
            blocks.vector_to_stream(gr.sizeof_float, 2),
            sink
        )
        top.run()
        result = np.array(sink.data(), dtype=np.float)
        # print(result)
        self.assertEqual(len(result), 2 * stride * data_len)

        reference = self.reference(vlen, stride, taps, data)
        # print(reference)
        print(np.max(np.abs(result - reference)))
        self.assertTrue(np.allclose(result, reference, atol=1e-7))

    def test3(self):
        self.gen_test(16, 11, 281, random.randint(1, 99))
        self.gen_test(16, 11, 281, random.randint(1, 99))
        self.gen_test(20, 22, 281, random.randint(1, 99))
        self.gen_test(20, 22, 281, random.randint(1, 99))
        self.gen_test(25, 28, 281, random.randint(1, 99))
        self.gen_test(25, 28, 281, random.randint(1, 99))
        self.gen_test(25, 28, 300, random.randint(1, 99))
        self.gen_test(25, 28, 300, random.randint(1, 99))


if __name__ == '__main__':
    gr_unittest.run(qa_polyphase_synt_filter_ccf)
