#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np


class qa_peak_probe (gr_unittest.TestCase):

    def test1(self):
        top = gr.top_block()
        data = list(map(float, np.random.randint(-1000, 1001, 32)))
        probe = marmote3.peak_probe()
        top.connect(blocks.vector_source_f(data, False),
                    blocks.stream_to_vector(gr.sizeof_float, 2),
                    probe)
        top.run()

        a = np.array(data[0::2])
        b = np.array(data[1::2])

        self.assertTrue(
            probe.get_peak() - np.sqrt(np.amax(a * a + b * b)) < 1e-3)
        self.assertEqual(probe.get_peak(), 0.0)


if __name__ == '__main__':
    gr_unittest.run(qa_peak_probe)
