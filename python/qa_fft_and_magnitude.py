#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2019 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks, fft
import numpy as np

import marmote3_swig as marmote3


class qa_fft_and_magnitude(gr_unittest.TestCase):

    def test1(self):
        top = gr.top_block()
        sink = blocks.vector_sink_f()
        top.connect(
            blocks.vector_source_c(
                [1, 1j, -1, -1j], False),
            marmote3.fft_and_magnitude(4, 4, True, [1, 1, 1, 1], True),
            # blocks.stream_to_vector(gr.sizeof_gr_complex, 4),
            # fft.fft_vcc(4, True, [1, 1, 1, 1], True, 1),
            # blocks.complex_to_mag_squared(4),
            blocks.vector_to_stream(gr.sizeof_float, 4),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), [0, 0, 0, 16]))

    def test2(self):
        top = gr.top_block()
        sink = blocks.vector_sink_f()
        top.connect(
            blocks.vector_source_c(
                [1, 2, 3, 4, 5], False),
            marmote3.fft_and_magnitude(4, 1, True, [0, 0, 1, 0], False),
            blocks.vector_to_stream(gr.sizeof_float, 4),
            sink
        )
        top.run()
        print(sink.data())
        self.assertTrue(
            np.allclose(sink.data(), [9, 9, 9, 9, 16, 16, 16, 16]))


if __name__ == '__main__':
    gr_unittest.run(qa_fft_and_magnitude)
