#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio.filter import firdes, optfir
import numpy as np
from scipy import signal, optimize
import matplotlib.pyplot as plt
from marmote3.filters import *


def print_stats(taps):
    taps = np.array(taps)
    print("taps length:", len(taps))
    print("taps[0]:    ", taps[0])
    print("taps[mid-1]:", taps[len(taps) / 2 - 1])
    print("taps[mid]:  ", taps[len(taps) / 2])
    print("taps[mid+1]:", taps[len(taps) / 2 + 1])
    print("taps[-1]:   ", taps[-1])
    norm = get_norm(taps)
    print("max norm:", np.max(norm))
    print("avg norm:", np.mean(norm))
    print("sum norm:", np.sum(norm))


def print_cross_corrs(taps, stride=1):
    for i in range(0, len(taps), stride):
        s1 = np.pad(taps, (0, i), mode='constant')
        s2 = np.pad(taps, (i, 0), mode='constant')
        print(i, np.dot(s1, s2))


def print_taps(taps):
    print("[" + ", ".join(map(str, taps)) + "]")


def calc_self_ici_db(chan_sep, tx_taps, rx_taps):
    """Calculates the inter channel interference of own signal in dB."""
    rot = np.exp(2.0j * np.pi * np.arange(len(tx_taps)) / chan_sep)
    power = [np.max(get_norm(np.convolve((rot ** i) * tx_taps, rx_taps)))
             for i in range(chan_sep)]
    ici = np.max(power[1:]) / power[0]
    return 10.0 * math.log10(ici)


def calc_noise_ici_db(chan_sep, taps, skip=0):
    """Calculates the inter channel interference for noise."""
    power = get_norm(np.fft.fft(taps, n=101 * chan_sep))
    outofb = np.sum(power[51 + skip:-50 - skip])
    inband = np.sum(power) - outofb
    return 10.0 * math.log10(outofb / inband)


def calc_ripple_db(sep, taps, skip=0):
    """Calculates max ripple quotient in dB."""
    power = np.abs(np.fft.fft(taps, n=101 * sep))
    power = np.square(np.sum(np.reshape(power, (sep, 101)), axis=0))
    power = np.concatenate((power[-50 + skip:], power[0:51 - skip]))
    return 10.0 * math.log10(np.max(power) / np.min(power))


def calc_attenuation_db(sep, taps, skip=0):
    """Calculates the difference between the minimum ripple of
    the passband and the maximum ripple in the stop band."""
    power = np.abs(np.fft.fft(taps, n=101 * sep))
    a = min(np.min(power[:51 - skip]), np.min(power[-50 + skip:]))
    b = np.max(power[51 + skip:-50 - skip])
    return 20.0 * math.log10(b / a)


def calc_isi_db(symb_sep, taps):
    """Calculates the inter symbol interference in dB."""
    assert len(taps) % 2 == 1
    delay = (len(taps) - 1) / 2
    power = np.square(taps[delay % symb_sep::symb_sep])
    # print(power)
    isi = (np.sum(power) - power[delay / symb_sep]) / np.sum(power)
    return 10.0 * math.log10(isi)


def calc_papr_db(symb_sep, tx_taps):
    """Calculates the PAPR in dB."""
    extra = len(tx_taps) % symb_sep
    if extra > 0:
        tx_taps = np.pad(tx_taps, (0, symb_sep - extra), mode="constant")
    table = np.reshape(tx_taps, (len(tx_taps) / symb_sep, symb_sep))
    peak = np.max(np.sum(np.abs(table), axis=0)) ** 2.0
    power = np.sum(np.square(tx_taps)) / symb_sep
    return 10.0 * math.log10(peak / power)


def calc_nsr(chan_sep, tx_taps, rx_taps, nsr=0.1):
    """Assuming that the transmitted signal has the given
    noise to signal ratio, calculates the noise to signal
    ratio after applying the receive filter."""
    e1 = np.sum(get_norm(tx_taps))
    e2 = np.sum(get_norm(rx_taps))
    e3 = np.sum(get_norm(np.convolve(tx_taps, rx_taps)))
    return (e1 * chan_sep * nsr * e2) / e3


def calc_smoothness(taps):
    return np.sum(get_norm(np.convolve(taps, [-0.5, 1, -0.5])))


def plot_filters(taps_list):
    plt.figure()
    plt.title("Filter(s) taps in time")
    for taps in taps_list:
        plt.plot(taps)
    plt.show()


def plot_spectrum(sep, taps, title=None):
    taps = taps / ((sep * np.sum(np.square(taps))) ** 0.5)
    if title:
        plt.figure()
        plt.title(title)
    w, h = signal.freqz(taps, worN=100 * sep, whole=True)
    w = w[:200] / (2.0 * np.pi) * sep
    plt.plot(w, 20.0 * np.log10(np.abs(h[:200])))
    plt.plot(w, 20.0 * np.log10(np.abs(np.roll(h, 100)[:200])))
    plt.plot(w, 20.0 * np.log10(np.abs(np.roll(h, 200)[:200])))
    h = np.tile(np.sum(np.reshape(np.abs(h), [sep, 100]), axis=0), 2)
    plt.plot(w, 20.0 * np.log10(h))
    plt.gca().set_ylim([-80, 10])
    if title:
        plt.show()


def plot_graphs(chan_sep, symb_sep, tx_taps=None, rx_taps=None):
    if tx_taps is not None:
        print("TX filter")
        print_stats(tx_taps)
        for skip in range(0, 21, 2):
            print("TX chan noise ICI skip {}: {} dB".format(
                skip, calc_noise_ici_db(chan_sep, tx_taps, skip=skip)))
        for skip in range(0, 11, 2):
            print("TX symb ripple skip {}: {} dB".format(
                skip, calc_ripple_db(symb_sep, tx_taps, skip=skip)))
        print("TX symb PAPR: {} dB".format(calc_papr_db(symb_sep, tx_taps)))
        plot_spectrum(chan_sep, tx_taps, title="TX spectrum w/ channel sep")
        plot_spectrum(symb_sep, tx_taps, title="TX spectrum w/ symbol sep")

    if rx_taps is not None:
        print("RX filter")
        print_stats(rx_taps)
        for skip in range(0, 11, 2):
            print("RX chan noise ICI skip {}: {} dB".format(
                skip, calc_noise_ici_db(chan_sep, rx_taps, skip=skip)))
        for skip in range(0, 11, 2):
            print("RX symb ripple skip {}: {} dB".format(
                skip, calc_ripple_db(symb_sep, rx_taps, skip=skip)))
        plot_spectrum(chan_sep, rx_taps, title="RX spectrum w/ channel sep")
        plot_spectrum(symb_sep, rx_taps, title="RX spectrum w/ symbol sep")

    if tx_taps is not None and rx_taps is not None:
        print("TX+RX filter")
        txrx_taps = np.convolve(tx_taps, rx_taps)
        print_stats(txrx_taps)
        print("TX+RX symb ISI: {} dB:".format(calc_isi_db(symb_sep, txrx_taps)))
        print("TX+RX chan self ICI: {} dB:".format(calc_self_ici_db(chan_sep, tx_taps, rx_taps)))
        plot_spectrum(
            chan_sep, txrx_taps, title="TX+RX spectrum w/ channel sep")
        plot_spectrum(
            symb_sep, txrx_taps, title="TX+RX spectrum w/ symbol sep")

    if tx_taps is not None and rx_taps is not None:
        plot_filters([tx_taps, rx_taps])
    elif tx_taps is not None:
        plot_filters([tx_taps])
    elif rx_taps is not None:
        plot_filters([rx_taps])


def design_tx_filter():
    chan_sep = 20
    symb_sep = 22

    tx_len = 351
    trim_len = 100
    tx_taps = get_rrc_taps(chan_sep, symb_sep, length=tx_len - 2 * trim_len)
    tx_taps = np.pad(tx_taps, (trim_len, trim_len), mode='constant')

    tx_mid = (len(tx_taps) + 1) / 2

    def fun(vars2):
        tx_taps2 = np.concatenate([vars2, vars2[-2::-1]])
        goal = 0
        goal += max(calc_noise_ici_db(chan_sep, tx_taps2, skip=5), -50)
        goal += max(calc_noise_ici_db(chan_sep, tx_taps2, skip=20), -60)
        goal += 0.5 * max(calc_ripple_db(symb_sep, tx_taps2), 2)
        goal += max(calc_papr_db(symb_sep, tx_taps2), 4)
        return goal

    vars = optimize.minimize(
        fun,
        tx_taps[:tx_mid],
        method="SLSQP",
        options={"maxiter": 500, "ftol": 1e-4, "eps": 1e-6}
    )["x"]

    tx_taps = np.concatenate([vars, vars[-2::-1]])
    tx_taps = tx_taps / (np.sum(np.square(tx_taps)) ** 0.5)
    plot_graphs(chan_sep, symb_sep, tx_taps=tx_taps)
    print_taps(tx_taps)
    return tx_taps


def design_tx_filter2():
    chan_sep = 20
    symb_sep = 22

    tx_len = 351
    trim_len = 0
    # tx_taps = get_rrc_taps(chan_sep, symb_sep, length=tx_len - 2 * trim_len)
    tx_taps = signal.firwin(
        tx_len - 2 * trim_len, 1.0 / chan_sep, window="hamming")
    tx_taps = np.pad(tx_taps, (trim_len, trim_len), mode='constant')

    extra = len(tx_taps) % symb_sep
    if extra > 0:
        tx_taps = np.pad(tx_taps, (0, symb_sep - extra), mode="constant")
    table = np.reshape(tx_taps, (len(tx_taps) / symb_sep, symb_sep))
    table = np.sum(np.abs(table), axis=0)
    print(table)

    tx_mid = (len(tx_taps) + 1) / 2

    def fun(vars2):
        tx_taps2 = np.concatenate([vars2, vars2[-2::-1]])
        goal = 0
        # goal += max(calc_attenuation_db(chan_sep, tx_taps2, skip=5), -50)
        # goal += 0.1 * max(calc_noise_ici_db(chan_sep, tx_taps2, skip=20),
        # -60)
        goal += max(calc_papr_db(symb_sep, tx_taps2), 1)
        return goal

    vars = optimize.minimize(
        fun,
        tx_taps[:tx_mid],
        # method="SLSQP",
        options={"maxiter": 500, "disp": True}
    )["x"]

    tx_taps = np.concatenate([vars, vars[-2::-1]])
    tx_taps = tx_taps / (np.sum(np.square(tx_taps)) ** 0.5)
    plot_graphs(chan_sep, symb_sep, tx_taps=tx_taps)
    # print_taps(tx_taps)
    return tx_taps


def design_rx_filter(tx_taps):
    chan_sep = 20
    symb_sep = 22

    rx_len = 351
    trim_len = 0
    rx_taps = get_rrc_taps(chan_sep, symb_sep, length=rx_len - 2 * trim_len)
    rx_taps = np.pad(rx_taps, (trim_len, trim_len), mode='constant')

    rx_mid = (len(rx_taps) + 1) / 2

    def fun(vars2):
        rx_taps2 = np.concatenate([vars2, vars2[-2::-1]])
        goal = 0
        goal += max(calc_isi_db(symb_sep, np.convolve(tx_taps, rx_taps2)), -40)
        goal += max(calc_self_ici_db(chan_sep, tx_taps, rx_taps2), -50)
        goal += 0.1 * max(calc_noise_ici_db(chan_sep, rx_taps2, skip=20), -60)
        return goal

    vars = optimize.minimize(
        fun,
        rx_taps[:rx_mid],
        method="SLSQP",
        options={"maxiter": 500, "ftol": 1e-4, "eps": 1e-6}
    )["x"]

    rx_taps = np.concatenate([vars, vars[-2::-1]])
    rx_taps = rx_taps / (np.sum(np.square(rx_taps)) ** 0.5)
    plot_graphs(chan_sep, symb_sep, tx_taps=tx_taps, rx_taps=rx_taps)
    print_taps(rx_taps)
    return rx_taps


def design_txrx_filters(tx_taps, rx_taps):
    chan_sep = 20
    symb_sep = 22

    tx_mid = (len(tx_taps) + 1) / 2
    rx_mid = (len(rx_taps) + 1) / 2

    def fun(vars2):
        tx_taps2 = np.concatenate([vars2[:tx_mid], vars2[tx_mid - 2::-1]])
        rx_taps2 = np.concatenate([vars2[tx_mid:], vars2[-2:tx_mid - 1:-1]])
        goal = 0
        goal += 10 * max(calc_papr_db(symb_sep, tx_taps2), 4)
        goal += max(
            calc_isi_db(symb_sep, np.convolve(tx_taps2, rx_taps2)), -42)
        goal += max(calc_self_ici_db(chan_sep, tx_taps2, rx_taps2), -50)
        goal += max(calc_noise_ici_db(chan_sep, tx_taps2, skip=20), -60)
        goal += max(calc_noise_ici_db(chan_sep, rx_taps2, skip=20), -60)
        return goal

    vars = optimize.minimize(
        fun,
        np.concatenate([tx_taps[:tx_mid], rx_taps[:rx_mid]]),
        method="BFGS",
        options={"disp": True, "maxiter": 500, "gtol": 1e-4, "eps": 1e-5}
    )["x"]

    tx_taps = np.concatenate([vars[:tx_mid], vars[tx_mid - 2::-1]])
    rx_taps = np.concatenate([vars[tx_mid:], vars[-2:tx_mid - 1:-1]])

    tx_taps = tx_taps / (np.sum(np.square(tx_taps)) ** 0.5)
    rx_taps = rx_taps / (np.sum(np.square(rx_taps)) ** 0.5)
    plot_graphs(chan_sep, symb_sep, tx_taps=tx_taps, rx_taps=rx_taps)
    print_taps(tx_taps)
    print_taps(rx_taps)


if __name__ == "__main__":
    if True:
        tx_taps = design_tx_filter2()
    else:
        tx_taps = [
            -0.000192918490856, -0.000467358444114, -0.000331601785365, -0.000464556304054, -0.00070026332548, -0.000844568356425, -0.00102189083911, -0.00115864720111, -0.00137886536622, -0.00172025368848, -0.00204133467272, -0.00219895087333, -0.00261284574479, -0.00274276759055, -0.00313989847607, -0.00334647138135, -0.00365125841351, -0.00393821201272, -0.00436009539226, -0.0046173873393, -0.0047191981822, -0.00500443800804, -0.00518037638954, -0.00521139430608, -0.00533804957976, -0.00529831006956, -0.00524248412353, -0.00517850367142, -0.00503925391517, -0.00483000318354, -0.00451669419787, -0.00425848321245, -0.00387656019659, -0.00349707565187, -0.00309445887255, -0.00260387111356, -0.00218178952658, -0.00169763664077, -0.00113534374284, -0.000677362445717, -0.000294102661435, 0.000128509168692, 0.00045140089368, 0.000806621943449, 0.00097044034811, 0.00104709792692, 0.001315009073, 0.00125311544566, 0.00121663000487, 0.000945284115091, 0.000928827718889, 0.000471256897581, 0.000107436206376, -0.000308925696779, -0.000663305780184, -0.00109830214865, -0.00162934944921, -0.00221570686704, -0.00273156449309, -0.00318210587883, -0.00369085701744, -0.00403082776285, -0.00415402649921, -0.0044111402984, -0.00440989256274, -0.00447664554603, -0.00428194290343, -0.00400695682713, -0.00355534706404, -0.00304654511147, -0.00250701406556, -0.00179398709257, -0.000863806849511, 5.69559970475e-05, 0.0008155310156, 0.00196841524288, 0.00292133592169, 0.00397110823304, 0.00487141254447, 0.00578291976689, 0.00661985857401, 0.00726367413772, 0.00793416790995, 0.00841930464498, 0.00863627804363, 0.00860661056039, 0.00858458577551, 0.00839384137453, 0.00785818696498, 0.00727962806, 0.0064569350345, 0.00550087829264, 0.00439389920005, 0.00302631338794, 0.00168170820888, 0.000345915874381, -
                0.00126581796758, -0.00263021704305, -0.00413671658942, -0.00550913031452, -0.00682900986349, -0.00803019332751, -0.00905168595943, -0.0099182374892, -0.0106036627663, -0.0110161620229, -0.0112453542627, -0.0112059495927, -0.0110244313439, -0.0105172310672, -0.00973186009689, -0.00888706611281, -0.0077403939608, -0.0065533052339, -0.00506688054024, -0.00359647669775, -0.00205644269641, -0.000587532243566, 0.000961352686601, 0.0024470736534, 0.00369684256134, 0.00483305843841, 0.00579507127946, 0.00639964655589, 0.00678353144024, 0.00697961824721, 0.00664762285763, 0.00604072977028, 0.00514485928201, 0.00387079910629, 0.00241246666724, 0.000543870083545, -0.00153651604746, -0.00377936543465, -
                    0.00621792181465, -0.00863782286075, -0.0112407061763, -0.0136496508841, -0.0160676454117, -0.0180208363392, -0.0198301979738, -0.0213273359113, -0.0221374599436, -0.0224303451015, -0.0222678905902, -0.0210776677711, -0.0192787173283, -0.0165499132829, -0.012963441518, -0.00851897591345, -
                        0.00323791604728, 0.00318946780236, 0.0103067232444, 0.0184382225019, 0.0274208096228, 0.0371879976496, 0.0475591494218, 0.0586771411212, 0.0702702273749, 0.0821997969299, 0.0944631207804, 0.106820318674, 0.11917896835, 0.131367687164, 0.143293888286, 0.154839344306, 0.165786277736, 0.175897311879, 0.185350580855, 0.193631517396, 0.200786596744, 0.207040418074, 0.211811225171, 0.215367482643, 0.217430611643,
            0.21811393657, 0.217430611643, 0.215367482643, 0.211811225171, 0.207040418074, 0.200786596744, 0.193631517396, 0.185350580855, 0.175897311879, 0.165786277736, 0.154839344306, 0.143293888286, 0.131367687164, 0.11917896835, 0.106820318674, 0.0944631207804, 0.0821997969299, 0.0702702273749, 0.0586771411212, 0.0475591494218, 0.0371879976496, 0.0274208096228, 0.0184382225019, 0.0103067232444, 0.00318946780236, -0.00323791604728, -0.00851897591345, -0.012963441518, -0.0165499132829, -0.0192787173283, -0.0210776677711, -0.0222678905902, -0.0224303451015, -0.0221374599436, -0.0213273359113, -0.0198301979738, -0.0180208363392, -0.0160676454117, -0.0136496508841, -0.0112407061763, -0.00863782286075, -0.00621792181465, -0.00377936543465, -0.00153651604746, 0.000543870083545, 0.00241246666724, 0.00387079910629, 0.00514485928201, 0.00604072977028, 0.00664762285763, 0.00697961824721, 0.00678353144024, 0.00639964655589, 0.00579507127946, 0.00483305843841, 0.00369684256134, 0.0024470736534, 0.000961352686601, -0.000587532243566, -0.00205644269641, -0.00359647669775, -0.00506688054024, -0.0065533052339, -0.0077403939608, -0.00888706611281, -0.00973186009689, -0.0105172310672, -0.0110244313439, -0.0112059495927, -0.0112453542627, -0.0110161620229, -0.0106036627663, -0.0099182374892, -0.00905168595943, -0.00803019332751, -0.00682900986349, -0.00550913031452, -0.00413671658942, -0.00263021704305, -0.00126581796758, 0.000345915874381, 0.00168170820888, 0.00302631338794, 0.00439389920005, 0.00550087829264, 0.0064569350345, 0.00727962806, 0.00785818696498, 0.00839384137453, 0.00858458577551, 0.00860661056039, 0.00863627804363, 0.00841930464498, 0.00793416790995, 0.00726367413772, 0.00661985857401, 0.00578291976689, 0.00487141254447, 0.00397110823304, 0.00292133592169, 0.00196841524288, 0.0008155310156, 5.69559970475e-05, -0.000863806849511, -0.00179398709257, -0.00250701406556, -0.00304654511147, -0.00355534706404, -0.00400695682713, -0.00428194290343, -0.00447664554603, -0.00440989256274, -0.0044111402984, -0.00415402649921, -0.00403082776285, -0.00369085701744, -0.00318210587883, -0.00273156449309, -0.00221570686704, -0.00162934944921, -0.00109830214865, -0.000663305780184, -0.000308925696779, 0.000107436206376, 0.000471256897581, 0.000928827718889, 0.000945284115091, 0.00121663000487, 0.00125311544566, 0.001315009073, 0.00104709792692, 0.00097044034811, 0.000806621943449, 0.00045140089368, 0.000128509168692, -0.000294102661435, -0.000677362445717, -0.00113534374284, -0.00169763664077, -0.00218178952658, -0.00260387111356, -0.00309445887255, -0.00349707565187, -0.00387656019659, -0.00425848321245, -0.00451669419787, -0.00483000318354, -0.00503925391517, -0.00517850367142, -0.00524248412353, -0.00529831006956, -0.00533804957976, -0.00521139430608, -0.00518037638954, -0.00500443800804, -0.0047191981822, -0.0046173873393, -0.00436009539226, -0.00393821201272, -0.00365125841351, -0.00334647138135, -0.00313989847607, -0.00274276759055, -0.00261284574479, -0.00219895087333, -0.00204133467272, -0.00172025368848, -0.00137886536622, -0.00115864720111, -0.00102189083911, -0.000844568356425, -0.00070026332548, -0.000464556304054, -0.000331601785365, -0.000467358444114, -0.000192918490856]

    if False:
        rx_taps = design_rx_filter(tx_taps)
    else:
        rx_taps = [
            -0.000264433049136, -0.000355515189713, -0.000456128761294, -0.00057357163686, -0.000661478464066, -0.000778593387276, -0.000868489935777, -0.000935370907069, -0.000987770395703, -0.00100195124938, -0.000973678339323, -0.000903668208257, -0.000780925092135, -0.000611340520725, -0.000398602105493, -0.000109924079547, 0.000214797245108, 0.000615564041556, 0.00106341308487, 0.00158112290479, 0.00215248767161, 0.00276991110652, 0.00340090068947, 0.00406061864086, 0.00472696837356, 0.00537375750446, 0.00600521816423, 0.00660593644455, 0.00715755924485, 0.00764057897908, 0.0080523005721, 0.00837967682346, 0.00859796177602, 0.00871356729403, 0.00869407292691, 0.00855843168606, 0.00828374974153, 0.00786743005919, 0.00732935444773, 0.00663646371434, 0.00581217194846, 0.00487170674676, 0.00381817719379, 0.0026907114219, 0.00147185828063, 0.000217518242651, -0.00106759281813, -0.00234892474629, -0.00361011117689, -0.00482298450483, -0.00596226355447, -0.00698847911876, -0.00789377813358, -0.00865920975361, -0.00926301714539, -0.009671181259, -0.00987724787242, -0.00986905497404, -0.00964972177647, -0.00920278767051, -0.00853012706844, -0.00762851786711, -0.00653738873057, -0.00524517070691, -0.00379189212714, -0.00222921148842, -0.000552450336282, 0.00119734620187, 0.00297442500274, 0.00474200086491, 0.00645447452276, 0.00809576395161, 0.00960631957953, 0.0109695449933, 0.0121199986264, 0.0130732726403, 0.0137676096156, 0.0141905248251, 0.0143036489842, 0.0141088236382, 0.0135867877577, 0.0127433341199, 0.0115758371661, 0.0100851760147, 0.00831043396389, 0.00624486548465, 0.00395155742079, 0.00145628895605, -
                0.00119681690529, -0.00397244265632, -0.00679619531091, -0.00963466524288, -0.0123944100846, -0.0150504520183, -0.0175220139412, -0.0197324842544, -0.0216440493744, -0.023202181914, -0.0243263605117, -0.0249948631875, -0.0251691117885, -0.0248159517138, -0.0239081153336, -0.0224834375347, -0.0205143741447, -0.0180238587831, -0.0150454089097, -0.0116194550076, -0.00781612349076, -0.00366099560972, 0.000758385759475, 0.00538886933295, 0.0101406397454, 0.0149153971346, 0.0196494852325, 0.0242553234282, 0.0285968889331, 0.0326077746572, 0.0362010783526, 0.0392725279597, 0.0417444605743, 0.0435255892037, 0.0445711735002, 0.0448037097291, 0.0442171646045, 0.0427605059411, 0.0404512447591, 0.0372895761406, 0.0332879341255, 0.0285031967508, 0.023003633293, 0.0168380609837, 0.0101161320003, 0.00294279522579, -
                    0.00461091773825, -0.0123163793271, -0.0201497814408, -0.0279100484617, -0.0354471171615, -0.0426361386363, -0.0493118171173, -0.055310625332, -0.0605076148226, -0.0647839214081, -0.068001752013, -0.0700317520689, -0.0708056632433, -0.0702291876108, -0.0682828520669, -0.0648843090211, -0.0600306555337, -0.0537381936316, -0.0460089860078, -0.0369180846788, -
                        0.0265498581519, -0.0149465407512, -
                            0.00228084809633, 0.0113618766919, 0.0257829450945, 0.0408460257798, 0.0563478734801, 0.0721246570738, 0.087956165539, 0.10364225714, 0.118982242241, 0.133785090149, 0.147875687102, 0.161053176033, 0.173120025009, 0.183948318398, 0.193377154536, 0.201288705347, 0.20756265953, 0.212089977341, 0.214846830382,
            0.215802912648, 0.214846830382, 0.212089977341, 0.20756265953, 0.201288705347, 0.193377154536, 0.183948318398, 0.173120025009, 0.161053176033, 0.147875687102, 0.133785090149, 0.118982242241, 0.10364225714, 0.087956165539, 0.0721246570738, 0.0563478734801, 0.0408460257798, 0.0257829450945, 0.0113618766919, -0.00228084809633, -0.0149465407512, -0.0265498581519, -0.0369180846788, -0.0460089860078, -0.0537381936316, -0.0600306555337, -0.0648843090211, -0.0682828520669, -0.0702291876108, -0.0708056632433, -0.0700317520689, -0.068001752013, -0.0647839214081, -0.0605076148226, -0.055310625332, -0.0493118171173, -0.0426361386363, -0.0354471171615, -0.0279100484617, -0.0201497814408, -0.0123163793271, -0.00461091773825, 0.00294279522579, 0.0101161320003, 0.0168380609837, 0.023003633293, 0.0285031967508, 0.0332879341255, 0.0372895761406, 0.0404512447591, 0.0427605059411, 0.0442171646045, 0.0448037097291, 0.0445711735002, 0.0435255892037, 0.0417444605743, 0.0392725279597, 0.0362010783526, 0.0326077746572, 0.0285968889331, 0.0242553234282, 0.0196494852325, 0.0149153971346, 0.0101406397454, 0.00538886933295, 0.000758385759475, -0.00366099560972, -0.00781612349076, -0.0116194550076, -0.0150454089097, -0.0180238587831, -0.0205143741447, -0.0224834375347, -0.0239081153336, -0.0248159517138, -0.0251691117885, -0.0249948631875, -0.0243263605117, -0.023202181914, -0.0216440493744, -0.0197324842544, -0.0175220139412, -0.0150504520183, -0.0123944100846, -0.00963466524288, -0.00679619531091, -0.00397244265632, -0.00119681690529, 0.00145628895605, 0.00395155742079, 0.00624486548465, 0.00831043396389, 0.0100851760147, 0.0115758371661, 0.0127433341199, 0.0135867877577, 0.0141088236382, 0.0143036489842, 0.0141905248251, 0.0137676096156, 0.0130732726403, 0.0121199986264, 0.0109695449933, 0.00960631957953, 0.00809576395161, 0.00645447452276, 0.00474200086491, 0.00297442500274, 0.00119734620187, -0.000552450336282, -0.00222921148842, -0.00379189212714, -0.00524517070691, -0.00653738873057, -0.00762851786711, -0.00853012706844, -0.00920278767051, -0.00964972177647, -0.00986905497404, -0.00987724787242, -0.009671181259, -0.00926301714539, -0.00865920975361, -0.00789377813358, -0.00698847911876, -0.00596226355447, -0.00482298450483, -0.00361011117689, -0.00234892474629, -0.00106759281813, 0.000217518242651, 0.00147185828063, 0.0026907114219, 0.00381817719379, 0.00487170674676, 0.00581217194846, 0.00663646371434, 0.00732935444773, 0.00786743005919, 0.00828374974153, 0.00855843168606, 0.00869407292691, 0.00871356729403, 0.00859796177602, 0.00837967682346, 0.0080523005721, 0.00764057897908, 0.00715755924485, 0.00660593644455, 0.00600521816423, 0.00537375750446, 0.00472696837356, 0.00406061864086, 0.00340090068947, 0.00276991110652, 0.00215248767161, 0.00158112290479, 0.00106341308487, 0.000615564041556, 0.000214797245108, -0.000109924079547, -0.000398602105493, -0.000611340520725, -0.000780925092135, -0.000903668208257, -0.000973678339323, -0.00100195124938, -0.000987770395703, -0.000935370907069, -0.000868489935777, -0.000778593387276, -0.000661478464066, -0.00057357163686, -0.000456128761294, -0.000355515189713, -0.000264433049136]

    if False:
        design_txrx_filters(tx_taps, rx_taps)
