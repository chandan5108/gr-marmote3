#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function
import math
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
import torch
import sigtorch
from torch.autograd import Function, Variable, gradcheck


def half_firwin(numtaps, cutoff, window="hamming"):
    """Returns the half taps for the given bandpass filter."""
    assert numtaps % 2 == 1
    taps = signal.firwin(numtaps, cutoff, window=window)
    taps /= math.sqrt(np.sum(np.square(taps)))
    return torch.from_numpy(taps[(numtaps - 1) / 2:])


def half_to_full(sig):
    """Converts a half vector to a full symmetric vector."""
    idx = range(sig.size(0)) + range(sig.size(0) - 1, 0, -1)
    idx = torch.LongTensor(idx)
    if isinstance(sig, Variable):
        idx = Variable(idx)
    return torch.index_select(sig, 0, idx)


def normalize(sig):
    return sig / torch.sum(sig * sig, dim=0, keepdim=True).sqrt()


def full_to_shft(sig):
    """Rotates the signal so that sig[0] goes to the middle."""
    assert sig.size(0) % 2 == 1
    mid = (sig.size(0) + 1) / 2
    return torch.cat((sig.narrow(0, mid, mid - 1), sig.narrow(0, 0, mid)), dim=0)


def log10(sig):
    return (1.0 / math.log(10)) * torch.log(sig)


def power_db(sig, start=0.0, end=1.0):
    return 10.0 * log10(torch.sum(sig * sig, dim=0))


def shft_peak_db(taps, symb_sep):
    mid = ((taps.size(0) - 1) / 2) % symb_sep
    taps = sigtorch.pad(taps, 0, symb_sep - (taps.size(0) % symb_sep))
    size = [taps.size(0) / symb_sep, symb_sep] + list(taps.size()[1:])
    peak = torch.sum(torch.abs(taps.view(size)), dim=0)
    peak = 20.0 * log10(peak * math.sqrt(symb_sep))
    if mid > 0:
        peak = torch.cat((peak.narrow(0, mid, symb_sep - mid),
                          peak.narrow(0, 0, mid)), dim=0)
    return peak


def supress_db(spec, passband, stopband):
    assert 0.0 <= passband and passband <= stopband and stopband <= 0.5
    passband = int(passband * spec.size(0))
    stopband = int(stopband * spec.size(0))
    halfband = int(0.5 * spec.size(0))
    power = spec * spec
    a = torch.min(power.narrow(0, 0, passband))
    b = torch.max(power.narrow(0, stopband, halfband))
    return 10.0 * log10(b / a)


def shft_isi_db(taps, symb_sep):
    pass

if True:
    taps1 = torch.DoubleTensor([1, 2, 3, 4, 3, 2, 1])
    taps2 = torch.DoubleTensor([10, 11, 10])
    print(sigtorch.convolve_rr(taps1, taps2))

    half1 = Variable(
        half_firwin(249, 1.0 / 20, window="flattop"),
        requires_grad=True)
    half2 = Variable(
        half_firwin(249, 1.0 / 22, window="flattop"),
        requires_grad=True)
    opti = torch.optim.Adam([half1, half2], lr=1e-5)

    for step in range(1001):
        opti.zero_grad()

        taps1 = normalize(half_to_full(half1))
        taps2 = normalize(half_to_full(half2))
        spec1 = sigtorch.sfft(taps1)
        spec2 = sigtorch.sfft(taps2)
        taps1 = full_to_shft(taps1)
        taps2 = full_to_shft(taps2)
        peak1 = shft_peak_db(taps1, 22)
        supr1 = supress_db(spec1, 0.7 / 40, 1.3 / 40)

        loss = peak1.max().clamp(min=3)
        loss = loss + 0.1 * supr1.clamp(min=-80)

        loss.backward()
        opti.step()
        # half1.data /= (half_to_full(half1) *
        # half_to_full(half1)).sum().sqrt().data

        print(step, peak1.max().data[
            0], supr1.data[0], loss.data[0], )

        if step % 1000 == 0:
            # plt.plot(peak1.data.numpy())
            # plt.show()
            plt.plot(taps1.data.numpy())
            plt.plot(spec1.data.numpy())
            plt.show()
