#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function, division

import numpy as np
import matplotlib
import matplotlib.pyplot as plt


def load_spectrogram(filename, fft_len=256, scale=4, shift=60):
    """Returns the spectogram as numpy array of shape (millisecs, fft_len)."""

    data = np.fromfile(filename, dtype=np.int8)
    length = (len(data) // fft_len) * fft_len
    if len(data) != length:
        data = data[:length]
    data = np.reshape(data, [length // fft_len, fft_len])
    data = data.astype(np.float32) * (1.0 / scale) - shift
    return data


def show_spectrogram(data, start=0, length=2000, png_file=None):
    """Shows/saves the spectogram at the given start offset."""

    start = max(min(start, data.shape[0] - 1), 0)
    length = max(min(length, data.shape[0] - start), 0)

    clim = (-80, -40)
    fig, ax = plt.subplots(1, 1)
    img = ax.imshow(data[start:start + length, :]
                    .transpose(), aspect='auto', clim=clim)
    ax.invert_yaxis()
    ax.set_xticklabels([str(int(t + start)) for t in ax.get_xticks()])
    cbar = plt.colorbar(img, ax=ax)
    cbar.locator = matplotlib.ticker.MultipleLocator(5.0)
    cbar.update_ticks()
    cbar.ax.set_yticklabels(
        ['{:.0f} dB'.format(clim[0] + t * (clim[1] - clim[0]))
         for t in cbar.ax.get_yticks()])

    if png_file is None:
        plt.show()
    else:
        fig.savefig(png_file, dpi=150, bbox_inches='tight', pad_inches=0.25)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('filename', nargs="*", help="path to spectrogram.dat")
    parser.add_argument('--show', type=int, default=None,
                        help="show the spectrum at the given offset")
    args = parser.parse_args()

    for filename in args.filename:
        data = load_spectrogram(filename)
        if args.show is not None:
            show_spectrogram(data, start=args.show)
