/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PACKET_FLOW_MERGER_IMPL_H
#define INCLUDED_MARMOTE3_PACKET_FLOW_MERGER_IMPL_H

#include <marmote3/packet_flow_merger.h>

namespace gr {
namespace marmote3 {

class packet_flow_merger_impl : public packet_flow_merger {
private:
  const int itemsize;
  const int nflows;
  const int max_output_len;

  std::vector<int> input_lens; // expected input lengths
  int done;

  std::vector<unsigned int> packets;
  gr::thread::mutex mutex;

public:
  packet_flow_merger_impl(int itemsize, int nflows, int max_output_len);
  ~packet_flow_merger_impl();

  void forecast(int noutput_items,
                gr_vector_int &ninput_items_required) override;

  int general_work(int noutput_items, gr_vector_int &ninput_items,
                   gr_vector_const_void_star &input_items,
                   gr_vector_void_star &output_items) override;

  void collect_block_report(ModemReport *modem_report) override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_FLOW_MERGER_IMPL_H */
