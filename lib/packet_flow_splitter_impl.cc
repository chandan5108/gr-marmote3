/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_flow_splitter_impl.h"
#include <boost/thread/locks.hpp>
#include <gnuradio/block_detail.h>
#include <gnuradio/buffer.h>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <marmote3/marmote3.pb.h>
#include <sstream>

namespace gr {
namespace marmote3 {

packet_flow_splitter::sptr packet_flow_splitter::make(int itemsize, int nflows,
                                                      int max_output_len) {
  return gnuradio::get_initial_sptr(
      new packet_flow_splitter_impl(itemsize, nflows, max_output_len));
}

packet_flow_splitter_impl::packet_flow_splitter_impl(int itemsize, int nflows,
                                                     int max_output_len)
    : gr::block("packet_flow_splitter", gr::io_signature::make(1, 1, itemsize),
                gr::io_signature::make(nflows, nflows, itemsize)),
      itemsize(itemsize), max_output_len(max_output_len), input_len(1),
      last_port(0), dropped(0) {
  if (max_output_len <= 0)
    throw std::invalid_argument(
        "packet_flow_splitter: invalid maximum output length");

  if (itemsize <= 0)
    throw std::invalid_argument("packet_flow_splitter: invalid item size");

  if (nflows <= 0)
    throw std::invalid_argument(
        "packet_flow_splitter: invalid number of output flows");

  packets.resize(nflows);

  set_tag_propagation_policy(TPP_DONT);
  set_min_noutput_items(max_output_len);
  for (int i = 0; i < nflows; i++)
    set_min_output_buffer(i, 2 * max_output_len);
}

packet_flow_splitter_impl::~packet_flow_splitter_impl() {}

void packet_flow_splitter_impl::forecast(int noutput_items,
                                         gr_vector_int &ninput_items_required) {
  if (false) {
    gr::buffer_reader *reader = detail()->input(0).get();
    boost::unique_lock<boost::mutex> guard(*reader->mutex());

    std::stringstream msg;
    msg << "split forecast len:" << input_len << " nout:" << noutput_items
        << " rd:" << nitems_read(0) << " wr0:" << nitems_written(0)
        << " wr1:" << nitems_written(1) << std::endl;
    std::cout << msg.str();
  }

  ninput_items_required[0] = input_len;
}

int packet_flow_splitter_impl::general_work(
    int noutput_items, gr_vector_int &ninput_items,
    gr_vector_const_void_star &input_items, gr_vector_void_star &output_items) {

  if (false) {
    std::stringstream msg;
    msg << "split gen_work len:" << input_len << " nin:" << ninput_items[0]
        << " nout:" << noutput_items << " rd:" << nitems_read(0)
        << " wr0:" << nitems_written(0) << " wr1:" << nitems_written(1)
        << std::endl;
    std::cout << msg.str();
  }

  std::vector<tag_t> tags;
  uint64_t r = nitems_read(0);
  int c = ninput_items[0];
  pmt::pmt_t input_srcid;
  pmt::pmt_t input_dict;

  // get the length and dictionaries
  get_tags_in_range(tags, 0, r, r + 1, PMT_PACKET_LEN);
  if (tags.size() == 1 && pmt::is_integer(tags[0].value)) {
    input_len = std::max(1, (int)pmt::to_long(tags[0].value));
    if (c < input_len)
      return 0;
    input_srcid = tags[0].srcid;

    get_tags_in_range(tags, 0, r, r + 1, PMT_PACKET_DICT);
    if (tags.size() == 1 && pmt::is_dict(tags[0].value)) {
      input_dict = tags[0].value;
    } else {
      input_dict = PMT_EMPTY_DICT;

      // drop the dictionary on error
      if (tags.size() != 0)
        std::cout
            << "####### packet_flow_splitter: invalid packet dictionary\n";
    }
  } else {
    // skip input items on missing packet length
    std::cout << "####### packet_flow_splitter: missing packet length\n";

    get_tags_in_range(tags, 0, r + 1, r + c);
    if (tags.size() > 0)
      c = std::max(1, (int)(tags[0].offset - r));

    consume(0, c);
    return 0;
  }

  int port = -1;
  int aval = input_len - 1;
  for (int i = 0; i < output_items.size(); i++) {
    unsigned int p = (last_port + 1 + i) % output_items.size();
    gr::buffer *writer = detail()->output(p).get();
    boost::unique_lock<boost::mutex> guard(*writer->mutex());
    int a = writer->space_available();
    if (a > aval) {
      aval = a;
      port = p;
    }
  }

  if (0 <= port && port < output_items.size()) {
    last_port = port;

    std::memcpy(output_items[port], input_items[0], input_len * itemsize);
    add_item_tag(port, nitems_written(port), PMT_PACKET_LEN,
                 pmt::from_long(input_len), input_srcid);
    add_item_tag(port, nitems_written(port), PMT_PACKET_DICT, input_dict,
                 input_srcid);

    produce(port, input_len);
    consume(0, input_len);

    {
      gr::thread::scoped_lock lock(mutex);
      packets[port] += 1;
    }

    if (false) {
      std::stringstream msg;
      msg << "split produced port:" << port << " len:" << input_len
          << " rd:" << nitems_read(0) << " wr0:" << nitems_written(0)
          << " wr1:" << nitems_written(1) << std::endl;
      std::cout << msg.str();
    }
    input_len = 1; // next packet might be smaller
  } else {
    consume(0, input_len);
    {
      gr::thread::scoped_lock lock(mutex);
      dropped += 1;
    }
  }

  return WORK_CALLED_PRODUCE;
}

bool packet_flow_splitter_impl::start() {
  for (int i = 0; i < detail()->noutputs(); i++) {
    std::stringstream msg;
    msg << alias() << " output " << i << " buffer items "
        << max_output_buffer(i) << std::endl;
    std::cout << msg.str();
  }
  return block::start();
}

void packet_flow_splitter_impl::collect_block_report(
    ModemReport *modem_report) {
  PacketFlowReport *report = modem_report->mutable_packet_flow_splitter();
  gr::thread::scoped_lock lock(mutex);

  *report->mutable_packets() = {packets.begin(), packets.end()};
  report->set_dropped(dropped);

  for (int i = 0; i < packets.size(); i++)
    packets[i] = 0;
  dropped = 0;
}

} /* namespace marmote3 */
} /* namespace gr */
