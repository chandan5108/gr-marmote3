/* -*- c++ -*- */
/*
 * Copyright 2014-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "polyphase_chan_filter_ccf_impl.h"
#include "xmmintrin.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace marmote3 {

polyphase_chan_filter_ccf::sptr
polyphase_chan_filter_ccf::make(int vlen, const std::vector<float> &taps,
                                int stride) {
  return gnuradio::get_initial_sptr(
      new polyphase_chan_filter_ccf_impl(vlen, taps, stride));
}

polyphase_chan_filter_ccf_impl::polyphase_chan_filter_ccf_impl(
    int vlen, const std::vector<float> &taps_arg, int stride)
    : gr::sync_decimator(
          "polyphase_chan_filter_ccf",
          gr::io_signature::make(1, 1, sizeof(gr_complex)),
          gr::io_signature::make(1, 1, sizeof(gr_complex) * vlen), stride),
      vlen(vlen), stride(stride), taps_len(taps_arg.size()) {
  if (stride <= 0)
    throw std::invalid_argument(
        "polyphase_chan_filter_ccf: stride must be positive");

  if (vlen < 4)
    throw std::invalid_argument(
        "polyphase_chan_filter_ccf: vector length must be at least 4");

  if (taps_len <= 0)
    throw std::invalid_argument("polyphase_chan_filter_ccf: empty taps list");

  if (posix_memalign((void **)&taps, 16, (taps_len + 7) * sizeof(float)) != 0)
    throw std::bad_alloc();

  for (int i = 0; i < taps_len + 7; ++i)
    taps[i] = i < taps_arg.size() ? taps_arg[i] : 0.0f;

  set_history(1 + (taps_len - 1) / stride * stride);
}

polyphase_chan_filter_ccf_impl::~polyphase_chan_filter_ccf_impl() {
  free(taps);
  taps = NULL;
}

inline void polyphase_chan_filter_ccf_impl::method1(const gr_complex *in,
                                                    gr_complex *out) {
  for (int i = 0; i < vlen; i++) {
    gr_complex a(0.0f, 0.0f);
    for (int j = i; j < taps_len; j += vlen)
      a += taps[j] * in[j];

    out[i] = a;
  }
}

inline void polyphase_chan_filter_ccf_impl::method2(const gr_complex *in,
                                                    gr_complex *out) {
  for (int i = 0; i < vlen; i += 4) {
    __m128 a0 = _mm_setzero_ps();
    __m128 a1 = _mm_setzero_ps();

    for (int j = i; j < taps_len; j += vlen) {
      __m128 t0 = _mm_loadu_ps(taps + j);                    // t0, t1, t2, t3
      __m128 b0 = _mm_loadu_ps((const float *)(in + j));     // r0, i0, r1, i1
      __m128 b1 = _mm_loadu_ps((const float *)(in + j + 2)); // r2, i2, r3, i3
      a0 = _mm_add_ps(
          a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
      a1 = _mm_add_ps(
          a1, _mm_mul_ps(b1, _mm_shuffle_ps(t0, t0, 0xfa))); // t2, t2, t3, t3
    }

    // we will overwrite memory when i > vlen - 4, but that is fine
    _mm_storeu_ps((float *)(out + i), a0);
    _mm_storeu_ps((float *)(out + i + 2), a1);
  }
}

inline void polyphase_chan_filter_ccf_impl::method3(const gr_complex *in,
                                                    gr_complex *out) {
  int i = 0;
  for (; i + 4 <= vlen; i += 4) {
    __m128 a0 = _mm_setzero_ps();
    __m128 a1 = _mm_setzero_ps();

    for (int j = i; j < taps_len; j += vlen) {
      __m128 t0 = _mm_loadu_ps(taps + j);                    // t0, t1, t2, t3
      __m128 b0 = _mm_loadu_ps((const float *)(in + j));     // r0, i0, r1, i1
      __m128 b1 = _mm_loadu_ps((const float *)(in + j + 2)); // r2, i2, r3, i3
      a0 = _mm_add_ps(
          a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
      a1 = _mm_add_ps(
          a1, _mm_mul_ps(b1, _mm_shuffle_ps(t0, t0, 0xfa))); // t2, t2, t3, t3
    }

    _mm_storeu_ps((float *)(out + i), a0);
    _mm_storeu_ps((float *)(out + i + 2), a1);
  }

  for (; i + 2 <= vlen; i += 2) {
    __m128 a0 = _mm_setzero_ps();

    for (int j = i; j < taps_len; j += vlen) {
      __m128 t0 = _mm_loadu_ps(taps + j);                // t0, t1
      __m128 b0 = _mm_loadu_ps((const float *)(in + j)); // r0, i0, r1, i1
      a0 = _mm_add_ps(
          a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
    }

    _mm_storeu_ps((float *)(out + i), a0);
  }

  for (; i < vlen; i++) {
    gr_complex a(0.0f, 0.0f);
    for (int j = i; j < taps_len; j += vlen)
      a += taps[j] * in[j];

    out[i] = a;
  }
}

inline void polyphase_chan_filter_ccf_impl::method_0mod8(const gr_complex *in,
                                                         gr_complex *out) {
  assert(vlen % 8 == 0);

  for (int i = 0; i < vlen; i += 8) {
    __m128 a0 = _mm_setzero_ps();
    __m128 a1 = _mm_setzero_ps();
    __m128 a2 = _mm_setzero_ps();
    __m128 a3 = _mm_setzero_ps();

    for (int j = i; j < taps_len; j += vlen) {
      __m128 t0 = _mm_loadu_ps(taps + j);                    // t0, t1, t2, t3
      __m128 t1 = _mm_loadu_ps(taps + j + 4);                // t4, t5, t6, t7
      __m128 b0 = _mm_loadu_ps((const float *)(in + j));     // r0, i0, r1, i1
      __m128 b1 = _mm_loadu_ps((const float *)(in + j + 2)); // r2, i2, r3, i3
      __m128 b2 = _mm_loadu_ps((const float *)(in + j + 4)); // r4, i4, r5, i5
      __m128 b3 = _mm_loadu_ps((const float *)(in + j + 6)); // r6, i6, r7, i7

      a0 = _mm_add_ps(
          a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
      a1 = _mm_add_ps(
          a1, _mm_mul_ps(b1, _mm_shuffle_ps(t0, t0, 0xfa))); // t2, t2, t3, t3
      a2 = _mm_add_ps(
          a2, _mm_mul_ps(b2, _mm_shuffle_ps(t1, t1, 0x50))); // t4, t4, t5, t5
      a3 = _mm_add_ps(
          a3, _mm_mul_ps(b3, _mm_shuffle_ps(t1, t1, 0xfa))); // t6, t6, t7, t7
    }

    _mm_storeu_ps((float *)(out + i), a0);
    _mm_storeu_ps((float *)(out + i + 2), a1);
    _mm_storeu_ps((float *)(out + i + 4), a2);
    _mm_storeu_ps((float *)(out + i + 6), a3);
  }
}

inline void polyphase_chan_filter_ccf_impl::method_2mod8(const gr_complex *in,
                                                         gr_complex *out) {
  assert(vlen % 8 == 2);

  for (int i = 0; i < vlen - 2; i += 8) {
    __m128 a0 = _mm_setzero_ps();
    __m128 a1 = _mm_setzero_ps();
    __m128 a2 = _mm_setzero_ps();
    __m128 a3 = _mm_setzero_ps();

    for (int j = i; j < taps_len; j += vlen) {
      __m128 t0 = _mm_loadu_ps(taps + j);                    // t0, t1, t2, t3
      __m128 t1 = _mm_loadu_ps(taps + j + 4);                // t4, t5, t6, t7
      __m128 b0 = _mm_loadu_ps((const float *)(in + j));     // r0, i0, r1, i1
      __m128 b1 = _mm_loadu_ps((const float *)(in + j + 2)); // r2, i2, r3, i3
      __m128 b2 = _mm_loadu_ps((const float *)(in + j + 4)); // r4, i4, r5, i5
      __m128 b3 = _mm_loadu_ps((const float *)(in + j + 6)); // r6, i6, r7, i7

      a0 = _mm_add_ps(
          a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
      a1 = _mm_add_ps(
          a1, _mm_mul_ps(b1, _mm_shuffle_ps(t0, t0, 0xfa))); // t2, t2, t3, t3
      a2 = _mm_add_ps(
          a2, _mm_mul_ps(b2, _mm_shuffle_ps(t1, t1, 0x50))); // t4, t4, t5, t5
      a3 = _mm_add_ps(
          a3, _mm_mul_ps(b3, _mm_shuffle_ps(t1, t1, 0xfa))); // t6, t6, t7, t7
    }

    _mm_storeu_ps((float *)(out + i), a0);
    _mm_storeu_ps((float *)(out + i + 2), a1);
    _mm_storeu_ps((float *)(out + i + 4), a2);
    _mm_storeu_ps((float *)(out + i + 6), a3);
  }

  // last 2
  __m128 a0 = _mm_setzero_ps();

  for (int j = vlen - 2; j < taps_len; j += vlen) {
    __m128 t0 = _mm_loadu_ps(taps + j);                // t0, t1
    __m128 b0 = _mm_loadu_ps((const float *)(in + j)); // r0, i0, r1, i1

    a0 = _mm_add_ps(
        a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
  }

  _mm_storeu_ps((float *)(out + vlen - 2), a0);
}

inline void polyphase_chan_filter_ccf_impl::method_4mod8(const gr_complex *in,
                                                         gr_complex *out) {
  assert(vlen % 8 == 4);

  for (int i = 0; i < vlen - 4; i += 8) {
    __m128 a0 = _mm_setzero_ps();
    __m128 a1 = _mm_setzero_ps();
    __m128 a2 = _mm_setzero_ps();
    __m128 a3 = _mm_setzero_ps();

    for (int j = i; j < taps_len; j += vlen) {
      __m128 t0 = _mm_loadu_ps(taps + j);                    // t0, t1, t2, t3
      __m128 t1 = _mm_loadu_ps(taps + j + 4);                // t4, t5, t6, t7
      __m128 b0 = _mm_loadu_ps((const float *)(in + j));     // r0, i0, r1, i1
      __m128 b1 = _mm_loadu_ps((const float *)(in + j + 2)); // r2, i2, r3, i3
      __m128 b2 = _mm_loadu_ps((const float *)(in + j + 4)); // r4, i4, r5, i5
      __m128 b3 = _mm_loadu_ps((const float *)(in + j + 6)); // r6, i6, r7, i7

      a0 = _mm_add_ps(
          a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
      a1 = _mm_add_ps(
          a1, _mm_mul_ps(b1, _mm_shuffle_ps(t0, t0, 0xfa))); // t2, t2, t3, t3
      a2 = _mm_add_ps(
          a2, _mm_mul_ps(b2, _mm_shuffle_ps(t1, t1, 0x50))); // t4, t4, t5, t5
      a3 = _mm_add_ps(
          a3, _mm_mul_ps(b3, _mm_shuffle_ps(t1, t1, 0xfa))); // t6, t6, t7, t7
    }

    _mm_storeu_ps((float *)(out + i), a0);
    _mm_storeu_ps((float *)(out + i + 2), a1);
    _mm_storeu_ps((float *)(out + i + 4), a2);
    _mm_storeu_ps((float *)(out + i + 6), a3);
  }

  // last 4
  __m128 a0 = _mm_setzero_ps();
  __m128 a1 = _mm_setzero_ps();

  for (int j = vlen - 4; j < taps_len; j += vlen) {
    __m128 t0 = _mm_loadu_ps(taps + j);                    // t0, t1, t2, t3
    __m128 b0 = _mm_loadu_ps((const float *)(in + j));     // r0, i0, r1, i1
    __m128 b1 = _mm_loadu_ps((const float *)(in + j + 2)); // r2, i2, r3, i3

    a0 = _mm_add_ps(
        a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
    a1 = _mm_add_ps(
        a1, _mm_mul_ps(b1, _mm_shuffle_ps(t0, t0, 0xfa))); // t2, t2, t3, t3
  }

  _mm_storeu_ps((float *)(out + vlen - 4), a0);
  _mm_storeu_ps((float *)(out + vlen - 2), a1);
}

inline void polyphase_chan_filter_ccf_impl::method_6mod8(const gr_complex *in,
                                                         gr_complex *out) {
  assert(vlen % 8 == 6);

  for (int i = 0; i < vlen - 6; i += 8) {
    __m128 a0 = _mm_setzero_ps();
    __m128 a1 = _mm_setzero_ps();
    __m128 a2 = _mm_setzero_ps();
    __m128 a3 = _mm_setzero_ps();

    for (int j = i; j < taps_len; j += vlen) {
      __m128 t0 = _mm_loadu_ps(taps + j);                    // t0, t1, t2, t3
      __m128 t1 = _mm_loadu_ps(taps + j + 4);                // t4, t5, t6, t7
      __m128 b0 = _mm_loadu_ps((const float *)(in + j));     // r0, i0, r1, i1
      __m128 b1 = _mm_loadu_ps((const float *)(in + j + 2)); // r2, i2, r3, i3
      __m128 b2 = _mm_loadu_ps((const float *)(in + j + 4)); // r4, i4, r5, i5
      __m128 b3 = _mm_loadu_ps((const float *)(in + j + 6)); // r6, i6, r7, i7

      a0 = _mm_add_ps(
          a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
      a1 = _mm_add_ps(
          a1, _mm_mul_ps(b1, _mm_shuffle_ps(t0, t0, 0xfa))); // t2, t2, t3, t3
      a2 = _mm_add_ps(
          a2, _mm_mul_ps(b2, _mm_shuffle_ps(t1, t1, 0x50))); // t4, t4, t5, t5
      a3 = _mm_add_ps(
          a3, _mm_mul_ps(b3, _mm_shuffle_ps(t1, t1, 0xfa))); // t6, t6, t7, t7
    }

    _mm_storeu_ps((float *)(out + i), a0);
    _mm_storeu_ps((float *)(out + i + 2), a1);
    _mm_storeu_ps((float *)(out + i + 4), a2);
    _mm_storeu_ps((float *)(out + i + 6), a3);
  }

  // last 6
  __m128 a0 = _mm_setzero_ps();
  __m128 a1 = _mm_setzero_ps();
  __m128 a2 = _mm_setzero_ps();

  for (int j = vlen - 6; j < taps_len; j += vlen) {
    __m128 t0 = _mm_loadu_ps(taps + j);                    // t0, t1, t2, t3
    __m128 t1 = _mm_loadu_ps(taps + j + 4);                // t4, t5
    __m128 b0 = _mm_loadu_ps((const float *)(in + j));     // r0, i0, r1, i1
    __m128 b1 = _mm_loadu_ps((const float *)(in + j + 2)); // r2, i2, r3, i3
    __m128 b2 = _mm_loadu_ps((const float *)(in + j + 4)); // r4, i4, r5, i5

    a0 = _mm_add_ps(
        a0, _mm_mul_ps(b0, _mm_shuffle_ps(t0, t0, 0x50))); // t0, t0, t1, t1
    a1 = _mm_add_ps(
        a1, _mm_mul_ps(b1, _mm_shuffle_ps(t0, t0, 0xfa))); // t2, t2, t3, t3
    a2 = _mm_add_ps(
        a2, _mm_mul_ps(b2, _mm_shuffle_ps(t1, t1, 0x50))); // t4, t4, t5, t5
  }

  _mm_storeu_ps((float *)(out + vlen - 6), a0);
  _mm_storeu_ps((float *)(out + vlen - 4), a1);
  _mm_storeu_ps((float *)(out + vlen - 2), a2);
}

int polyphase_chan_filter_ccf_impl::work(int noutput_items,
                                         gr_vector_const_void_star &input_items,
                                         gr_vector_void_star &output_items) {
  monitor_work(); // record extra performance values

  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  if (vlen % 8 == 0) {
    for (int n = 0; n < noutput_items; n += 1) {
      method_0mod8(in, out);
      in += stride;
      out += vlen;
    }
  } else if (vlen % 8 == 2) {
    for (int n = 0; n < noutput_items; n += 1) {
      method_2mod8(in, out);
      in += stride;
      out += vlen;
    }
  } else if (vlen % 8 == 4) {
    for (int n = 0; n < noutput_items; n += 1) {
      method_4mod8(in, out);
      in += stride;
      out += vlen;
    }
  } else if (vlen % 8 == 6) {
    for (int n = 0; n < noutput_items; n += 1) {
      method_6mod8(in, out);
      in += stride;
      out += vlen;
    }
  } else {
    // create garbage at the output
    for (int n = 0; n < noutput_items - 1; n += 1) {
      method2(in, out);
      in += stride;
      out += vlen;
    }

    // overwrite it in the last pass
    if (noutput_items > 0)
      method3(in, out);
  }

  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
