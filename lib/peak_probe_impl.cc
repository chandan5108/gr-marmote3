/* -*- c++ -*- */
/*
 * Copyright 2013, 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "peak_probe_impl.h"
#include <emmintrin.h>
#include <gnuradio/io_signature.h>
#include <marmote3/marmote3.pb.h>
#include <pmmintrin.h>
#include <xmmintrin.h>

namespace gr {
namespace marmote3 {

peak_probe::sptr peak_probe::make() {
  return gnuradio::get_initial_sptr(new peak_probe_impl());
}

peak_probe_impl::peak_probe_impl()
    : gr::sync_block("peak_probe",
                     gr::io_signature::make(1, 1, sizeof(gr_complex)),
                     gr::io_signature::make(0, 0, 0)),
      curr_peak(0.0f), energy(0.0), maximum(0.0f) {
  set_output_multiple(8);
  set_tag_propagation_policy(TPP_DONT);
}

peak_probe_impl::~peak_probe_impl() {}

int peak_probe_impl::work(int noutput_items,
                          gr_vector_const_void_star &input_items,
                          gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];

  __m128 max0 = _mm_setzero_ps();
  __m128 max1 = _mm_setzero_ps();
  __m128 sum0 = _mm_setzero_ps();
  __m128 sum1 = _mm_setzero_ps();

  for (int i = 0; i < noutput_items; i += 8) {
    __m128 a0 = _mm_load_ps((float *)in);
    a0 = _mm_mul_ps(a0, a0);
    __m128 a1 = _mm_load_ps((float *)(in + 2));
    a1 = _mm_mul_ps(a1, a1);
    __m128 b0 = _mm_hadd_ps(a0, a1);

    __m128 a2 = _mm_load_ps((float *)(in + 4));
    a2 = _mm_mul_ps(a2, a2);
    __m128 a3 = _mm_load_ps((float *)(in + 6));
    a3 = _mm_mul_ps(a3, a3);
    __m128 b1 = _mm_hadd_ps(a2, a3);

    max0 = _mm_max_ps(max0, b0);
    sum0 = _mm_add_ps(sum0, b0);
    max1 = _mm_max_ps(max1, b1);
    sum1 = _mm_add_ps(sum1, b1);

    in += 8;
  }

  max0 = _mm_max_ps(max0, max1);
  sum0 = _mm_add_ps(sum0, sum1);

  max0 = _mm_max_ps(max0, _mm_shuffle_ps(max0, max0, _MM_SHUFFLE(2, 3, 0, 1)));
  sum0 = _mm_add_ps(sum0, _mm_shuffle_ps(sum0, sum0, _MM_SHUFFLE(2, 3, 0, 1)));

  max0 = _mm_max_ps(max0, _mm_shuffle_ps(max0, max0, _MM_SHUFFLE(1, 0, 3, 2)));
  sum0 = _mm_add_ps(sum0, _mm_shuffle_ps(sum0, sum0, _MM_SHUFFLE(1, 0, 3, 2)));

  float max, sum;
  _mm_store_ss(&max, max0);
  _mm_store_ss(&sum, sum0);

  // update it atomically
  {
    gr::thread::scoped_lock lock(mutex);

    curr_peak = std::max(curr_peak, max);

    energy += sum;
    maximum = std::max(maximum, max);
    samples += noutput_items;
  }

  return noutput_items;
}

float peak_probe_impl::get_peak() {
  float peak;
  {
    gr::thread::scoped_lock lock(mutex);
    peak = curr_peak;
    curr_peak = 0.0f;
  }
  return std::sqrt(peak);
}

void peak_probe_impl::collect_block_report(ModemReport *modem_report) {
  PeakProbeReport *report = modem_report->mutable_single_peak_probe();
  gr::thread::scoped_lock lock(mutex);

  report->set_samples(samples);
  if (samples > 0) {
    float average_db = 10.0 * std::log10(std::max(energy / samples, 1e-40));
    float maximum_db = 10.0f * std::log10(std::max(maximum, 1e-40f));

    PeakProbeCarrier *report2 = report->add_carriers();
    report2->set_average_db(average_db);
    report2->set_maximum_db(maximum_db);
  }

  energy = 0.0;
  maximum = 0.0f;
  samples = 0;
}

} // namespace marmote3
} /* namespace gr */
