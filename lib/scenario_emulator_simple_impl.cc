/* -*- c++ -*- */
/*
 * Copyright 2017 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "scenario_emulator_simple_impl.h"
#include <gnuradio/io_signature.h>

#include <iostream>

namespace gr {
namespace marmote3 {

scenario_emulator_simple::sptr
scenario_emulator_simple::make(float fs, float fdopp, float update_interval,
                               float K_fact, int no_sines) {
  return gnuradio::get_initial_sptr(new scenario_emulator_simple_impl(
      fs, fdopp, update_interval, K_fact, no_sines));
}

scenario_emulator_simple_impl::scenario_emulator_simple_impl(
    float fs, float fdopp, float update_interval, float K_fact, int no_sines)
    : gr::sync_block("scenario_emulator_simple",
                     gr::io_signature::make(1, 1, sizeof(gr_complex)),
                     gr::io_signature::make(1, 1, sizeof(gr_complex))),
      samp_rate(fs), fdopp_abs(fdopp), update_interval(update_interval),
      K_fact(K_fact), no_sines(no_sines), norm_doppler(float(fdopp) / fs),
      update_samples(update_interval * fs) 
{
  fg = new fading_generator_sos(norm_doppler, Jakes);
  fg->set_K_factor(K_fact);
  update_cnt = update_samples;

  std::cerr << "update every " << update_samples << " samples" << std::endl;
  std::cerr << "norm. Doppler " << norm_doppler << std::endl;
}

scenario_emulator_simple_impl::~scenario_emulator_simple_impl() { delete fg; }

int scenario_emulator_simple_impl::work(int noutput_items,
                                        gr_vector_const_void_star &input_items,
                                        gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  gr_complex fading_samp;

  fg->generate(&fading_samp, 1);

  for (int ii = 0; ii < noutput_items; ++ii) {
    if (update_cnt == update_samples) { // mimic the crappy Colosseum
      update_cnt = 0;
      fg->shift_time_offset(update_samples);
      fg->generate(&fading_samp, 1);
    }
    out[ii] = fading_samp * in[ii];
  }

  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
