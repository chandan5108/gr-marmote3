/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_HEADER_COMPRESSOR_IMPL_H
#define INCLUDED_MARMOTE3_HEADER_COMPRESSOR_IMPL_H

#include <marmote3/header_compressor.h>
#include <marmote3/headers.h>
#include <unordered_set>

namespace gr {
namespace marmote3 {

class header_compressor_impl : public header_compressor {
private:
  const int mode;
  const bool print_headers;
  const pmt::pmt_t in_port;
  const pmt::pmt_t out_port;

  unsigned int valid_messages = 0;
  unsigned int invalid_messages = 0;
  unsigned long uncompressed_bytes = 0;
  unsigned long compressed_bytes = 0;
  gr::thread::mutex mutex;

  std::unordered_set<mmt_header_t> observed_headers;

  pmt::pmt_t process_mode0(const uint8_t *data_ptr, int data_len);
  pmt::pmt_t process_mode1(const uint8_t *data_ptr, int data_len);
  pmt::pmt_t process_mode2(const uint8_t *data_ptr, int data_len);
  pmt::pmt_t process_mode3(const uint8_t *data_ptr, int data_len);
  pmt::pmt_t process_mode4(const uint8_t *data_ptr, int data_len);
  void process_header(const mmt_header_t &mmt, const uint8_t *raw_ptr,
                      int raw_len);

  int mgen_seqno; // output from process_mode4

public:
  header_compressor_impl(int mode, bool print_headers);
  ~header_compressor_impl();

  void receive(pmt::pmt_t msg);
  void collect_block_report(ModemReport *modem_report) override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_HEADER_COMPRESSOR_IMPL_H */
