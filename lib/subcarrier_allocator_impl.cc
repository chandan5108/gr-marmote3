/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "subcarrier_allocator_impl.h"
#include <algorithm>
#include <cassert>
#include <chrono>
#include <cstring>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <marmote3/marmote3.pb.h>
#include <sstream>

namespace gr {
namespace marmote3 {

subcarrier_allocator::sptr subcarrier_allocator::make(
    int vlen, const std::vector<int> &active_channels,
    const std::vector<int> &backup_channels, int max_xmitting,
    vector_peak_probe::sptr cca_peak_probe,
    const std::vector<int> &subcarrier_map, modem_sender2::sptr modem_sender,
    bool debug_scheduler) {
  return gnuradio::get_initial_sptr(new subcarrier_allocator_impl(
      vlen, active_channels, backup_channels, max_xmitting, cca_peak_probe,
      subcarrier_map, modem_sender, debug_scheduler));
}

subcarrier_allocator_impl::subcarrier_allocator_impl(
    int vlen, const std::vector<int> &active_channels,
    const std::vector<int> &backup_channels, int max_xmitting,
    vector_peak_probe::sptr cca_peak_probe,
    const std::vector<int> &subcarrier_map, modem_sender2::sptr modem_sender,
    bool debug_scheduler)
    : gr::block("subcarrier_allocator",
                gr::io_signature::make(1, 1, sizeof(gr_complex)),
                gr::io_signature::make(1, 1, sizeof(gr_complex) * vlen)),
      vlen(vlen), max_xmitting(max_xmitting), cca_peak_probe(cca_peak_probe),
      subcarrier_map(subcarrier_map), modem_sender(modem_sender),
      debug_scheduler(debug_scheduler), next_channel(0) {
  if (vlen <= 0)
    throw std::invalid_argument(
        "subcarrier_allocator: vector length must be positive");

  if (max_xmitting < 1)
    throw std::invalid_argument(
        "subcarrier_allocator: max xmitting must be positive");

  if (subcarrier_map.size() != 0 && subcarrier_map.size() != vlen)
    throw std::invalid_argument("subcarrier_allocator: invalid subcarrier map");

  if (cca_peak_probe != NULL && subcarrier_map.size() != vlen)
    throw std::invalid_argument("subcarrier_allocator: missing subcarrier map");

  if (cca_peak_probe != NULL)
    std::cout << "subcarrier_allocator: CCA peak probe is enabled\n";

  if (modem_sender != NULL)
    std::cout << "subcarrier_allocator: modem sender is enabled\n";

  for (int i = 0; i < vlen; i++)
    channels.push_back(channel_t(i));

  set_active_channels(active_channels);
  set_backup_channels(backup_channels);

  set_relative_rate(1.0);
  set_tag_propagation_policy(TPP_DONT);
}

subcarrier_allocator_impl::~subcarrier_allocator_impl() {}

void subcarrier_allocator_impl::forecast(int noutput_items,
                                         gr_vector_int &ninput_items_required) {
  ninput_items_required[0] = 0; // we are always ready to produce
}

long subcarrier_allocator_impl::get_input_long(const pmt::pmt_t &dict,
                                               const pmt::pmt_t &key,
                                               long defval) {
  pmt::pmt_t v = pmt::dict_ref(dict, key, pmt::get_PMT_EOF());
  return pmt::is_integer(v) ? pmt::to_long(v) : defval;
}

bool subcarrier_allocator_impl::load_samples_into(channel_t &chan) {
  const tag_t *tag = NULL;
  for (;;) {
    if (tags.empty()) {
      if (input_consumed < input_length) {
        std::cout << "####### subcarrier_allocator: missing packet_len tag\n";
        input_consumed = input_length;
      }
      return false;
    }

    tag = &tags.front();
    int diff = tag->offset - (nitems_read(0) + input_consumed);
    if (diff < 0) {
      std::cout << "####### subcarrier_allocator: overlapping packets\n";
      tags.erase(tags.begin());
      continue;
    } else if (diff > 0) {
      std::cout << "####### subcarrier_allocator: extra samples after packet\n";
      input_consumed += std::min(diff, input_length - input_consumed);
    }
    break;
  }

  int next_length = std::max(1, (int)pmt::to_long(tag->value));
  if (input_consumed + next_length > input_length)
    return false;

  unsigned long current_pos = nitems_written(0);

  // load data outside of lock
  assert(chan.xmitting == false && chan.position == chan.samples.size());
  chan.samples.resize(next_length); // TODO: unnecessary initialization
  std::memcpy(chan.samples.data(), input_data + input_consumed,
              sizeof(gr_complex) * next_length);
  chan.position = chan.samples.size();

  // for debugging
  double mgen2 = 0.0;
  double schd2 = 0.0;
  int flow_id = -1;

  {
    gr::thread::scoped_lock lock(mutex, boost::try_to_lock);
    if (!lock)
      return false;

    while (!inhibits.empty()) {
      // expired inhibit
      if (inhibits.front().stop_pos <= current_pos) {
        inhibits.pop_front();
        continue;
      }

      // active inhibit, message would go into the inhibit zone
      if (inhibits.front().start_pos <= current_pos + next_length)
        return false;
      else
        break;
    }

    // enable tranmission for loaded data
    chan.position = 0;
    chan.xmitting = true;

    std::vector<tag_t> dicts;
    get_tags_in_range(dicts, 0, tag->offset, tag->offset + 1, PMT_PACKET_DICT);

    flow_key_t flow_key;
    flow_key.channel = chan.channel;
    if (dicts.size() == 1 && pmt::is_dict(dicts[0].value)) {
      flow_key.source = get_input_long(dicts[0].value, PMT_PACKET_SRC, -1);
      flow_key.destination = get_input_long(dicts[0].value, PMT_PACKET_DST, -1);
      flow_key.mcs = get_input_long(dicts[0].value, PMT_PACKET_MCS, -1);

      if (debug_scheduler) {
        mgen2 = get_input_long(dicts[0].value, PMT_LOAD_TIME, 0.0) * 1e-6;
        schd2 = get_input_long(dicts[0].value, PMT_SCHD_TIME, 0.0) * 1e-6;
        flow_id = get_input_long(dicts[0].value, PMT_FLOW_ID, -1);
      }
    }

    flow_val_t &flow_val = flows[flow_key];
    flow_val.frames += 1;
    flow_val.length_min = std::min(flow_val.length_min, next_length);
    flow_val.length_max = std::max(flow_val.length_max, next_length);
    flow_val.cca_bin_min = std::min(flow_val.cca_bin_min, chan.cca_bin);
    flow_val.cca_bin_max = std::max(flow_val.cca_bin_max, chan.cca_bin);

    input_consumed += next_length;
    tags.erase(tags.begin());
  }

  // for debugging the modem_sender2 scheduler
  if (debug_scheduler) {
    double now2 =
        0.000001 * std::chrono::duration_cast<std::chrono::microseconds>(
                       std::chrono::system_clock::now().time_since_epoch())
                       .count();

    std::stringstream str;
    str << alias() << ": allocate flow " << flow_id << std::fixed
        << std::setprecision(3) << " mgen " << mgen2 << " schd " << schd2
        << " elapsed " << (schd2 - mgen2) << " now2 " << now2 << " elapsed "
        << (now2 - mgen2) << std::endl;
    std::cout << str.str();
  }

  if (modem_sender != NULL)
    modem_sender->received_inflight_packet();

  return true;
}

int subcarrier_allocator_impl::general_work(
    int noutput_items, gr_vector_int &ninput_items,
    gr_vector_const_void_star &input_items, gr_vector_void_star &output_items) {
  input_consumed = 0;
  input_length = ninput_items[0];
  input_data = (const gr_complex *)input_items[0];

  int total_xmitting = 0;
  int total_active = 0;
  for (int i = 0; i < vlen; i++) {
    if (channels[i].xmitting)
      total_xmitting += 1;
    if (channels[i].active)
      total_active += 1;
  }

  if (input_length > 0) {
    if (cca_peak_probe != NULL) {
      assert(subcarrier_map.size() == vlen);
      std::vector<unsigned int> bins = cca_peak_probe->get_power_bins();
      for (int i = 0; i < vlen; i++) {
        int sub = subcarrier_map[i];
        int bin = (0 <= sub && sub < bins.size()) ? bins[sub] : -1;
        channels[i].cca_bin = bin;
      }
    }

    tags.clear();
    get_tags_in_window(tags, 0, 0, input_length, PMT_PACKET_LEN);

    for (int i = 0; i < vlen; i++) {
      if (total_xmitting >= std::min(max_xmitting, total_active))
        break;

      int j = (next_channel + i) % vlen;
      if (channels[j].active && !channels[j].xmitting) {
        if (!load_samples_into(channels[j])) {
          next_channel = j;
          break; // not enough input, wait for more
        } else
          total_xmitting += 1;
      }
    }
  }

  gr_complex *out = (gr_complex *)output_items[0];
  std::memset(out, 0, sizeof(gr_complex) * noutput_items * vlen);

  int n = 0;
  while (n < noutput_items) {
    int m = noutput_items - n;
    for (int i = 0; i < vlen; i++) {
      channel_t &chan = channels[i];
      if (chan.xmitting) {
        int a = chan.samples.size() - chan.position;
        assert(a > 0);
        m = std::min(m, a);
      }
    }

    for (int i = 0; i < vlen; i++) {
      channel_t &chan = channels[i];
      if (chan.xmitting) {
        for (int j = 0; j < m; j++)
          out[i + j * vlen] = chan.samples[chan.position + j];

        chan.position += m;
        if (chan.position >= chan.samples.size()) {
          chan.xmitting = false;
          if (chan.active && !load_samples_into(chan))
            noutput_items = n + m; // wait for more input
        }
      }
    }

    out += m * vlen;
    n += m;
  }

  consume(0, input_consumed);
  return noutput_items;
}

void subcarrier_allocator_impl::collect_block_report(
    ModemReport *modem_report) {
  SubcarrierAllocatorReport *report =
      modem_report->mutable_subcarrier_allocator();

  for (int chan = 0; chan < vlen; chan++) {
    if (channels[chan].active)
      report->add_active_channels(chan);
    if (channels[chan].backup)
      report->add_backup_channels(chan);
  }

  gr::thread::scoped_lock lock(mutex);

  // report flows
  for (auto pair : flows) {
    SubcarrierAllocatorFlow *flow = report->add_flows();
    flow->set_channel(pair.first.channel);
    flow->set_source(pair.first.source);
    flow->set_destination(pair.first.destination);
    flow->set_mcs(pair.first.mcs);
    flow->set_frames(pair.second.frames);
    flow->set_length_min(pair.second.length_min);
    flow->set_length_max(pair.second.length_max);
    flow->set_cca_bin_min(pair.second.cca_bin_min);
    flow->set_cca_bin_max(pair.second.cca_bin_max);

    // be careful, we are under a lock
    if (false) {
      std::stringstream msg;
      msg << "subcarrier_allocator: chan " << pair.first.channel
          << " cca bin min " << pair.second.cca_bin_min << " max "
          << pair.second.cca_bin_max << std::endl;
      std::cout << msg.str();
    }
  }
  flows.clear();

  // report and update inhibits
  if (last_report_pos <= 0) {
    last_report_sec = modem_report->time();
    last_report_pos = nitems_written(0);
    pos_per_sec = 0.0;
  } else {
    double new_report_sec = modem_report->time();
    unsigned long new_report_pos = nitems_written(0);

    double elapsed_sec = new_report_sec - last_report_sec;
    if (elapsed_sec <= 0.001) // just to be safe use 1 ms
      pos_per_sec = 0.0;
    else
      pos_per_sec = (new_report_pos - last_report_pos) / elapsed_sec;

    last_report_sec = new_report_sec;
    last_report_pos = new_report_pos;
  }

  for (auto inhibit : inhibits) {
    SubcarrierAllocatorInhibit *item = report->add_inhibits();
    item->set_start_time(inhibit.start_sec);
    item->set_duration(inhibit.stop_sec - inhibit.start_sec);
    inhibit.start_pos = inhibit_sec_to_pos(inhibit.start_sec);
    inhibit.stop_pos = inhibit_sec_to_pos(inhibit.stop_sec);
  }
}

void subcarrier_allocator_impl::process_block_command(
    const ModemCommand *modem_command) {
  if (modem_command->has_subcarrier_allocator()) {
    const SubcarrierAllocatorCommand &command =
        modem_command->subcarrier_allocator();

    auto active_channels = command.active_channels();
    auto backup_channels = command.backup_channels();

    for (int chan = 0; chan < vlen; chan++) {
      channels[chan].active =
          std::find(active_channels.begin(), active_channels.end(), chan) !=
          active_channels.end();

      channels[chan].backup =
          std::find(backup_channels.begin(), backup_channels.end(), chan) !=
          backup_channels.end();
    }
  }

  if (modem_command->has_subcarrier_inhibit()) {
    const SubcarrierInhibitCommand &command =
        modem_command->subcarrier_inhibit();

    gr::thread::scoped_lock lock(mutex);

    inhibits.clear();
    for (const SubcarrierAllocatorInhibit &inhibit : command.inhibits()) {
      double start_time = inhibit.start_time();
      double stop_time = start_time + inhibit.duration();
      inhibits.emplace_back(start_time, stop_time,
                            inhibit_sec_to_pos(start_time),
                            inhibit_sec_to_pos(stop_time));
    }
  }
}

void subcarrier_allocator_impl::set_active_channels(
    const std::vector<int> &active_channels) {
  for (int chan = 0; chan < vlen; chan++) {
    channels[chan].active =
        std::find(active_channels.begin(), active_channels.end(), chan) !=
        active_channels.end();
  }
}

void subcarrier_allocator_impl::set_backup_channels(
    const std::vector<int> &backup_channels) {
  for (int chan = 0; chan < vlen; chan++) {
    channels[chan].backup =
        std::find(backup_channels.begin(), backup_channels.end(), chan) !=
        backup_channels.end();
  }
}

} /* namespace marmote3 */
} /* namespace gr */
