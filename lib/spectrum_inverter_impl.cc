/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "spectrum_inverter_impl.h"
#include <gnuradio/io_signature.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

spectrum_inverter::sptr spectrum_inverter::make(bool invert) {
  return gnuradio::get_initial_sptr(new spectrum_inverter_impl(invert));
}

spectrum_inverter_impl::spectrum_inverter_impl(bool invert)
    : gr::sync_block("spectrum_inverter",
                     gr::io_signature::make(1, 1, sizeof(gr_complex)),
                     gr::io_signature::make(1, 1, sizeof(gr_complex))),
      invert(invert) {
  set_output_multiple(256);
}

spectrum_inverter_impl::~spectrum_inverter_impl() {}

int spectrum_inverter_impl::work(int noutput_items,
                                 gr_vector_const_void_star &input_items,
                                 gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  if (invert)
    volk_32fc_conjugate_32fc(out, in, noutput_items);
  else
    std::memcpy(out, in, noutput_items * sizeof(gr_complex));

  return noutput_items;
}

void spectrum_inverter_impl::set_invert(bool inv) { invert = inv; }

} /* namespace marmote3 */
} /* namespace gr */
