/* -*- c++ -*- */
/*
 * Copyright 2014-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "vector_fir_filter_ccc_impl.h"
#include <gnuradio/io_signature.h>

#ifdef __SSE3__
#include "pmmintrin.h"
#include "xmmintrin.h"
#endif

namespace gr {
namespace marmote3 {

vector_fir_filter_ccc::sptr
vector_fir_filter_ccc::make(int vlen, const std::vector<gr_complex> &taps) {
  return gnuradio::get_initial_sptr(new vector_fir_filter_ccc_impl(vlen, taps));
}

vector_fir_filter_ccc_impl::vector_fir_filter_ccc_impl(
    int vlen, const std::vector<gr_complex> &taps)
    : gr::sync_block("vector_fir_filter_ccc",
                     gr::io_signature::make(1, 1, sizeof(gr_complex) * vlen),
                     gr::io_signature::make(1, 1, sizeof(gr_complex) * vlen)),
      vlen(vlen), taps(taps) {
  if (vlen <= 0 || vlen % 4 != 0)
    throw std::invalid_argument(
        "vector_fir_filter_ccc: vector length must be divisible by 4");

  if (taps.size() <= 0)
    throw std::invalid_argument("vector_fir_filter_ccc: empty taps list");

  set_history(taps.size());
}

vector_fir_filter_ccc_impl::~vector_fir_filter_ccc_impl() {}

int vector_fir_filter_ccc_impl::work(int noutput_items,
                                     gr_vector_const_void_star &input_items,
                                     gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  for (int n = 0; n < noutput_items; n++) {
#ifdef __SSE3__
    work_sse_a(in, out);
#else
    work_cpu_a(in, out);
#endif
    in += vlen;
    out += vlen;
  }

  return noutput_items;
}

void vector_fir_filter_ccc_impl::work_cpu_a(const gr_complex *in,
                                            gr_complex *out) {
  gr_complex c = taps[0];
  for (int j = 0; j < vlen; j += 4) {
    out[j + 0] = c * in[j + 0];
    out[j + 1] = c * in[j + 1];
    out[j + 2] = c * in[j + 2];
    out[j + 3] = c * in[j + 3];
  }
  in += vlen;

  for (int i = 1; i < taps.size(); i++) {
    c = taps[i];
    for (int j = 0; j < vlen; j += 4) {
      out[j + 0] += c * in[j + 0];
      out[j + 1] += c * in[j + 1];
      out[j + 2] += c * in[j + 2];
      out[j + 3] += c * in[j + 3];
    }
    in += vlen;
  }
}

void vector_fir_filter_ccc_impl::work_cpu_b(const gr_complex *in,
                                            gr_complex *out) {
  for (int i = 0; i < vlen; i += 4) {
    const gr_complex *in2 = in;
    gr_complex c[4];

    gr_complex t = taps[0];
    c[0] = t * in[0];
    c[1] = t * in[1];
    c[2] = t * in[2];
    c[3] = t * in[3];
    in2 += vlen;

    for (int j = 1; j < taps.size(); j++) {
      t = taps[j];
      c[0] += t * in2[0];
      c[1] += t * in2[1];
      c[2] += t * in2[2];
      c[3] += t * in2[3];
      in2 += vlen;
    }

    out[0] = c[0];
    out[1] = c[1];
    out[2] = c[2];
    out[3] = c[3];

    out += 4;
    in += 4;
  }
}

#ifdef __SSE3__
void vector_fir_filter_ccc_impl::work_sse_a(const gr_complex *in,
                                            gr_complex *out) {
  __m128 zero = _mm_setzero_ps();
  for (int i = 0; i < vlen; i += 4) {
    _mm_store_ps((float *)(out + i), zero);
    _mm_store_ps((float *)(out + i + 2), zero);
  }

  for (int i = 0; i < taps.size(); i++) {
    const gr_complex c = taps[i];
    __m128 xa =
        _mm_set_ps(c.imag(), c.real(), c.imag(), c.real()); // ar, ai, ar, ai
    __m128 xb = _mm_shuffle_ps(xa, xa, 0xB1);               // ai, ar, ar, ai

    for (int j = 0; j < vlen; j += 4) {
      __m128 y1 = _mm_load_ps((float *)(in + j)); // br, bi, cr, ci
      __m128 y2 = _mm_load_ps((float *)(in + j + 2));
      __m128 z1 = _mm_load_ps((float *)(out + j));
      __m128 z2 = _mm_load_ps((float *)(out + j + 2));

      __m128 y1a = _mm_moveldup_ps(y1); // br, br, cr, cr
      __m128 y2a = _mm_moveldup_ps(y2);
      __m128 y1b = _mm_movehdup_ps(y1); // bi, bi, ci, ci
      __m128 y2b = _mm_movehdup_ps(y2);

      __m128 t1a = _mm_mul_ps(xa, y1a); // ar*br, ai*br, ar*cr, ai*cr
      __m128 t2a = _mm_mul_ps(xa, y2a);
      __m128 t1b = _mm_mul_ps(xb, y1b); // ai*bi, ar*bi, ai*ci, ar*ci
      __m128 t2b = _mm_mul_ps(xb, y2b);

      __m128 u1 = _mm_addsub_ps(
          t1a, t1b); // ar*br-ai*bi, ai*br+ar*bi, ar*cr-ai*ci, ai*cr+ar*ci
      __m128 u2 = _mm_addsub_ps(t2a, t2b);

      _mm_store_ps((float *)(out + j), _mm_add_ps(z1, u1));
      _mm_store_ps((float *)(out + j + 2), _mm_add_ps(z2, u2));
    }

    in += vlen;
  }
}
#endif

} /* namespace marmote3 */
} /* namespace gr */
