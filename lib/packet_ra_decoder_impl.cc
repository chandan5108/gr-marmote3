/* -*- c++ -*- */
/*
 * Copyright 2013-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_ra_decoder_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <sstream>

namespace gr {
namespace marmote3 {

packet_ra_decoder::sptr packet_ra_decoder::make(int data_len, int code_len,
                                                int full_passes,
                                                int half_passes) {
  return gnuradio::get_initial_sptr(
      new packet_ra_decoder_impl(data_len, code_len, full_passes, half_passes));
}

packet_ra_decoder_impl::packet_ra_decoder_impl(int data_len, int code_len,
                                               int full_passes, int half_passes)
    : tagged_stream_block2(
          "packet_ra_decoder", gr::io_signature::make(1, 1, sizeof(int8_t)),
          gr::io_signature::make(1, 1, sizeof(uint8_t)), data_len),
      data_len(data_len), code_len(code_len), full_passes(full_passes),
      half_passes(half_passes) {
  if (data_len <= 0)
    throw std::invalid_argument("packet_ra_decoder::invalid data length");

  if (full_passes < 0 || half_passes < 0)
    throw std::invalid_argument("packet_ra_decoder::invalid passes argument");

  if (code_len != -1)
    get_decoder(data_len, code_len);
}

packet_ra_decoder_impl::~packet_ra_decoder_impl() {
  for (auto iter = decoders.begin(); iter != decoders.end(); iter++)
    delete iter->second;
}

fec_ra_decoder *packet_ra_decoder_impl::get_decoder(int data_len2,
                                                    int code_len2) {
  uint64_t key = ((uint64_t)data_len2 << 32) + (uint32_t)code_len2;

  auto iter = decoders.find(key);
  if (iter != decoders.end())
    return iter->second;

  std::stringstream str;
  str << alias() << ": creating decoder for " << data_len2 << "/" << code_len2
      << std::endl;
  std::cout << str.str();

  fec_ra_decoder *decoder =
      new fec_ra_decoder(data_len2, code_len2, full_passes, half_passes);
  decoders.insert(std::pair<const uint64_t, fec_ra_decoder *>(key, decoder));
  return decoder;
}

int packet_ra_decoder_impl::work(int noutput_items, gr_vector_int &ninput_items,
                                 gr_vector_const_void_star &input_items,
                                 gr_vector_void_star &output_items) {
  int data_len2 = data_len;
  int code_len2 = code_len;

  if (code_len == -1) {
    if (!has_input_long(0, PMT_DATA_LEN) ||
        !has_input_long(0, PMT_PACKET_MCS)) {
      std::cout << "####### packet_ra_decoder: missing MCS parameters\n";
      return 0;
    }

    data_len2 = get_input_long(0, PMT_DATA_LEN, -1);
    code_len2 = get_mcscheme(get_input_long(0, PMT_PACKET_MCS, -1))
                    .get_code_len(data_len2);
  }

  if (ninput_items[0] != 8 * code_len2) {
    std::stringstream msg;
    msg << "####### packet_ra_decoder: invalid input length " << ninput_items[0]
        << ", expecting " << (8 * code_len2) << " soft bits" << std::endl;
    std::cout << msg.str();
    return 0;
  } else if (data_len2 > noutput_items) {
    std::cout << "####### packet_ra_decoder: output buffer is too small\n";
    return 0;
  } else if (data_len2 <= 0 || code_len2 < data_len2 ||
             code_len2 > 4 * data_len2) {
    std::cout << "##### packet_ra_decoder: invalid data/code length\n";
    return 0;
  }

  fec_ra_decoder *decoder = get_decoder(data_len2, code_len2);
  decoder->decode((const int8_t *)input_items[0], (uint8_t *)output_items[0]);
  return data_len2;
}

} /* namespace marmote3 */
} /* namespace gr */
