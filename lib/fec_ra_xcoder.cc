/* -*- c++ -*- */
/*
 * Copyright 2013-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cassert>
#include <emmintrin.h>
#include <iostream>
#include <marmote3/fec_ra_xcoder.h>
#include <smmintrin.h>
#include <stdexcept>
#include <tmmintrin.h>

namespace gr {
namespace marmote3 {

// ------- Permutation

fec_ra_permutation::fec_ra_permutation(int data_len2, int repeat)
    : data_len2(data_len2), repeat(repeat) {
  if (data_len2 <= 0 || repeat <= 0)
    throw std::invalid_argument("fec_ra_permutation: invalid parameters");

  for (int b = 0; b < repeat; b++) {
    perm.push_back(std::vector<int>(data_len2, -1));
    invs.push_back(std::vector<int>(data_len2, -1));
  }
}

int fec_ra_permutation::get_random_int(int mod) {
  assert(mod > 0);
  return ((unsigned int)random()) % ((unsigned int)mod);
}

int fec_ra_permutation::get_unused_index(int block, int unused_count) {
  assert(0 <= unused_count && unused_count < data_len2);

  int n = get_random_int(data_len2 - unused_count);
  for (int i = 0; i < data_len2; i++) {
    if (invs[block][i] < 0 && --n < 0)
      return i;
  }
  throw std::runtime_error("fec_ra_permutation: this cannot happen");
}

void fec_ra_permutation::generate_random() {
  for (int b = 0; b < repeat; b++) {
    for (int i = 0; i < data_len2; i++)
      invs[b][i] = -1;

    for (int i = 0; i < data_len2; i++) {
      int j = get_unused_index(b, i);
      perm[b][i] = j;
      invs[b][j] = i;
    }
  }
}

int fec_ra_permutation::get_modulo_distance(int a, int b) {
  assert(0 <= a && a < data_len2 && 0 <= b && b < data_len2);

  int d = std::abs(a - b);
  return (data_len2 - std::abs(data_len2 - 2 * d)) / 2;
}

bool fec_ra_permutation::is_min_distance_violated(int min_dist, int block,
                                                  int j1, int j2) {
  if (get_modulo_distance(j1, j2) <= min_dist)
    return true;

  for (int b = 0; b < repeat; b++) {
    if (b != block && get_modulo_distance(invs[b][j1], invs[b][j2]) <= min_dist)
      return true;
  }

  return false;
}

int fec_ra_permutation::improve(int min_dist) {
  int bad = 0;
  for (int b = 0; b < repeat; b++) {
    for (int i1 = 0; i1 < data_len2; i1++) {
      for (int i2 = std::max(i1 - min_dist, 0); i2 < i1; i2++) {
        int j1 = perm[b][i1];
        int j2 = perm[b][i2];

        if (is_min_distance_violated(min_dist, b, j1, j2)) {
          bad += 1;

          int i3 = get_random_int(data_len2);
          int j3 = perm[b][i3];

          perm[b][i2] = j3;
          perm[b][i3] = j2;

          invs[b][j2] = i3;
          invs[b][j3] = i2;
        }
      }
    }
  }
  return bad;
}

bool fec_ra_permutation::generate(int min_dist, int max_iteration) {
  if (min_dist < 0)
    throw std::invalid_argument("fec_ra_permutation: invalid minimum distance");

  generate_random();
  if (min_dist == 0)
    return true;

  while (--max_iteration >= 0)
    if (improve(min_dist) == 0)
      return true;

  return false;
}

std::vector<int> fec_ra_permutation::get_permutation() const {
  std::vector<int> vec(data_len2 * repeat, 0);
  for (int i = 0; i < vec.size(); i++)
    vec[i] = perm[i / data_len2][i % data_len2];
  return vec;
}

std::vector<int> fec_ra_permutation::generate_permutation(int data_len2) {
  if (data_len2 <= 0)
    throw std::invalid_argument("fec_ra_permutation: invalid data length");
  fec_ra_permutation perm(data_len2, 4);

  int dist =
      data_len2 < 25
          ? 0
          : data_len2 < 125 ? 1 : data_len2 < 275 ? 2 : data_len2 < 475 ? 3 : 4;
  if (!perm.generate(dist, 200))
    std::cout << "####### fec_ra_permutation: could not reach distance " << dist
              << " for data length " << data_len2 << std::endl;

  return perm.get_permutation();
}

std::vector<int> fec_ra_permutation::generate_puncturing(int data_len2,
                                                         int code_len2) {
  if (data_len2 <= 0)
    throw std::invalid_argument("fec_ra_permutation: invalid data length");

  if (code_len2 < data_len2 || code_len2 > 4 * data_len2)
    throw std::invalid_argument(
        "fec_ra_permutation: code rate must between 1 and 1/4");

  std::vector<int> punc;
  for (int i = 0; i < data_len2; i++)
    punc.push_back(i);

  int limit = 3 * data_len2;
  int step = code_len2 - data_len2;
  int count = limit - std::max(step, 1);
  for (int i = data_len2; i < 4 * data_len2; i++) {
    count += step;
    if (count >= limit) {
      count -= limit;
      punc.push_back(i);
    }
  }
  assert(punc.size() == code_len2);

  return punc;
}

std::vector<int> fec_ra_permutation::generate_punc_inv(int data_len2,
                                                       int code_len2) {
  std::vector<int> pun = generate_puncturing(data_len2, code_len2);
  assert(pun.size() == code_len2);

  std::vector<int> inv(4 * data_len2, -1);
  for (int i = 0; i < code_len2; i++) {
    assert(0 <= pun[i] && pun[i] < 4 * data_len2);
    inv[pun[i]] = i;
  }

  return inv;
}

// ------- Encoder

fec_ra_encoder::fec_ra_encoder(int data_len, int code_len)
    : data_len(data_len), code_len(code_len), data_len2((data_len + 1) / 2),
      code_len2((code_len + 1) / 2),
      perm(fec_ra_permutation::generate_permutation(data_len2)),
      punc_inv(fec_ra_permutation::generate_punc_inv(data_len2, code_len2)) {}

void fec_ra_encoder::encode(const uint8_t *data_bytes,
                            uint8_t *code_bytes) const {
  for (int s = 0; s < perm.size(); s += data_len2) {
    uint16_t code = 0;
    for (int i = 0; i < data_len2; i++) {
      // load 16-bit data with forced to 0xff at end
      int p = perm[s + i];
      int data0 = 2 * p < data_len ? data_bytes[2 * p] : 0xff;
      int data1 = 2 * p + 1 < data_len ? data_bytes[2 * p + 1] : 0xff;
      uint16_t data = data0 | (data1 << 8);

      code ^= data;

      // store 16-bit code trimmed at the end
      p = punc_inv[s + i];
      if (p >= 0) {
        if (2 * p < code_len)
          code_bytes[2 * p] = code;
        if (2 * p + 1 < code_len)
          code_bytes[2 * p + 1] = code >> 8;
      }

      code = (code >> 1) | (code << 15);
    }
  }
}

std::vector<uint8_t>
fec_ra_encoder::encode(const std::vector<uint8_t> &data_bytes) const {
  if (data_bytes.size() != data_len)
    throw std::invalid_argument("data length is invalid");

  std::vector<uint8_t> code_bytes(code_len, 0);
  encode(data_bytes.data(), code_bytes.data());
  return code_bytes;
}

// ------- Decoder

fec_ra_decoder::fec_ra_decoder(int data_len, int code_len, int full_passes,
                               int half_passes)
    : data_len(data_len), code_len(code_len), data_len2((data_len + 1) / 2),
      code_len2((code_len + 1) / 2), full_passes(full_passes),
      half_passes(half_passes),
      perm(fec_ra_permutation::generate_permutation(data_len2)),
      punc_inv(fec_ra_permutation::generate_punc_inv(data_len2, code_len2)) {
  if (full_passes < 0 || half_passes < 0)
    throw std::invalid_argument("fec_ra_decoder: invalid number of passes");

  data = new __m128i[data_len2];
  code = new __m128i[4 * data_len2];
  sums = new __m128i[data_len2];
}

fec_ra_decoder::~fec_ra_decoder() {
  delete[] data;
  data = NULL;

  delete[] code;
  code = NULL;

  delete[] sums;
  sums = NULL;
}

void fec_ra_decoder::load_punctured_code(const uint64_t *soft_bits) {
  __m128i zero = _mm_setzero_si128();
  for (int i = 0; i < data_len2; i++)
    _mm_store_si128(data + i, zero);

  for (int i = 0; i < punc_inv.size(); i++) {
    int a = punc_inv[i];
    __m128i bits;
    if (a < 0)
      bits = zero;
    else if (2 * a + 1 < code_len)
      bits = _mm_loadu_si128((__m128i *)(soft_bits + 2 * a));
    else if (2 * a + 1 == code_len) {
      // bits = _mm_loadu_si64(soft_bits + 2 * a);
      bits = _mm_set_epi64x(0, *(soft_bits + 2 * a));
    } else
      bits = zero;
    _mm_store_si128(code + i, bits);
  }
}

void fec_ra_decoder::store_packed_data(uint8_t *data_bytes) {
  for (int i = 0; i < data_len2; i++) {
    int bits = _mm_movemask_epi8(_mm_load_si128(data + i));
    if (2 * i + 1 < data_len)
      *(uint16_t *)(data_bytes + 2 * i) = bits;
    else if (2 * i + 1 == data_len)
      *(data_bytes + 2 * i) = bits;
  }
}

#define SSE_LLR_MIN1(A, B)                                                     \
  {                                                                            \
    __m128i mm_trim = _mm_set1_epi8(2);                                        \
    __m128i xaba = _mm_abs_epi8(A);                                            \
    __m128i xabb = _mm_abs_epi8(B);                                            \
    __m128i xsig = _mm_xor_si128(A, B);                                        \
    __m128i xmin =                                                             \
        _mm_min_epu8(_mm_min_epu8(xaba, xabb),                                 \
                     _mm_subs_epu8(_mm_max_epu8(xaba, xabb), mm_trim));        \
    A = _mm_blendv_epi8(xmin, _mm_subs_epi8(mm_zero, xmin), xsig);             \
  }

#define SSE_LLR_MIN2(A, B)                                                     \
  {                                                                            \
    __m128i xsig = _mm_xor_si128(A, B);                                        \
    A = _mm_min_epu8(_mm_abs_epi8(A), _mm_abs_epi8(B));                        \
    A = _mm_blendv_epi8(_mm_min_epu8(A, mm_127), _mm_subs_epi8(mm_zero, A),    \
                        xsig);                                                 \
  }

#define SSE_LLR_MIN(A, B) SSE_LLR_MIN2(A, B)

void fec_ra_decoder::full_pass_block(const int *perm_block,
                                     const __m128i *code_block) {
  __m128i mm_zero = _mm_setzero_si128();
  __m128i mm_127 = _mm_set1_epi8(127);

  // make the forward pass
  __m128i sum = mm_127; // zero bit
  for (int i = 0; i < data_len2; i++) {
    __m128i dat = _mm_load_si128(data + perm_block[i]);
    __m128i cod = _mm_load_si128(code_block + i);
    _mm_store_si128(sums + i, sum);

    SSE_LLR_MIN(sum, dat);

    sum = _mm_adds_epi8(sum, cod);
    sum = _mm_alignr_epi8(sum, sum, 1);
  }

  // backward pass, update data
  for (int i = data_len2 - 1; i >= 0; i--) {
    __m128i cod = _mm_load_si128(code_block + i);
    __m128i lft = _mm_load_si128(sums + i);
    __m128i dat = _mm_load_si128(data + perm_block[i]);

    sum = _mm_alignr_epi8(sum, sum, 15);
    sum = _mm_adds_epi8(sum, cod);

    SSE_LLR_MIN(lft, sum);

    lft = _mm_adds_epi8(lft, dat);
    _mm_store_si128(data + perm_block[i], lft);

    SSE_LLR_MIN(sum, dat);
  }
}

void fec_ra_decoder::half_pass_block(const int *perm_block,
                                     const __m128i *code_block) {
  __m128i mm_zero = _mm_setzero_si128();
  __m128i mm_127 = _mm_set1_epi8(127);
  __m128i mm_128 = _mm_set1_epi8(-128);

  // make the forward pass
  __m128i sum = mm_127; // zero bit
  for (int i = 0; i < data_len2; i++) {
    __m128i dat = _mm_load_si128(data + perm_block[i]);
    __m128i cod = _mm_load_si128(code_block + i);
    _mm_store_si128(sums + i, sum);

    SSE_LLR_MIN(sum, dat);

    sum = _mm_adds_epi8(sum, cod);
    sum = _mm_alignr_epi8(sum, sum, 1);
  }

  // backward pass, update data
  for (int i = data_len2 - 1; i >= 0; i--) {
    __m128i cod = _mm_load_si128(code_block + i);
    __m128i lft = _mm_load_si128(sums + i);
    __m128i dat = _mm_load_si128(data + perm_block[i]);

    sum = _mm_alignr_epi8(sum, sum, 15);
    sum = _mm_adds_epi8(sum, cod);

    SSE_LLR_MIN(lft, sum);

    __m128i hal =
        _mm_sub_epi8(_mm_avg_epu8(_mm_add_epi8(dat, mm_128), mm_128), mm_128);
    lft = _mm_adds_epi8(lft, hal);
    _mm_store_si128(data + perm_block[i], lft);

    SSE_LLR_MIN(sum, dat);
  }
}

void fec_ra_decoder::decode(const int8_t *code_bits, uint8_t *data_bytes) {
  load_punctured_code((uint64_t *)code_bits);

  for (int i = 0; i < full_passes; i++) {
    const int *perm_block = perm.data();
    const __m128i *code_block = code;
    for (int j = 0; j < 4; j++) {
      // force bytes beyond data_len to 0xff
      for (int k = data_len; k < 2 * data_len2; k++)
        ((uint64_t *)data)[k] = 0x8080808080808080ULL;

      full_pass_block(perm_block, code_block);
      perm_block += data_len2;
      code_block += data_len2;
    }
  }

  for (int i = 0; i < half_passes; i++) {
    const int *perm_block = perm.data();
    const __m128i *code_block = code;
    for (int j = 0; j < 4; j++) {
      // force bytes beyond data_len to 0xff
      for (int k = data_len; k < 2 * data_len2; k++)
        ((uint64_t *)data)[k] = 0x8080808080808080ULL;

      if (j == 0)
        full_pass_block(perm_block, code_block);
      else
        half_pass_block(perm_block, code_block);
      perm_block += data_len2;
      code_block += data_len2;
    }
  }

  store_packed_data(data_bytes);
}

std::vector<uint8_t>
fec_ra_decoder::decode(const std::vector<int8_t> &code_bits) {
  if (code_len * 8 != code_bits.size())
    throw std::invalid_argument("code length is invalid");

  std::vector<uint8_t> data_bytes(data_len, 0);
  decode(code_bits.data(), data_bytes.data());
  return data_bytes;
}

} /* namespace marmote3 */
} /* namespace gr */
