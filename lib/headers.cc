/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cassert>
#include <cstddef>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <marmote3/headers.h>

namespace gr {
namespace marmote3 {

void arq_header_t::increment_flow_seqno(int &last_sent_seqno) {
  constexpr int seqno_maxval = (1 << flow_seqno_width) - 1;
  assert(-1 <= last_sent_seqno && last_sent_seqno <= seqno_maxval);

  if (last_sent_seqno < 0)
    last_sent_seqno = 0;
  else if (last_sent_seqno >= seqno_maxval)
    last_sent_seqno = 1;
  else
    last_sent_seqno += 1;
}

int arq_header_t::get_flow_missed_count(int rcvd_seqno, int last_rcvd_seqno) {
  constexpr int seqno_maxval = (1 << flow_seqno_width) - 1;
  assert(0 <= rcvd_seqno && rcvd_seqno <= seqno_maxval);
  assert(-1 <= last_rcvd_seqno && last_rcvd_seqno <= seqno_maxval);

  if (last_rcvd_seqno < 0 || rcvd_seqno <= 0)
    return 0;

  int missed = (rcvd_seqno - (last_rcvd_seqno + 1)) % seqno_maxval;
  if (missed < 0)
    missed += seqno_maxval;

  return missed;
}

inline uint16_t read16(const uint8_t *ptr, int &pos) {
  uint16_t a = ptr[pos++];
  uint16_t b = ptr[pos++];
  return (a << 8) + b;
}

inline void write16(uint8_t *ptr, int &pos, uint16_t val) {
  ptr[pos++] = val >> 8;
  ptr[pos++] = val;
}

inline uint32_t read32(const uint8_t *ptr, int &pos) {
  uint32_t a = ptr[pos++];
  a = (a << 8) + ptr[pos++];
  a = (a << 8) + ptr[pos++];
  a = (a << 8) + ptr[pos++];
  return a;
}

inline void write32(uint8_t *ptr, int &pos, uint32_t val) {
  ptr[pos++] = val >> 24;
  ptr[pos++] = val >> 16;
  ptr[pos++] = val >> 8;
  ptr[pos++] = val;
}

void mmt_header_t::load(const uint8_t *ptr) {
  int pos = 0;
  flags = read16(ptr, pos);
  src = ptr[pos++];
  dst = ptr[pos++];
  flowid = read16(ptr, pos);
  assert(pos == mmt_header_t::len());
}

void mmt_header_t::save(uint8_t *ptr) const {
  int pos = 0;
  write16(ptr, pos, flags);
  ptr[pos++] = src;
  ptr[pos++] = dst;
  write16(ptr, pos, flowid);
  assert(pos == mmt_header_t::len());
}

bool mmt_header_t::operator==(const mmt_header_t &mmt) const {
  return std::memcmp(this, &mmt, sizeof(mmt_header_t)) == 0;
}

long mmt_header_t::get_hash() const {
  long hash = flowid;
  hash <<= 8;
  hash += src;
  hash <<= 8;
  hash += dst;
  hash <<= 16;
  hash += flags;
  return hash;
}

long mmt_header_t::get_zip_payload_len(const uint8_t *data_ptr, int data_len,
                                       int payload_mode) {
  if (data_len < mmt_header_t::len())
    return 0;
  mmt_header_t mmt;
  mmt.load(data_ptr);
  data_ptr += mmt_header_t::len();
  data_len -= mmt_header_t::len();
  assert(data_len >= 0);

  if (payload_mode <= 0 || payload_mode > 4)
    return data_len;

  eth_header_t eth;
  const int eth_zip_len = eth.eth_zip_load(mmt, data_ptr, data_len);
  if (eth_zip_len < 0)
    return 0;
  data_ptr += eth_zip_len;
  data_len -= eth_zip_len;
  assert(data_len >= 0);

  if (payload_mode == 1)
    return data_len;

  ip4_header_t ip4;
  const int ip4_zip_len = ip4.ip4_zip_load1(mmt, data_ptr, data_len);
  if (ip4_zip_len < 0)
    return 0;
  data_ptr += ip4_zip_len;
  data_len -= ip4_zip_len;
  assert(data_len >= 0);

  if (payload_mode == 2)
    return data_len;

  // only UDP and TCP should be reported
  if (ip4.prot != 0x11 && ip4.prot != 0x06)
    return 0;

  // if not the first fragment, then it counts full
  if (ip4.prot == 0x11 && (ip4.frag & 0x1fff) == 0) {
    const int udp_zip_len = udp_header_t::udp_zip_len(mmt);
    if (udp_zip_len < 0 || data_len < udp_zip_len)
      return 0;
    data_ptr += udp_zip_len;
    data_len -= udp_zip_len;
    assert(data_len >= 0);
  } else if (ip4.prot == 0x06 && (ip4.frag & 0x1fff) == 0) {
    tcp_header_t tcp;
    const int tcp_zip_len = tcp.tcp_zip_load1(mmt, data_ptr, data_len);
    if (tcp_zip_len < 0)
      return 0;
    data_ptr += tcp_zip_len;
    data_len -= tcp_zip_len;
    assert(data_len >= 0);
  }

  if (payload_mode == 3)
    return data_len;

  // mgen compression is tried even for fragments
  mgn_header_t mgn;
  const int mgn_zip_len = mgn.mgn_zip_load(mmt, ip4, data_ptr, data_len);
  if (mgn_zip_len < 0)
    return 0;
  data_ptr += mgn_zip_len;
  data_len -= mgn_zip_len;
  assert(data_len >= 0);
  const int mgn_raw_len = mgn.mgn_raw_len();

  if (false) {
    std::stringstream str;
    str << "get_zip_payload_len: " << (mgn_raw_len + data_len) << " " << mmt
        << " " << eth_zip_len << " " << eth.eth_raw_len() << " " << ip4_zip_len
        << " " << ip4.ip4_raw_len() << " " << mgn_zip_len << " " << mgn_raw_len
        << " " << data_len;
    str << std::endl;
    std::cout << str.str();
  }

  return mgn_raw_len + data_len;
}

std::ostream &operator<<(std::ostream &str, const mmt_header_t &mmt) {
  auto flags(str.flags());
  str << "flags 0x" << std::right << std::setfill('0') << std::hex
      << std::setw(4) << (int)mmt.flags;
  str << std::dec << " src " << (int)mmt.src;
  str << " dst " << (int)mmt.dst;
  str << " flowid " << (int)mmt.flowid;
  str.flags(flags);
  return str;
}

int eth_header_t::eth_raw_load(const uint8_t *ptr, int len) {
  int pos = 0;

  if (pos + 12 > len)
    return -1;
  else {
    std::memcpy(dst, ptr + pos, 6);
    std::memcpy(src, ptr + pos + 6, 6);
    pos += 12;
  }

  if (pos + 2 > len)
    return -1;
  else
    typ = read16(ptr, pos);

  if (typ != 0x8100)
    tci = 0xffff;
  else {
    if (pos + 4 > len)
      return -1;
    else {
      tci = read16(ptr, pos);
      typ = read16(ptr, pos);
      if (tci == 0xffff || typ == 0x8100)
        return -1;
    }
  }

  assert(pos <= len);
  return pos;
}

int eth_header_t::eth_raw_save(uint8_t *ptr) const {
  if (typ == 0x8100)
    return -1;

  int pos = 0;

  std::memcpy(ptr + pos, dst, 6);
  std::memcpy(ptr + pos + 6, src, 6);
  pos += 12;

  if (tci != 0xffff) {
    write16(ptr, pos, 0x8100);
    write16(ptr, pos, tci);
  }

  write16(ptr, pos, typ);

  return pos;
}

int eth_header_t::eth_raw_len() const {
  if (typ == 0x8100)
    return -1;
  else if (tci == 0xffff)
    return 14;
  else
    return 18;
}

static const uint8_t DEFAULT_MAC[5] = {0x12, 0x34, 0x56, 0x78, 0x90};
static const uint8_t BRDCAST_MAC[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

bool is_default_mac(const uint8_t *mac) {
  return std::memcmp(mac, BRDCAST_MAC, 6) == 0 ||
         (std::memcmp(mac, DEFAULT_MAC, 5) == 0 && mac[5] != 0xff);
}

int eth_header_t::eth_zip_load(const mmt_header_t &mmt, const uint8_t *ptr,
                               int len) {
  int pos = 0;

  dst[5] = mmt.dst;
  src[5] = mmt.src;
  if ((mmt.flags & FLAGS_ETH_MAC) != 0) {
    std::memcpy(dst, dst[5] == 0xff ? BRDCAST_MAC : DEFAULT_MAC, 5);
    std::memcpy(src, src[5] == 0xff ? BRDCAST_MAC : DEFAULT_MAC, 5);
  } else {
    if (pos + 10 > len)
      return -1;
    else {
      std::memcpy(dst, ptr + pos, 5);
      std::memcpy(src, ptr + pos + 5, 5);
      pos += 10;
    }
  }

  if ((mmt.flags & FLAGS_ETH_IP4) != 0) {
    tci = 0xffff;
    typ = 0x0800;
  } else {
    if (pos + 2 > len)
      return -1;
    else
      typ = read16(ptr, pos);

    if (typ != 0x8100)
      tci = 0xffff;
    else {
      if (pos + 4 > len)
        return -1;
      else {
        tci = read16(ptr, pos);
        typ = read16(ptr, pos);
        if (tci == 0xffff || typ == 0x8100)
          return -1;
      }
    }
  }

  assert(pos <= len);
  return pos;
}

int eth_header_t::eth_zip_save1(mmt_header_t &mmt) const {
  if (typ == 0x8100)
    return -1;

  mmt.flags &= ~FLAGS_ETH_MSK;
  int pos = 0;

  mmt.dst = dst[5];
  mmt.src = src[5];
  if (is_default_mac(dst) && is_default_mac(src))
    mmt.flags |= FLAGS_ETH_MAC;
  else
    pos += 10;

  if (tci == 0xffff && typ == 0x0800)
    mmt.flags |= FLAGS_ETH_IP4;
  else {
    if (tci != 0xffff)
      pos += 4;

    pos += 2;
  }

  return pos;
}

int eth_header_t::eth_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const {
  if (typ == 0x8100)
    return -1;

  int pos = 0;

  if ((mmt.flags & FLAGS_ETH_MAC) == 0) {
    std::memcpy(ptr + pos, dst, 5);
    std::memcpy(ptr + pos + 5, src, 5);
    pos += 10;
  }

  if ((mmt.flags & FLAGS_ETH_IP4) == 0) {
    if (tci != 0xffff) {
      write16(ptr, pos, 0x8100);
      write16(ptr, pos, tci);
    }

    write16(ptr, pos, typ);
  }

  return pos;
}

bool eth_header_t::operator==(const eth_header_t &eth) const {
  return std::memcmp(this, &eth, sizeof(eth_header_t)) == 0;
}

uint16_t ip4_crc(const uint8_t *ptr, int len) {
  uint32_t crc = 0;

  int pos = 0;
  while (pos + 2 <= len)
    crc += read16(ptr, pos);

  if (pos < len)
    crc += ptr[pos++] << 8;

  while (crc > 0xffff)
    crc = (crc & 0xffff) + (crc >> 16);

  return ~(uint16_t)crc;
}

int ip4_header_t::ip4_raw_load(const uint8_t *ptr, int len) {
  if (len < 20 || ptr[0] < 0x45 || ptr[0] > 0x4f)
    return -1;

  int pos = 0;
  ihl = ptr[pos++] & 0x0f;
  if (4 * ihl > len || ip4_crc(ptr, 4 * ihl) != 0)
    return -1;

  tos = ptr[pos++];
  tlen = read16(ptr, pos);
  id = read16(ptr, pos);
  frag = read16(ptr, pos);
  ttl = ptr[pos++];
  prot = ptr[pos++];
  pos += 2; // crc
  std::memcpy(src, ptr + pos, 4);
  std::memcpy(dst, ptr + pos + 4, 4);
  pos += 8;
  assert(pos == 20);
  std::memcpy(opt, ptr + 20, 4 * ihl - 20);

  return 4 * ihl;
}

int ip4_header_t::ip4_raw_save(uint8_t *ptr) const {
  if (ihl < 0x05 || ihl > 0x0f)
    return -1;

  int pos = 0;
  ptr[pos++] = 0x40 + ihl;
  ptr[pos++] = tos;
  write16(ptr, pos, tlen);
  write16(ptr, pos, id);
  write16(ptr, pos, frag);
  ptr[pos++] = ttl;
  ptr[pos++] = prot;
  assert(pos == 10);
  write16(ptr, pos, 0); // crc
  std::memcpy(ptr + pos, src, 4);
  std::memcpy(ptr + pos + 4, dst, 4);
  pos += 8;
  assert(pos == 20);
  std::memcpy(ptr + 20, opt, 4 * ihl - 20);

  pos = 10;
  write16(ptr, pos, ip4_crc(ptr, 4 * ihl)); // crc

  return 4 * ihl;
}

int ip4_header_t::ip4_raw_len() const {
  if (ihl < 0x05 || ihl > 0x0f)
    return -1;
  else
    return 4 * ihl;
}

int ip4_header_t::ip4_zip_load1(const mmt_header_t &mmt, const uint8_t *ptr,
                                int len) {
  int pos = 0;

  if ((mmt.flags & FLAGS_IP4_IHL) != 0)
    ihl = 5;
  else {
    if (pos + 1 > len)
      return -1;
    else {
      ihl = ptr[pos++];
      if (ihl < 0x45 || ihl > 0x4f)
        return -1;
      else
        ihl &= 0x0f;
    }
  }

  if (pos + 1 > len)
    return -1;
  else
    tos = ptr[pos++];

  if ((mmt.flags & FLAGS_IP4_LEN) != 0) {
    tlen = 0; // will overwrite it in load2
  } else {
    if (pos + 2 > len)
      return -1;
    else
      tlen = read16(ptr, pos);
  }

  if (pos + 2 > len)
    return -1;
  else
    id = read16(ptr, pos);

  if ((mmt.flags & FLAGS_IP4_FRG) != 0)
    frag = 0x4000;
  else {
    if (pos + 2 > len)
      return -1;
    else
      frag = read16(ptr, pos);
  }

  if ((mmt.flags & FLAGS_IP4_TTL) != 0)
    ttl = 254;
  else {
    if (pos + 1 > len)
      return -1;
    else
      ttl = ptr[pos++];
  }

  if ((mmt.flags & FLAGS_IP4_UDP) != 0)
    prot = 0x11;
  else {
    if (pos + 1 > len)
      return -1;
    else
      prot = ptr[pos++];
  }

  if ((mmt.flags & FLAGS_IP4_SPC) != 0 && (mmt.flags & FLAGS_IP4_ADR) != 0) {
    src[0] = 192;
    src[1] = 168;
    src[2] = 45;
    src[3] = mmt.src;
    dst[0] = 192;
    dst[1] = 168;
    dst[2] = 45;
    dst[3] = mmt.dst;
  } else if ((mmt.flags & FLAGS_IP4_SPC) != 0 &&
             (mmt.flags & FLAGS_IP4_ADR) == 0) {
    if (pos + 2 > len)
      return -1;
    else {
      src[0] = 192;
      src[1] = 168;
      src[2] = mmt.src;
      src[3] = ptr[pos++];
      dst[0] = 192;
      dst[1] = 168;
      dst[2] = mmt.dst;
      dst[3] = ptr[pos++];
    }
  } else if ((mmt.flags & FLAGS_IP4_SPC) == 0 &&
             (mmt.flags & FLAGS_IP4_ADR) != 0) {
    if (pos + 4 > len)
      return -1;
    else {
      src[0] = 192;
      src[1] = 168;
      src[2] = ptr[pos++];
      src[3] = ptr[pos++];
      dst[0] = 192;
      dst[1] = 168;
      dst[2] = ptr[pos++];
      dst[3] = ptr[pos++];
    }
  } else {
    if (pos + 8 > len)
      return -1;
    else {
      std::memcpy(src, ptr + pos, 4);
      std::memcpy(dst, ptr + pos + 4, 4);
      pos += 8;
    }
  }

  if (ihl > 5) {
    if (pos + 4 * ihl - 20 > len)
      return -1;
    else {
      std::memcpy(opt, ptr + pos, 4 * ihl - 20);
      pos += 4 * ihl - 20;
    }
  }

  assert(pos <= len);
  return pos;
}

void ip4_header_t::ip4_zip_load2(const mmt_header_t &mmt, int payload_len) {
  if ((mmt.flags & FLAGS_IP4_LEN) != 0)
    tlen = 4 * ihl + payload_len;
}

int ip4_header_t::ip4_zip_save1(mmt_header_t &mmt, int payload_len) const {
  if (ihl < 0x05 || ihl > 0x0f)
    return -1;

  mmt.flags &= ~FLAGS_IP4_MSK;
  int pos = 0;

  // ihl
  if (ihl == 5)
    mmt.flags |= FLAGS_IP4_IHL;
  else
    pos += 1;

  // tos
  pos += 1;

  // tlen
  if (4 * ihl + payload_len == tlen)
    mmt.flags |= FLAGS_IP4_LEN;
  else
    pos += 2;

  // id
  pos += 2;

  // frag
  if (frag == 0x4000)
    mmt.flags |= FLAGS_IP4_FRG;
  else
    pos += 2;

  // ttl
  if (ttl == 254)
    mmt.flags |= FLAGS_IP4_TTL;
  else
    pos += 1;

  // prot
  if (prot == 0x11)
    mmt.flags |= FLAGS_IP4_UDP;
  else
    pos += 1;

  // src and dst
  if (src[0] == 192 && src[1] == 168 && dst[0] == 192 && dst[1] == 168) {
    if (src[2] == 45 && src[3] == mmt.src && dst[2] == 45 && dst[3] == mmt.dst)
      mmt.flags |= FLAGS_IP4_ADR | FLAGS_IP4_SPC;
    else if (src[2] == mmt.src && dst[2] == mmt.dst) {
      mmt.flags |= FLAGS_IP4_SPC;
      pos += 2;
    } else {
      mmt.flags |= FLAGS_IP4_ADR;
      pos += 4;
    }
  } else
    pos += 8;

  // opt
  if (ihl > 5)
    pos += 4 * ihl - 20;

  return pos;
}

int ip4_header_t::ip4_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const {
  if (ihl < 0x05 || ihl > 0x0f)
    return -1;

  int pos = 0;

  if ((mmt.flags & FLAGS_IP4_IHL) == 0)
    ptr[pos++] = 0x40 + ihl;

  ptr[pos++] = tos;

  if ((mmt.flags & FLAGS_IP4_LEN) == 0)
    write16(ptr, pos, tlen);

  write16(ptr, pos, id);

  if ((mmt.flags & FLAGS_IP4_FRG) == 0)
    write16(ptr, pos, frag);

  if ((mmt.flags & FLAGS_IP4_TTL) == 0)
    ptr[pos++] = ttl;

  if ((mmt.flags & FLAGS_IP4_UDP) == 0)
    ptr[pos++] = prot;

  if ((mmt.flags & FLAGS_IP4_SPC) != 0 && (mmt.flags & FLAGS_IP4_ADR) == 0) {
    ptr[pos++] = src[3];
    ptr[pos++] = dst[3];
  } else if ((mmt.flags & FLAGS_IP4_SPC) == 0 &&
             (mmt.flags & FLAGS_IP4_ADR) != 0) {
    ptr[pos++] = src[2];
    ptr[pos++] = src[3];
    ptr[pos++] = dst[2];
    ptr[pos++] = dst[3];
  } else if ((mmt.flags & FLAGS_IP4_SPC) == 0 &&
             (mmt.flags & FLAGS_IP4_ADR) == 0) {
    std::memcpy(ptr + pos, src, 4);
    std::memcpy(ptr + pos + 4, dst, 4);
    pos += 8;
  }

  if (ihl > 5) {
    std::memcpy(ptr + pos, opt, 4 * ihl - 20);
    pos += 4 * ihl - 20;
  }

  return pos;
}

bool ip4_header_t::operator==(const ip4_header_t &ip4) const {
  return std::memcmp(this, &ip4, offsetof(ip4_header_t, opt)) == 0 &&
         std::memcmp(opt, ip4.opt, std::min(std::max(4 * (ihl - 5), 0), 40)) ==
             0;
}

int udp_header_t::udp_raw_load(const uint8_t *ptr, int len) {
  if (len < 8)
    return -1;

  int pos = 0;
  src_port = read16(ptr, pos);
  dst_port = read16(ptr, pos);
  ulen = read16(ptr, pos);
  crc = read16(ptr, pos);

  assert(pos == udp_header_t::udp_raw_len());
  return pos;
}

void udp_header_t::udp_raw_save(uint8_t *ptr) const {
  int pos = 0;
  write16(ptr, pos, src_port);
  write16(ptr, pos, dst_port);
  write16(ptr, pos, ulen);
  write16(ptr, pos, crc);

  assert(pos == udp_header_t::udp_raw_len());
}

uint16_t udp_header_t::udp_crc(const ip4_header_t &ip4, const udp_header_t &udp,
                               const uint8_t *ptr, int len) {
  uint32_t crc = 0;

  int pos = 0;
  crc += read16(ip4.src, pos);
  crc += read16(ip4.src, pos);

  pos = 0;
  crc += read16(ip4.dst, pos);
  crc += read16(ip4.dst, pos);

  assert(ip4.prot == 0x11);
  crc += ip4.prot;
  crc += udp.ulen;

  crc += udp.src_port;
  crc += udp.dst_port;
  crc += udp.ulen;
  crc += udp.crc;

  pos = 0;
  while (pos + 2 <= len)
    crc += read16(ptr, pos);

  if (pos < len)
    crc += ptr[pos++] << 8;

  while (crc > 0xffff)
    crc = (crc & 0xffff) + (crc >> 16);

  return ~(uint16_t)crc;
}

int udp_header_t::udp_zip_len(const mmt_header_t &mmt) {
  int pos = 0;

  if ((mmt.flags & FLAGS_UDP_PRT) == 0)
    pos += 2;

  if ((mmt.flags & FLAGS_UDP_LEN) == 0)
    pos += 2;

  if ((mmt.flags & FLAGS_UDP_CRC) == 0)
    pos += 2;

  return pos;
}

void udp_header_t::udp_zip_load1(const mmt_header_t &mmt, const uint8_t *ptr) {
  int pos = 0;

  dst_port = mmt.flowid;

  if ((mmt.flags & FLAGS_UDP_PRT) == 0)
    src_port = read16(ptr, pos);
  else
    src_port = dst_port - 1000;

  if ((mmt.flags & FLAGS_UDP_LEN) == 0)
    ulen = read16(ptr, pos);

  if ((mmt.flags & FLAGS_UDP_CRC) == 0)
    crc = read16(ptr, pos);

  assert(pos == udp_zip_len(mmt));
}

void udp_header_t::udp_zip_load2(const mmt_header_t &mmt,
                                 const ip4_header_t &ip4,
                                 const uint8_t *payload, int payload_len) {
  assert(ip4.prot == 0x11);

  if ((mmt.flags & FLAGS_UDP_LEN) != 0)
    ulen = ip4.tlen - ip4.ihl * 4;

  if ((mmt.flags & FLAGS_UDP_CRC) != 0) {
    crc = 0; // must be cleared for crc calculation
    crc = udp_crc(ip4, *this, payload, payload_len);
  }
}

void udp_header_t::udp_zip_save1(mmt_header_t &mmt, const ip4_header_t &ip4,
                                 const uint8_t *payload,
                                 int payload_len) const {
  assert(ip4.prot == 0x11);

  mmt.flags &= ~FLAGS_UDP_MSK;

  if (src_port == (uint16_t)(dst_port - 1000))
    mmt.flags |= FLAGS_UDP_PRT;

  mmt.flowid = dst_port;

  if (ulen == ip4.tlen - ip4.ihl * 4)
    mmt.flags |= FLAGS_UDP_LEN;

  if (udp_crc(ip4, *this, payload, payload_len) == 0)
    mmt.flags |= FLAGS_UDP_CRC;
}

void udp_header_t::udp_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const {
  int pos = 0;

  if ((mmt.flags & FLAGS_UDP_PRT) == 0)
    write16(ptr, pos, src_port);

  if ((mmt.flags & FLAGS_UDP_LEN) == 0)
    write16(ptr, pos, ulen);

  if ((mmt.flags & FLAGS_UDP_CRC) == 0)
    write16(ptr, pos, crc);

  assert(pos == udp_zip_len(mmt));
}

bool udp_header_t::operator==(const udp_header_t &udp) const {
  return std::memcmp(this, &udp, sizeof(udp_header_t)) == 0;
}

int tcp_header_t::tcp_raw_load(const uint8_t *ptr, int len) {
  if (len < 20)
    return -1;

  int pos = 0;
  src_port = read16(ptr, pos);
  dst_port = read16(ptr, pos);
  seq_no = read32(ptr, pos);
  ack_no = read32(ptr, pos);
  data_off = ptr[pos] >> 4;
  if (data_off < 5 || data_off > 15 || len < 4 * data_off)
    return -1;
  flags = read16(ptr, pos) & 0x0fff;
  win_size = read16(ptr, pos);
  crc = read16(ptr, pos);
  urg = read16(ptr, pos);
  assert(pos == 20);
  std::memcpy(opt, ptr + 20, 4 * data_off - 20);

  return 4 * data_off;
}

int tcp_header_t::tcp_raw_len() const {
  if (data_off < 0x05 || data_off > 0x0f)
    return -1;
  else
    return 4 * data_off;
}

int tcp_header_t::tcp_raw_save(uint8_t *ptr) const {
  if (data_off < 0x05 || data_off > 0x0f || flags > 0x0fff)
    return -1;

  int pos = 0;
  write16(ptr, pos, src_port);
  write16(ptr, pos, dst_port);
  write32(ptr, pos, seq_no);
  write32(ptr, pos, ack_no);
  write16(ptr, pos, ((uint16_t)data_off << 12) + (flags & 0x0fff));
  write16(ptr, pos, win_size);
  write16(ptr, pos, crc);
  write16(ptr, pos, urg);
  assert(pos == 20);
  std::memcpy(ptr + 20, opt, 4 * data_off - 20);

  return 4 * data_off;
}

uint16_t tcp_crc(const ip4_header_t &ip4, const tcp_header_t &tcp,
                 const uint8_t *ptr, int len) {
  uint32_t crc = 0;

  int pos = 0;
  crc += read16(ip4.src, pos);
  crc += read16(ip4.src, pos);

  pos = 0;
  crc += read16(ip4.dst, pos);
  crc += read16(ip4.dst, pos);

  assert(ip4.prot == 0x06);
  crc += ip4.prot;
  crc += 4 * tcp.data_off + len;

  crc += tcp.src_port;
  crc += tcp.dst_port;
  crc += tcp.seq_no >> 16;
  crc += tcp.seq_no & 0xffff;
  crc += tcp.ack_no >> 16;
  crc += tcp.ack_no & 0xffff;
  crc += ((uint16_t)tcp.data_off << 12) + (tcp.flags & 0x0fff);
  crc += tcp.win_size;
  crc += tcp.crc;
  crc += tcp.urg;

  int opt_len = std::min(std::max(4 * tcp.data_off - 20, 0), 40);
  pos = 0;
  while (pos < opt_len)
    crc += read16(tcp.opt, pos);

  pos = 0;
  while (pos + 2 <= len)
    crc += read16(ptr, pos);

  if (pos < len)
    crc += ptr[pos++] << 8;

  while (crc > 0xffff)
    crc = (crc & 0xffff) + (crc >> 16);

  return ~(uint16_t)crc;
}

int tcp_header_t::tcp_zip_load1(const mmt_header_t &mmt, const uint8_t *ptr,
                                int len) {
  int pos = 0;

  dst_port = mmt.flowid;

  if ((mmt.flags & FLAGS_TCP_PRT) == 0) {
    if (pos + 2 > len)
      return -1;
    else
      src_port = read16(ptr, pos);
  } else
    src_port = dst_port - 1000;

  if (pos + 12 > len)
    return -1;
  else {
    seq_no = read32(ptr, pos);
    ack_no = read32(ptr, pos);
    data_off = ptr[pos] >> 4;
    if (data_off < 5 || data_off > 15)
      return -1;
    flags = read16(ptr, pos) & 0x0fff;
    win_size = read16(ptr, pos);
  }

  if ((mmt.flags & FLAGS_TCP_CRC) == 0) {
    if (pos + 2 > len)
      return -1;
    else
      crc = read16(ptr, pos);
  }

  if ((mmt.flags & FLAGS_TCP_URG) == 0) {
    if (pos + 2 > len)
      return -1;
    else
      urg = read16(ptr, pos);
  } else
    urg = 0;

  if (data_off > 5) {
    if (pos + 4 * data_off - 20 > len)
      return -1;
    else {
      std::memcpy(opt, ptr + pos, 4 * data_off - 20);
      pos += 4 * data_off - 20;
    }
  }

  assert(pos <= len);
  return pos;
}

void tcp_header_t::tcp_zip_load2(const mmt_header_t &mmt,
                                 const ip4_header_t &ip4,
                                 const uint8_t *payload, int payload_len) {
  assert(ip4.prot == 0x06);

  if ((mmt.flags & FLAGS_TCP_CRC) != 0) {
    crc = 0; // must be cleared for crc calculation
    crc = tcp_crc(ip4, *this, payload, payload_len);
  }
}

int tcp_header_t::tcp_zip_save1(mmt_header_t &mmt, const ip4_header_t &ip4,
                                const uint8_t *payload, int payload_len) const {
  assert(ip4.prot == 0x06);

  if (data_off < 0x05 || data_off > 0x0f || flags > 0x0fff)
    return -1;

  mmt.flags &= ~FLAGS_TCP_MSK;
  int pos = 0;

  if (src_port == (uint16_t)(dst_port - 1000))
    mmt.flags |= FLAGS_TCP_PRT;
  else
    pos += 2;

  mmt.flowid = dst_port;

  // seq_no, ack_no, data_off, flags, win_size
  pos += 12;

  if (tcp_crc(ip4, *this, payload, payload_len) == 0)
    mmt.flags |= FLAGS_TCP_CRC;
  else
    pos += 2;

  if (urg == 0)
    mmt.flags |= FLAGS_TCP_URG;
  else
    pos += 2;

  // opt
  pos += 4 * data_off - 20;

  return pos;
}

int tcp_header_t::tcp_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const {
  if (data_off < 0x05 || data_off > 0x0f || flags > 0x0fff)
    return -1;

  int pos = 0;

  if ((mmt.flags & FLAGS_TCP_PRT) == 0)
    write16(ptr, pos, src_port);

  write32(ptr, pos, seq_no);
  write32(ptr, pos, ack_no);
  write16(ptr, pos, ((uint16_t)data_off << 12) + (flags & 0x0fff));
  write16(ptr, pos, win_size);

  if ((mmt.flags & FLAGS_TCP_CRC) == 0)
    write16(ptr, pos, crc);

  if ((mmt.flags & FLAGS_TCP_URG) == 0)
    write16(ptr, pos, urg);

  if (data_off > 5) {
    std::memcpy(ptr + pos, opt, 4 * data_off - 20);
    pos += 4 * data_off - 20;
  }

  return pos;
}

bool tcp_header_t::operator==(const tcp_header_t &tcp) const {
  return std::memcmp(this, &tcp, offsetof(tcp_header_t, opt)) == 0 &&
         std::memcmp(opt, tcp.opt,
                     std::min(std::max(4 * (data_off - 5), 0), 40)) == 0;
}

int mgn_header_t::mgn_raw_load(const ip4_header_t &ip4, uint16_t dst_port,
                               const uint8_t *ptr, int len) {
  valid = false;
  if (len < 37)
    return 0;

  int pos = 0;
  messageSize = read16(ptr, pos);        // 0
  mgenVersion = ptr[pos++];              // 2
  mgenFlags = ptr[pos++];                // 3
  uint32_t flowid = read32(ptr, pos);    // 4
  mgenSeqno = read32(ptr, pos);          // 8
  mgenFrag = read32(ptr, pos);           // 12
  txTimeSeconds = read32(ptr, pos);      // 16
  txTimeMicroseconds = read32(ptr, pos); // 20
  uint16_t dstPort = read16(ptr, pos);   // 24
  uint8_t dstAddrType = ptr[pos++];      // 26
  uint8_t dstAddrLen = ptr[pos++];       // 27
  uint8_t dstAddr[4];                    //
  memcpy(dstAddr, ptr + pos, 4);         // 28
  pos += 4;                              //
  uint32_t hostData = read32(ptr, pos);  // 32
  mgenTos = ptr[pos++];                  // 36
  assert(pos == 37);

  if (flowid != dst_port || dstPort != dst_port || dstAddrType != 1 ||
      dstAddrLen != 4 || memcmp(dstAddr, ip4.dst, 4) != 0 || hostData != 0 ||
      txTimeMicroseconds > 999999)
    return 0;

  valid = true;
  valid_gps = false;

  if (pos + 16 <= len) {
    int pos2 = pos;
    uint32_t latitude = read32(ptr, pos2);
    uint32_t longitude = read32(ptr, pos2);
    uint32_t altitude = read32(ptr, pos2);
    uint32_t gpsStatus = read32(ptr, pos2);
    assert(pos2 == 53);

    if (latitude == 0x04376820 && longitude == 0x04376820 &&
        altitude == 0xfffffc19 && gpsStatus == 0) {
      valid_gps = true;
      pos = pos2;
    }
  }

  assert(pos == mgn_raw_len());
  return pos;
}

int mgn_header_t::mgn_raw_len() const {
  if (!valid)
    return 0;
  else if (!valid_gps)
    return 37;
  else
    return 53;
}

int mgn_header_t::mgn_raw_save(const ip4_header_t &ip4, uint16_t dst_port,
                               uint8_t *ptr) const {
  if (!valid)
    return 0;

  int pos = 0;
  write16(ptr, pos, messageSize);
  ptr[pos++] = mgenVersion;
  ptr[pos++] = mgenFlags;
  write32(ptr, pos, dst_port);
  write32(ptr, pos, mgenSeqno);
  write32(ptr, pos, mgenFrag);
  write32(ptr, pos, txTimeSeconds);
  write32(ptr, pos, txTimeMicroseconds);
  write16(ptr, pos, dst_port);
  ptr[pos++] = 1;
  ptr[pos++] = 4;
  memcpy(ptr + pos, ip4.dst, 4);
  pos += 4;
  write32(ptr, pos, 0);
  ptr[pos++] = mgenTos;
  assert(pos == 37);

  if (valid_gps) {
    write32(ptr, pos, 0x04376820);
    write32(ptr, pos, 0x04376820);
    write32(ptr, pos, 0xfffffc19);
    write32(ptr, pos, 0);
    assert(pos == 53);
  }

  assert(pos == mgn_raw_len());
  return pos;
}

int mgn_header_t::mgn_zip_len(const mmt_header_t &mmt) {
  if ((mmt.flags & FLAGS_MGN_VLD) == 0)
    return 0;

  int pos = 0;
  pos += 2;

  if ((mmt.flags & FLAGS_MGN_TOS) == 0)
    pos += 2;

  if ((mmt.flags & FLAGS_MGN_FRG) == 0)
    pos += 5;

  pos += 11;

  return pos;
}

int mgn_header_t::mgn_zip_load(const mmt_header_t &mmt, const ip4_header_t &ip4,
                               const uint8_t *ptr, int len) {
  valid = false;
  if ((mmt.flags & FLAGS_MGN_VLD) == 0)
    return 0;

  int pos = 0;
  if (pos + 2 > len)
    return -1;
  messageSize = read16(ptr, pos);

  if ((mmt.flags & FLAGS_MGN_TOS) != 0) {
    mgenVersion = 4;
    mgenTos = ip4.tos;
  } else {
    if (pos + 2 > len)
      return -1;
    mgenVersion = ptr[pos++];
    mgenTos = ptr[pos++];
  }

  if ((mmt.flags & FLAGS_MGN_FRG) != 0) {
    mgenFlags = 0x0c;
    mgenFrag = 0;
  } else {
    if (pos + 5 > len)
      return -1;
    mgenFlags = ptr[pos++];
    mgenFrag = read32(ptr, pos);
  }

  if (pos + 11 > len)
    return -1;
  mgenSeqno = read32(ptr, pos);
  txTimeSeconds = read32(ptr, pos);
  txTimeMicroseconds = ptr[pos++];
  txTimeMicroseconds = (txTimeMicroseconds << 8) + ptr[pos++];
  txTimeMicroseconds = (txTimeMicroseconds << 8) + ptr[pos++];

  valid_gps = (mmt.flags & FLAGS_MGN_GPS) != 0;

  assert(pos == mgn_zip_len(mmt));
  valid = true;
  return pos;
}

int mgn_header_t::mgn_zip_save1(mmt_header_t &mmt,
                                const ip4_header_t &ip4) const {
  mmt.flags &= ~FLAGS_MGN_MSK;

  if (valid)
    mmt.flags |= FLAGS_MGN_VLD;
  else
    return 0;

  int pos = 0;
  pos += 2; // messageSize;

  if (mgenVersion == 4 && mgenTos == ip4.tos)
    mmt.flags |= FLAGS_MGN_TOS;
  else
    pos += 2; // ver, tos

  if (mgenFlags == 0x0c && mgenFrag == 0)
    mmt.flags |= FLAGS_MGN_FRG;
  else
    pos += 5; // flags, frag

  pos += 4; // seqno
  pos += 7; // txTime

  if (valid_gps)
    mmt.flags |= FLAGS_MGN_GPS;

  assert(pos == mgn_zip_len(mmt));
  return pos;
}

int mgn_header_t::mgn_zip_save2(const mmt_header_t &mmt, uint8_t *ptr) const {
  if ((mmt.flags & FLAGS_MGN_VLD) == 0)
    return 0;

  int pos = 0;
  write16(ptr, pos, messageSize);

  if ((mmt.flags & FLAGS_MGN_TOS) == 0) {
    ptr[pos++] = mgenVersion;
    ptr[pos++] = mgenTos;
  }

  if ((mmt.flags & FLAGS_MGN_FRG) == 0) {
    ptr[pos++] = mgenFlags;
    write32(ptr, pos, mgenFrag);
  }

  write32(ptr, pos, mgenSeqno);
  write32(ptr, pos, txTimeSeconds);
  ptr[pos++] = txTimeMicroseconds >> 16;
  ptr[pos++] = txTimeMicroseconds >> 8;
  ptr[pos++] = txTimeMicroseconds;

  assert(pos == mgn_zip_len(mmt));
  return pos;
}

bool mgn_header_t::operator==(const mgn_header_t &mgn) const {
  if (!valid || !mgn.valid)
    return valid == mgn.valid;

  if (false) {
    std::cout << "this:  " << valid << " " << valid_gps << " "
              << (int)mgenVersion << std::endl;
    std::cout << "other: " << mgn.valid << " " << mgn.valid_gps << " "
              << (int)mgn.mgenVersion << std::endl;
  }

  return std::memcmp(this, &mgn, sizeof(mgn_header_t)) == 0;
}

} // namespace marmote3
} /* namespace gr */
