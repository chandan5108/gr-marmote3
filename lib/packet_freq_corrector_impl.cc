/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_freq_corrector_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

packet_freq_corrector::sptr packet_freq_corrector::make(modulation_t mod,
                                                        int span,
                                                        bool postamble,
                                                        int max_output_len) {
  return gnuradio::get_initial_sptr(
      new packet_freq_corrector_impl(mod, span, postamble, max_output_len));
}

packet_freq_corrector_impl::packet_freq_corrector_impl(modulation_t mod,
                                                       int span, bool postamble,
                                                       int max_output_len)
    : tagged_stream_block2("packet_freq_corrector",
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           max_output_len),
      mod(mod), span(span), postamble(postamble), small_input_warning(false) {
  if (span < 1)
    throw std::invalid_argument("packet_freq_corrector: span must be positive");

  if (mod != MOD_BPSK && mod != MOD_QPSK && mod != MOD_8PSK &&
      mod != MOD_16QAM && mod != MOD_64QAM && mod != MOD_MCS)
    throw std::invalid_argument("packet_freq_corrector: invalid modulation");
}

packet_freq_corrector_impl::~packet_freq_corrector_impl() {}

int packet_freq_corrector_impl::work(int noutput_items,
                                     gr_vector_int &ninput_items,
                                     gr_vector_const_void_star &input_items,
                                     gr_vector_void_star &output_items) {
  int len = ninput_items[0];
  if (len > noutput_items) {
    std::cout << "####### packet_freq_corrector: output buffer too small\n";
    return 0;
  }

  modulation_t mod2 = mod;
  if (mod2 == MOD_MCS) {
    if (!has_input_long(0, PMT_PACKET_MCS)) {
      std::cout << "####### packet_freq_corrector: MCS is not set\n";
      return 0;
    }
    mod2 = get_mcscheme(get_input_long(0, PMT_PACKET_MCS, -1)).mod;
  }

  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  int dir;
  const gr_complex *in2;
  if (!postamble) {
    dir = 1;
    in2 = in;
  } else {
    dir = -1;
    in2 = in + len - 1;
  }

  float phase_err = 0.0f;
  float freq_err = 0.0f;

  if (len < 2 * span) {
    if (!small_input_warning) {
      std::cout << "####### packet_freq_corrector: too small input\n";
      small_input_warning = true;
    }
  } else if (mod2 == MOD_BPSK)
    method_bpsk(in2, dir, len, phase_err, freq_err);
  else if (mod2 == MOD_QPSK || mod2 == MOD_16QAM || mod2 == MOD_64QAM)
    method_qpsk(in2, dir, len, phase_err, freq_err);
  else if (mod2 == MOD_8PSK)
    method_8psk(in2, dir, len, phase_err, freq_err);
  else {
    std::cout << "####### packet_freq_corrector: invalid modulation\n";
    return 0;
  }

  if (postamble) {
    phase_err = phase_err + freq_err * len;
    freq_err = -freq_err;
  }

  set_output_float(PMT_FREQ_ERR, freq_err);
  set_output_float(PMT_PHASE_ERR, phase_err);

  gr_complex offset(std::cos(-freq_err), std::sin(-freq_err));
  gr_complex start(std::cos(-phase_err), std::sin(-phase_err));
  volk_32fc_s32fc_x2_rotator_32fc(out, in, offset, &start, len);

  return len;
}

inline gr_complex pow2(gr_complex c) { return c * c; }

void packet_freq_corrector_impl::method_bpsk(const gr_complex *in, int dir,
                                             int len, float &phase_err,
                                             float &freq_err) {
  int pnum = len / span;
  assert(pnum > 0);

  float xavg = pnum * 0.5f;
  float xvar = xavg * (pnum * pnum - 1) / 6.0f;
  float yavg = 0.0f;
  float ycov = 0.0f;

  float y = 0.0f;
  std::complex<float> a(1.0f, 0.0f);
  for (int i = 0; i < pnum; i++) {
    std::complex<float> b(0.0f, 0.0f);
    for (int j = 0; j < span; j++) {
      b += pow2(*in);
      in += dir;
    }

    float x = 0.5f + i;
    y += std::arg(b * std::conj(a));
    a = b;

    yavg += y;
    ycov += (x - xavg) * y;
  }
  yavg /= pnum;

  freq_err = ycov / xvar;
  phase_err = yavg - freq_err * (xavg - 0.5 / span);

  freq_err /= 2 * span;
  phase_err /= 2;
}

static inline gr_complex pow4(gr_complex c) {
  c = c * c;
  return c * c;
}

void packet_freq_corrector_impl::method_qpsk(const gr_complex *in, int dir,
                                             int len, float &phase_err,
                                             float &freq_err) {
  int pnum = len / span;
  assert(pnum > 0);

  float xavg = pnum * 0.5f;
  float xvar = xavg * (pnum * pnum - 1) / 6.0f;
  float yavg = 0.0f;
  float ycov = 0.0f;

  float y = 0.0f;
  std::complex<float> a(-1.0f, 0.0f);
  for (int i = 0; i < pnum; i++) {
    std::complex<float> b(0.0f, 0.0f);
    for (int j = 0; j < span; j++) {
      b += pow4(*in);
      in += dir;
    }

    float x = 0.5f + i;
    y += std::arg(b * std::conj(a));
    a = b;

    yavg += y;
    ycov += (x - xavg) * y;
  }
  yavg /= pnum;

  freq_err = ycov / xvar;
  phase_err = yavg - freq_err * (xavg - 0.5 / span);

  freq_err /= 4 * span;
  phase_err /= 4;
}

static inline gr_complex pow8(gr_complex c) {
  c = c * c;
  c = c * c;
  return c * c;
}

void packet_freq_corrector_impl::method_8psk(const gr_complex *in, int dir,
                                             int len, float &phase_err,
                                             float &freq_err) {
  int pnum = len / span;
  assert(pnum > 0);

  float xavg = pnum * 0.5f;
  float xvar = xavg * (pnum * pnum - 1) / 6.0f;
  float yavg = 0.0f;
  float ycov = 0.0f;

  float y = 0.0f;
  std::complex<float> a(-1.0f, 0.0f);
  for (int i = 0; i < pnum; i++) {
    std::complex<float> b(0.0f, 0.0f);
    for (int j = 0; j < span; j++) {
      b += pow8(*in);
      in += dir;
    }

    float x = 0.5f + i;
    y += std::arg(b * std::conj(a));
    a = b;

    yavg += y;
    ycov += (x - xavg) * y;
  }
  yavg /= pnum;

  freq_err = ycov / xvar;
  phase_err = yavg - freq_err * (xavg - 0.5 / span);

  freq_err /= 8 * span;
  phase_err /= 8;
}

} /* namespace marmote3 */
} /* namespace gr */
