/* -*- c++ -*- */
/*
 * Copyright 2014-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_POLYPHASE_SYNT_FILTER_CCF_IMPL_H
#define INCLUDED_MARMOTE3_POLYPHASE_SYNT_FILTER_CCF_IMPL_H

#include <marmote3/polyphase_synt_filter_ccf.h>

namespace gr {
namespace marmote3 {

class polyphase_synt_filter_ccf_impl : public polyphase_synt_filter_ccf {
private:
  const int vlen;
  const int stride;
  const int taps_len; // not counting trailing zeros
  const int work_len; // space where we add up data
  float *taps;

public:
  polyphase_synt_filter_ccf_impl(int vlen, const std::vector<float> &taps,
                                 int stride);
  ~polyphase_synt_filter_ccf_impl();

  void forecast(int noutput_items, gr_vector_int &ninput_items_required);

  int general_work(int noutput_items, gr_vector_int &ninput_items,
                   gr_vector_const_void_star &input_items,
                   gr_vector_void_star &output_items);

private:
  void method1(const gr_complex *in, gr_complex *out);
  void method2(const gr_complex *in, gr_complex *out);
  void method_vlen20(const gr_complex *in, gr_complex *out);
  void method_vlen25(const gr_complex *in, gr_complex *out);
  void method_vlen30(const gr_complex *in, gr_complex *out);
  void method_vlen40(const gr_complex *in, gr_complex *out);
  void method_vlen50(const gr_complex *in, gr_complex *out);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_POLYPHASE_SYNT_FILTER_CCF_IMPL_H */
