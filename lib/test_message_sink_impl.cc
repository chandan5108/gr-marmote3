/* -*- c++ -*- */
/*
 * Copyright 2013-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "test_message_sink_impl.h"
#include <boost/crc.hpp>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <marmote3/headers.h>
#include <marmote3/marmote3.pb.h>
#include <sstream>

namespace gr {
namespace marmote3 {

test_message_sink::sptr test_message_sink::make(int framing_type) {
  return gnuradio::get_initial_sptr(new test_message_sink_impl(framing_type));
}

test_message_sink_impl::test_message_sink_impl(int framing_type)
    : gr::block("test_message_sink", gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      framing_type(framing_type) {
  if (framing_type < 0 || framing_type > 4)
    throw std::out_of_range("test_message_sink: invalid framing type");

  pmt::pmt_t in_port = pmt::mp("in");
  message_port_register_in(in_port);
  set_msg_handler(in_port,
                  boost::bind(&test_message_sink_impl::receive, this, _1));
}

test_message_sink_impl::~test_message_sink_impl() {}

bool test_message_sink_impl::check_payload(const uint8_t *packet_ptr,
                                           int packet_len, int payload_len) {
  assert(0 <= payload_len && payload_len <= packet_len);
  int crc_len = std::min(payload_len, 2);
  payload_len -= crc_len;

  boost::crc_16_type crc16;
  crc16.process_bytes(packet_ptr + packet_len - crc_len - payload_len,
                      payload_len);
  crc16.process_bytes(packet_ptr, packet_len - crc_len - payload_len);
  uint16_t crc = crc16.checksum() ^ 0x1723;

  if (crc_len >= 1 && packet_ptr[packet_len - crc_len] != (uint8_t)crc)
    return true;
  if (crc_len == 2 && packet_ptr[packet_len - 1] != (uint8_t)(crc >> 8))
    return true;
  return false;
}

void test_message_sink_impl::receive(pmt::pmt_t msg) {
  if (!pmt::is_pair(msg) || !pmt::is_u8vector(pmt::cdr(msg))) {
    std::cout << "####### test_message_sink: malformed message received\n";
    return;
  }

  size_t len(0);
  const uint8_t *buffer = pmt::u8vector_elements(pmt::cdr(msg), len);

  bool invalid = false;
  if (framing_type == 0)
    invalid = check_payload(buffer, len, len);
  else if (framing_type == 1) {
    eth_header_t eth;
    int a = eth.eth_raw_load(buffer, len);
    if (a < 0)
      invalid = true;
    else
      invalid = check_payload(buffer, len, len - a);
  } else if (framing_type == 2) {
    eth_header_t eth;
    int a = eth.eth_raw_load(buffer, len);
    if (a < 0)
      invalid = true;
    else {
      ip4_header_t ip4;
      int b = ip4.ip4_raw_load(buffer + a, len - a);
      if (b < 0)
        invalid = true;
      else
        invalid = check_payload(buffer, len, len - a - b);
    }
  } else if (framing_type == 3) {
    eth_header_t eth;
    int a = eth.eth_raw_load(buffer, len);
    if (a < 0)
      invalid = true;
    else {
      ip4_header_t ip4;
      int b = ip4.ip4_raw_load(buffer + a, len - a);
      if (b < 0)
        invalid = true;
      else {
        udp_header_t udp;
        int c = udp.udp_raw_load(buffer + a + b, len - a - b);
        if (c < 0)
          invalid = true;
        else
          invalid = check_payload(buffer, len, len - a - b - c);
      }
    }
  } else if (framing_type == 4) {
    eth_header_t eth;
    int a = eth.eth_raw_load(buffer, len);
    if (a < 0)
      invalid = true;
    else {
      ip4_header_t ip4;
      int b = ip4.ip4_raw_load(buffer + a, len - a);
      if (b < 0)
        invalid = true;
      else {
        udp_header_t udp;
        int c = udp.udp_raw_load(buffer + a + b, len - a - b);
        if (c < 0)
          invalid = true;
        else {
          if (len - a - b - c < 53)
            invalid = true;
          else
            invalid = check_payload(buffer, len, len - a - b - c - 53);
        }
      }
    }
  }

  if (invalid) {
    std::stringstream str;
    str << alias() << ": invalid packet 0x" << std::right << std::setfill('0')
        << std::hex;
    for (int i = 0; i < std::min((int)len, 100); i++)
      str << std::setw(2) << (unsigned int)buffer[i];
    str << std::endl;
    std::cout << str.str();
  }

  gr::thread::scoped_lock lock(mutex);
  if (invalid) {
    dropped += 1;
    total_dropped += 1;
  } else {
    received += 1;
    total_received += 1;
  }
}

bool test_message_sink_impl::stop() {
  gr::thread::scoped_lock lock(mutex);
  std::stringstream str;
  str << alias() << ": total received " << total_received << " dropped "
      << total_dropped << std::endl;
  std::cout << str.str();

  return block::stop();
}

void test_message_sink_impl::collect_block_report(ModemReport *modem_report) {
  TestMessageSink *report = modem_report->mutable_test_message_sink();
  gr::thread::scoped_lock lock(mutex);

  report->set_received(received);
  report->set_dropped(dropped);
  received = 0;
  dropped = 0;
}

} /* namespace marmote3 */
} /* namespace gr */
