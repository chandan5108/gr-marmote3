/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_TEST_MESSAGE_SOURCE_IMPL_H
#define INCLUDED_MARMOTE3_TEST_MESSAGE_SOURCE_IMPL_H

#include <marmote3/test_message_source.h>
#include <pmt/pmt.h>
#include <random>

namespace gr {
namespace marmote3 {

class test_message_source_impl : public test_message_source {
private:
  const int payload_min;
  const int payload_max;
  const int framing_type;
  const int message_num;
  const int delay_first;
  const int delay_next;
  const int src_radio_id;
  const int dst_radio_id;
  const pmt::pmt_t out_port;

  unsigned int produced = 0;
  unsigned int total_produced = 0;
  gr::thread::mutex mutex;

  std::mt19937 rand_dev;
  std::uniform_int_distribution<> rand_len;
  std::uniform_int_distribution<> rand_byte;

  boost::shared_ptr<boost::thread> thread;
  void thread_work();
  void thread_stop();

  void add_payload(std::vector<uint8_t> &packet, int payload_len);

public:
  test_message_source_impl(int payload_min, int payload_max, int framing_type,
                           int message_num, int delay_first, int delay_next,
                           int src_radio_id, int dst_radio_id);
  ~test_message_source_impl();

  bool start() override;
  bool stop() override;

  void collect_block_report(ModemReport *modem_report) override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_TEST_MESSAGE_SOURCE_IMPL_H */
