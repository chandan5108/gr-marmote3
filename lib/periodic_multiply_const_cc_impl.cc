/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "periodic_multiply_const_cc_impl.h"
#include <gnuradio/io_signature.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

periodic_multiply_const_cc::sptr
periodic_multiply_const_cc::make(int vlen,
                                 const std::vector<gr_complex> &vals) {
  return gnuradio::get_initial_sptr(
      new periodic_multiply_const_cc_impl(vlen, vals));
}

periodic_multiply_const_cc_impl::periodic_multiply_const_cc_impl(
    int vlen, const std::vector<gr_complex> &vals)
    : gr::sync_block("periodic_multiply_const_cc",
                     gr::io_signature::make(1, 1, sizeof(gr_complex) * vlen),
                     gr::io_signature::make(1, 1, sizeof(gr_complex) * vlen)),
      vlen(vlen), vals(vals), pos(0) {
  if (vlen <= 0)
    throw std::invalid_argument(
        "periodic_multiply_const_cc: vector length must be positive");

  if (vals.size() <= 0)
    throw std::invalid_argument("periodic_multiply_const_cc: empty value list");
}

periodic_multiply_const_cc_impl::~periodic_multiply_const_cc_impl() {}

int periodic_multiply_const_cc_impl::work(
    int noutput_items, gr_vector_const_void_star &input_items,
    gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  if (vlen == 1) {
    int n = std::min(noutput_items, (int)(vals.size() - pos));
    volk_32fc_x2_multiply_32fc(out, in, vals.data() + pos, n);
    in += n;
    out += n;
    pos += n;
    if (pos < vals.size())
      return noutput_items;
    else
      pos = 0;

    while (n + vals.size() <= noutput_items) {
      volk_32fc_x2_multiply_32fc(out, in, vals.data(), vals.size());
      in += vals.size();
      out += vals.size();
      n += vals.size();
    }

    if (n < noutput_items) {
      pos = noutput_items - n;
      volk_32fc_x2_multiply_32fc(out, in, vals.data(), pos);
    }
  } else {
    for (int n = 0; n < noutput_items; n++) {
      volk_32fc_s32fc_multiply_32fc(out, in, vals[pos], vlen);
      in += vlen;
      out += vlen;
      if (++pos >= vals.size())
        pos = 0;
    }
  }

  return noutput_items;
}

void periodic_multiply_const_cc_impl::set_vals(
    const std::vector<gr_complex> &new_vals) {
  if (vals.size() != new_vals.size())
    std::cout
        << "periodic_multiply_const_cc: cannot change the length at runtime"
        << std::endl;
  else
    for (int i = 0; i < vals.size(); i++)
      vals[i] = new_vals[i];
}

} /* namespace marmote3 */
} /* namespace gr */
