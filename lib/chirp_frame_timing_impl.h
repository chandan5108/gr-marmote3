/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_CHIRP_FRAME_TIMING_IMPL_H
#define INCLUDED_MARMOTE3_CHIRP_FRAME_TIMING_IMPL_H

#include <marmote3/chirp_frame_timing.h>

namespace gr {
namespace marmote3 {

class chirp_frame_timing_impl : public chirp_frame_timing {
private:
  const int chirp_len;
  const int fft_len;
  const int vlen;
  const int vlen_times_fft;
  const int vlen_times_chirp;
  const int chirp_div_fft;
  const float scale;
  float noise_level;

  std::vector<int> bins;

  void *memory;
  typedef struct state_t {
    float *chirp; // fft_len * vlen
    float *noise; // vlen * (chirp_len / fft_len)
  } state_t;
  state_t state[2];
  int selector;

public:
  chirp_frame_timing_impl(int chirp_len, int fft_len, int vlen,
                          float noise_level);
  ~chirp_frame_timing_impl();

  void forecast(int noutput_items, gr_vector_int &ninput_items_required);

  virtual void set_noise_level(float noise_level);

  int general_work(int noutput_items, gr_vector_int &ninput_items,
                   gr_vector_const_void_star &input_items,
                   gr_vector_void_star &output_items);

private:
  inline void load_noise_levels(const float *in, state_t curr);
  inline void update_chirp_levels(const float *in, state_t prev, state_t curr);
  inline void write_norm_levels(float *out, state_t prev, state_t curr);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_CHIRP_FRAME_TIMING_IMPL_H */
