# Team MarmotE modem for the DARPA SC2 challenge

This is the repository used for the development of the [MarmotE](https://marmote.io/)
modem for the [DARPA SC2 challenge](https://www.spectrumcollaborationchallenge.com/).
This is a [GNU Radio](https://www.gnuradio.org/) out of tree module containing 
reusable components and the [full_modem.py](apps/full_modem.py) script that 
was used during the competition. The modem receives and delivers IPv4 packets 
through a network interface (`/dev/tap0`) and transmits and receives them 
through a USRP radio. The operation of the modem can be inspected and 
configured using a protobuf interface (or the [modem_cli.py](apps/modem_cli.py)
script. The rest of the MarmotE software stack (in particular the decision 
engine, the log analysis tools and the RFNoC components) is not open sourced.

Even though the original full modem requires the unpublished RFNoC components, 
it can be modified to run with software based components instead. We have 
created a simplified modem [simple_modem.py](apps/simple_modem.py) which can 
be run though any GNURadio supported radio. If you are interested in the RFNoC
implementation of the `gr-marmote3` components then [let us know](mailto:info@marmote.io).

## Installation

* First install GNU Radio. We have used version 3.7.13.4 in the SC2 competition,
but we have switched to the branch maint-3.7.
* Install protobuf3 compiler: 
`sudo apt-get install protobuf-compiler python3-protobuf libprotobuf-dev libzmq3-dev`.
* Build and install the `gr-marmote3` module the usual way: 
  * `mkdir build`
  * `cd build`
  * `cmake ..`
  * `make -j4`
  * `make test`
  * `sudo make install`
  * `sudo ldconfig`
* Now you can inspect and modify the modem scripts in the `apps` directory and
the various test scripts in the `examples` directory using `gnuradio-companion`.
All modem scripts were generated using the `gnuradio-companion` tool, so you 
can change and regenerate those if needed.

## Running

* To run the modem you first need to create a network interface for each radio:
  * `sudo ip tuntap add dev tap0 mode tap`
  * `sudo ip link set dev tap0 mtu 1500 address 12:34:56:78:90:xx up` where 
    `xx` is the hexadecimal value of your radio id
  * `sudo ip addr flush dev tap0`
  * `sudo ip addr add 192.168.45.xxx/24 dev tap0` where `xxx` is the radio id
* We have used static routing. For each remote radio id run the command
  * `sudo ip neigh replace 192.168.45.yyy lladdr 12:34:56:78:90:yy nud permanent dev tap0`
where `yy` and `yyy` are the radio id of the other node you want to connect.
* Run the `simple_modem.py` script on your computer for each radio, 
use `simple_modem.py --help` to see the available parameters, probably you want 
to set the `--tx-device` and `--rx-device` addresses to the same value.
If you want to run the modem twice on the same machine (connecting to different
radios), you also need to change the ZMQ report and command ports. 
* Use the `modem_cli.py read_reports` command to inspect the statistics 
produced by the modem. Use the `modem_cli.py` set commands to change the
parameters of the modem, see `modem_cli.py --help` for details.
* Use `ping`, `iperf` or the `traffic_gen.py` script to verify the connection.

You can use a single computer and a single radio to check if everything works.
The modem will overhear its own transmission, and in its output you can see 
the number of sent and received messages. A typical line in the output would 
look like this:
```
modem_monitor0: time 1571344669.000062 seq 908 load 676 sent 14 drop 674 requ 0 queue 5000 frms 19 rcvd 16 miss 63 fail 0 errs (0,2,0,0,0,0,0)
```
where `load` is the number of packets received over the `tap0` interface, 
`sent` is the number of packets sent through the radio, `drop` the number of
packets dropped because their timeout has reached, `requ` the number packets
requeued because ARQ detected that they were sent but not delivered, `queue`
the number of packets waiting to be sent, `frms` the number of frames passed
whose preamble was detected, `rcvd` the number of packets whose payload was
successfully decoded, `miss` the number of packets we are missing 
(did not receive), `fail` number of packets passed header CRC check but 
failed payload CRC check, and the various error counters. You can see in the 
output the modem creates FEC encoder and decoders on the fly for various 
payload size and MCS combinations.

## Containerized Installation

To install and run the MarmotE modem in isolation, install `lxd` (which will also
install `lxc`) and run the following bootstrap script on the host machine:

```
gr-marmote3/scripts/lxc/create_lxc_radio.sh X
```

The above will create a container named `radioX`, where `X` is the radio ID, a
positive integer. The container then can be accessed with `lxc exec radioX bash`.

Note that you should check the presence of the SDR device with `uhd_find_devices`
and attach it from the host if necessary before following the above instructions
with `simple_modem.py`.


## Channels

The modem uses FBMC to divide up the sampling bandwidth into subcarriers of 
equal width. Out of all subcarriers only 4/5 are usable channels. For example,
if you select 16 channels, then there will be 20 subcarriers. If the sampling
rate is 2 MHz, then each subcarrier/channel will be 0.1 MHz wide. The 
following table summarizes the numbering of channels and subcarriers:

| Subcarrier | Channel | Freq Range (MHz) |
|------------|---------|------------------|
| 10         | N/A     | [-1.0, -0.9]     |
| 11         | N/A     | [-0.9, -0.8]     |
| 12         | 0       | [-0.8, -0.7]     |
| 13         | 1       | [-0.7, -0.6]     |
| ...        | ...     | ...              |
| 19         | 7       | [-0.1, +0.0]     |
| 0          | 8       | [+0.0, +0.1]     |
| 1          | 9       | [+0.1, +0.2]     |
| ...        | ...     | ...              |
| 7          | 15      | [+0.7, +0.8]     |
| 8          | N/A     | [+0.8, +0.9]     |
| 9          | N/A     | [+0.9, +1.0]     |

Once the modem is started the number of channels and subcarriers cannot be
changed, but the center frequency and sampling rate (rf_bandwidth) can.
The modem is started with two randomly chosen channels. You can change that
with the `modem_cli.py set_active_channels` command. Not all choices of
number of channels have optimized implementations (use 16, 20, 24, 32 or 40).

## Flows

The modem prioritizes traffic based on flows. Each flow is identified by its
target UDP or TCP port. For IP traffic that has no associated port (e.g. ICMP)
the flow number 0 is used. You can change mandate parameters (e.g. max latency,
maximum throughput bps and discrete/continuos mandate) for each flow using
the `modem_cli.py set_mandates` command. The target throughput is the maximum
throughput that the modem tries to achieve (it will not send more even if data
is available).

## Radio IDs

The network can have at most 253 radios, identified by the last octet in the
IP address of its network interface. The radio will learn this value by 
observing the packets it tries to send to others, but it can also be specified
on the command line. For the SC2 competition this number was `100 + srn_id`.
A message is broadcast message if its target radio id is `255`.

## ARQ

The modem can send src/dst sequence numbers to detect lost messages. This 
feature is not enabled by default, you have to run the command 
`modem_cli.py set_enable_arq 1` to enable it on all nodes.

# MCS

There are 10 modulation and coding schemes that can be chosen for each 
destination radio. You have to choose the right MCS value; otherwise your packets
might not get through. Broadcast messages are always sent using MCS 0. 
There are some factors that slightly adjust (increases at most by 2) the 
selected MCS depending on ARQ performance and traffic load (see the modem
sender for details). All payload data is coded with a special twisted version
of a repeat accumulate code. The packet headers are always coded with a 
convolutional code. 

| MCS Index | Modulation | Coding Rate | Power (dB) | 
|-----------|------------|-------------|------------|
| 0         | BPSK       | 0.5         | 0.0        |
| 1         | BPSK       | 0.625       | 0.0        |
| 2         | BPSK       | 0.8         | 0.0        |
| 3         | QPSK       | 0.625       | 0.0        |
| 4         | QPSK       | 0.8         | 0.0        |
| 5         | 16QAM      | 0.5         | 0.0        |
| 6         | 16QAM      | 0.625       | 0.0        |
| 7         | 16QAM      | 0.75        | 0.0        |
| 8         | 16QAM      | 0.875       | 0.0        |
| 9         | 64QAM      | 0.666       | 0.0        |
| 10        | 64QAM      | 0.75        | 0.0        |
| 11        | 64QAM      | 0.75        | -2.0       |

Use the `modem_cli.py set_mcs_assignment` to change the MCS for different
destinations.
