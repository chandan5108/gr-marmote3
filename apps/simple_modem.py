#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Simple Modem
# GNU Radio version: 3.8.2.0

from gnuradio import blocks
from gnuradio import fft
from gnuradio.fft import window
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time
import marmote3
import math
import numpy as np
import random


class simple_modem(gr.top_block):

    def __init__(self, center_freq=1e9, num_channels=16, radio_id=-1, rx_device="", rx_gain=20, samp_rate=2000000, tap_device="tap0", tx_device="", tx_gain=10, tx_power=0, zmq_command_port=7555, zmq_report_port=7557):
        gr.top_block.__init__(self, "Simple Modem")

        ##################################################
        # Parameters
        ##################################################
        self.center_freq = center_freq
        self.num_channels = num_channels
        self.radio_id = radio_id
        self.rx_device = rx_device
        self.rx_gain = rx_gain
        self.samp_rate = samp_rate
        self.tap_device = tap_device
        self.tx_device = tx_device
        self.tx_gain = tx_gain
        self.tx_power = tx_power
        self.zmq_command_port = zmq_command_port
        self.zmq_report_port = zmq_report_port

        ##################################################
        # Variables
        ##################################################
        self.max_data_len = max_data_len = 1506
        self.max_code_len = max_code_len = 2 * max_data_len
        self.padding_len = padding_len = 8
        self.max_symb_len = max_symb_len = 8 * max_code_len
        self.fbmc_fft_len = fbmc_fft_len = int(num_channels * 5 // 4)
        self.chirp_len = chirp_len = 128
        self.subcarrier_map = subcarrier_map = list(range(fbmc_fft_len-num_channels//2, fbmc_fft_len)) + list(range(0, num_channels//2))
        self.max_frame_len = max_frame_len = 40 + padding_len + chirp_len + padding_len + max_symb_len
        self.fbmc_tx_stride = fbmc_tx_stride = {16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[num_channels]
        self.chirp = chirp = marmote3.get_chirp_taps(chirp_len)
        self.modem_monitor = marmote3.modem_monitor(1,
            "tcp://127.0.0.1:" + str(zmq_report_port), 2, "tcp://127.0.0.1:" + str(zmq_command_port))
        self.subcarrier_inv = subcarrier_inv = [subcarrier_map.index(x) if x in subcarrier_map else -1 for x in range(fbmc_fft_len)]
        self.postamble = postamble = True
        self.max_frame_time = max_frame_time = float(max_frame_len) / (samp_rate / fbmc_tx_stride)
        self.fbmc_tx_taps = fbmc_tx_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "tx")
        self.fbmc_rx_taps = fbmc_rx_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "rx")
        self.fbmc_rx_stride = fbmc_rx_stride = fbmc_tx_stride//2
        self.dc_chirp = dc_chirp = chirp + np.ones(chirp_len) * 0.5
        self.chirp_fft_len = chirp_fft_len = 64
        self.active_channels = active_channels = random.sample(range(num_channels), 2)

        ##################################################
        # Blocks
        ##################################################
        self.z1_modem_sender = marmote3.modem_sender2(max_data_len, max_code_len, max_symb_len, 5000, 0, 4.0, 5e6, False, 4, radio_id, [], 2, False, False)
        self.z1_modem_sender.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.vector_peak_probe_synt = marmote3.vector_peak_probe(fbmc_fft_len, 2, 15, -30.0, 2.0)
        self.vector_peak_probe_synt.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.vector_peak_probe_chan = marmote3.vector_peak_probe(fbmc_fft_len, 1, 30, -60.0, 2.0)
        self.vector_peak_probe_chan.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        _unused = self.msg_connect((self.modem_monitor, 'unused'),
            (self.modem_monitor, 'unused'))
        self.synt_subcarrier_remapper = marmote3.subcarrier_remapper(num_channels, subcarrier_inv, 32)
        self.synt_polyphase_synt_filter = marmote3.polyphase_synt_filter_ccf(fbmc_fft_len, fbmc_tx_taps, fbmc_tx_stride or fbmc_fft_len)
        self.synt_polyphase_synt_filter.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.synt_polyphase_rotator = marmote3.polyphase_rotator_vcc(fbmc_fft_len, subcarrier_map, fbmc_tx_stride)
        self.synt_periodic_multiply_const = marmote3.periodic_multiply_const_cc(1, np.exp(1.0j * np.pi / fbmc_fft_len * np.arange(0, 2 * fbmc_fft_len)))
        self.synt_fft = fft.fft_vcc(fbmc_fft_len, False, window.rectangular(fbmc_fft_len), False, 1)
        self.subcarrier_serializer2 = marmote3.subcarrier_serializer2(num_channels, 2 * chirp_len // chirp_fft_len, 12.0, 2 * (padding_len + 40 + padding_len + chirp_len) + 3, 2 * (padding_len + 40 + padding_len) + 1, postamble, 2 * (padding_len + 40 + padding_len + chirp_len + padding_len), 2 * (padding_len + max_frame_len + padding_len), list(range(num_channels)))
        self.subcarrier_serializer2.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.subcarrier_allocator = marmote3.subcarrier_allocator(num_channels, active_channels, [], 3, getattr(self, '') if '' else marmote3.vector_peak_probe_sptr(), subcarrier_map, getattr(self, 'z1_modem_sender') if 'z1_modem_sender' else marmote3.modem_sender2_sptr(), False)
        self.subcarrier_allocator.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.output_usrp_sink = uhd.usrp_sink(
            ",".join(("addr=" + tx_device if tx_device else "", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
            '',
        )
        self.output_usrp_sink.set_center_freq(center_freq, 0)
        self.output_usrp_sink.set_gain(tx_gain, 0)
        self.output_usrp_sink.set_antenna('TX/RX', 0)
        self.output_usrp_sink.set_samp_rate(samp_rate)
        self.output_usrp_sink.set_time_unknown_pps(uhd.time_spec())
        self.modem_receiver = marmote3.modem_receiver(4, getattr(self, 'z1_modem_sender') if 'z1_modem_sender' else marmote3.modem_sender2_sptr(), False)
        self.modem_receiver.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_control_0 = marmote3.modem_control('center_freq', 'samp_rate', 'rx_gain', 'tx_gain', 'tx_power', 'none', 'none')
        if 'center_freq' != 'none':
            self.marmote3_modem_control_0.set_center_freq(self.center_freq)
        if 'samp_rate' != 'none':
            self.marmote3_modem_control_0.set_samp_rate(self.samp_rate)
        if 'rx_gain' != 'none':
            self.marmote3_modem_control_0.set_rx_gain(self.rx_gain)
        if 'tx_gain' != 'none':
            self.marmote3_modem_control_0.set_tx_gain(self.tx_gain)
        if 'tx_power' != 'none':
            self.marmote3_modem_control_0.set_tx_power(self.tx_power)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_invert_spectrum(self.none)
        if 'none' != 'none':
            self.marmote3_modem_control_0.set_enable_recording(self.none)
        if 'num_channels' != 'none':
            self.marmote3_modem_control_0.set_num_channels(self.num_channels)
        if 'fbmc_fft_len' != 'none':
            self.marmote3_modem_control_0.set_num_subcarriers(self.fbmc_fft_len)
        self._modem_setter = marmote3.ModemSetter(self)
        self.marmote3_modem_control_0.set_modem_setter(self._modem_setter)
        self.marmote3_modem_control_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.input_usrp_source = uhd.usrp_source(
            ",".join(("addr=" + rx_device if rx_device else "", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=list(range(0,1)),
            ),
        )
        self.input_usrp_source.set_center_freq(center_freq, 0)
        self.input_usrp_source.set_gain(rx_gain, 0)
        self.input_usrp_source.set_antenna('RX2', 0)
        self.input_usrp_source.set_samp_rate(samp_rate)
        self.input_usrp_source.set_time_unknown_pps(uhd.time_spec())
        self.header_decompressor = marmote3.header_decompressor(4, True)
        self.header_decompressor.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.header_compressor = marmote3.header_compressor(4, True)
        self.header_compressor.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.fixed_tuntap_pdu = marmote3.fixed_tuntap_pdu(tap_device, 1500, False)
        self.encode_symb_mod = marmote3.packet_symb_mod(marmote3.MOD_MCS or 1, max_symb_len)
        self.encode_ra_encoder = marmote3.packet_ra_encoder(-1 if True else max_data_len, max_code_len)
        self.encode_preamb_insert = marmote3.packet_preamb_insert(np.concatenate([dc_chirp[-padding_len:],dc_chirp,dc_chirp[:padding_len]]), True, tx_power, marmote3.HDR_SRC_MCS_LEN_CRC, postamble, max_frame_len)
        self.decode_symb_demod_0 = marmote3.packet_symb_demod(marmote3.MOD_MCS or 1, 36.0, max_code_len * 8)
        self.decode_ra_decoder_0 = marmote3.packet_ra_decoder(max_data_len, -1 if True else 0, 1, 15)
        self.decode_preamb_freqcorr_0 = marmote3.packet_preamb_freqcorr2(2 * (padding_len + 40 + padding_len), 2 * chirp_len, postamble, 16, 0.25, 2 * (padding_len + max_frame_len + padding_len))
        self.decode_preamb_equalizer_0 = marmote3.packet_preamb_equalizer(marmote3.get_expected_signal(dc_chirp, 41), 2, padding_len, marmote3.HDR_SRC_MCS_LEN_CRC, postamble, max_symb_len, 0.2, False, False)
        self.decode_preamb_equalizer_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.decode_freq_corrector_0 = marmote3.packet_freq_corrector(marmote3.MOD_MCS or 1, 64, postamble, max_symb_len)
        self.decode_ampl_corrector_0 = marmote3.packet_ampl_corrector(512, max_symb_len)
        self.dechirp_transpose_1 = marmote3.transpose_vxx(gr.sizeof_float, chirp_fft_len, num_channels)
        self.dechirp_transpose_0 = marmote3.transpose_vxx(gr.sizeof_gr_complex, num_channels, chirp_fft_len)
        self.dechirp_periodic_multiply_const = marmote3.periodic_multiply_const_cc(num_channels, np.conj(chirp))
        self.dechirp_keep_one_in_n = blocks.keep_one_in_n(gr.sizeof_gr_complex*num_channels, 2)
        self.dechirp_fft = fft.fft_vcc(chirp_fft_len, True, window.rectangular(chirp_fft_len), False, 1)
        self.dechirp_complex_to_mag = blocks.complex_to_mag_squared(chirp_fft_len)
        self.dechirp_chirp_frame_timing = marmote3.chirp_frame_timing(chirp_len, chirp_fft_len, num_channels, 0.0)
        self.dechirp_block_sum = marmote3.block_sum_vxx(1 * num_channels, 1, 2)
        self.chan_subcarrier_remapper = marmote3.subcarrier_remapper(fbmc_fft_len, subcarrier_map, 0)
        self.chan_polyphase_rotator = marmote3.polyphase_rotator_vcc(fbmc_fft_len, subcarrier_map, -fbmc_rx_stride)
        self.chan_polyphase_chan_filter = marmote3.polyphase_chan_filter_ccf(fbmc_fft_len, fbmc_rx_taps, fbmc_rx_stride or fbmc_fft_len)
        self.chan_polyphase_chan_filter.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.chan_periodic_multiply_const = marmote3.periodic_multiply_const_cc(1, np.exp(-1.0j * np.pi / fbmc_fft_len * np.arange(0, 2 * fbmc_fft_len)))
        self.chan_fft = fft.fft_vcc(fbmc_fft_len, True, window.rectangular(fbmc_fft_len), False, 1)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.fixed_tuntap_pdu, 'out'), (self.header_compressor, 'in'))
        self.msg_connect((self.header_compressor, 'out'), (self.z1_modem_sender, 'in'))
        self.msg_connect((self.header_decompressor, 'out'), (self.fixed_tuntap_pdu, 'in'))
        self.msg_connect((self.modem_receiver, 'out'), (self.header_decompressor, 'in'))
        self.connect((self.chan_fft, 0), (self.chan_subcarrier_remapper, 0))
        self.connect((self.chan_fft, 0), (self.vector_peak_probe_chan, 0))
        self.connect((self.chan_periodic_multiply_const, 0), (self.chan_polyphase_chan_filter, 0))
        self.connect((self.chan_polyphase_chan_filter, 0), (self.chan_fft, 0))
        self.connect((self.chan_polyphase_rotator, 0), (self.dechirp_keep_one_in_n, 0))
        self.connect((self.chan_polyphase_rotator, 0), (self.subcarrier_serializer2, 0))
        self.connect((self.chan_subcarrier_remapper, 0), (self.chan_polyphase_rotator, 0))
        self.connect((self.dechirp_block_sum, 0), (self.subcarrier_serializer2, 1))
        self.connect((self.dechirp_chirp_frame_timing, 0), (self.dechirp_transpose_1, 0))
        self.connect((self.dechirp_complex_to_mag, 0), (self.dechirp_chirp_frame_timing, 0))
        self.connect((self.dechirp_fft, 0), (self.dechirp_complex_to_mag, 0))
        self.connect((self.dechirp_keep_one_in_n, 0), (self.dechirp_periodic_multiply_const, 0))
        self.connect((self.dechirp_periodic_multiply_const, 0), (self.dechirp_transpose_0, 0))
        self.connect((self.dechirp_transpose_0, 0), (self.dechirp_fft, 0))
        self.connect((self.dechirp_transpose_1, 0), (self.dechirp_block_sum, 0))
        self.connect((self.decode_ampl_corrector_0, 0), (self.decode_freq_corrector_0, 0))
        self.connect((self.decode_freq_corrector_0, 0), (self.decode_symb_demod_0, 0))
        self.connect((self.decode_preamb_equalizer_0, 0), (self.decode_ampl_corrector_0, 0))
        self.connect((self.decode_preamb_freqcorr_0, 0), (self.decode_preamb_equalizer_0, 0))
        self.connect((self.decode_ra_decoder_0, 0), (self.modem_receiver, 0))
        self.connect((self.decode_symb_demod_0, 0), (self.decode_ra_decoder_0, 0))
        self.connect((self.encode_preamb_insert, 0), (self.subcarrier_allocator, 0))
        self.connect((self.encode_ra_encoder, 0), (self.encode_symb_mod, 0))
        self.connect((self.encode_symb_mod, 0), (self.encode_preamb_insert, 0))
        self.connect((self.input_usrp_source, 0), (self.chan_periodic_multiply_const, 0))
        self.connect((self.subcarrier_allocator, 0), (self.synt_polyphase_rotator, 0))
        self.connect((self.subcarrier_serializer2, 0), (self.decode_preamb_freqcorr_0, 0))
        self.connect((self.synt_fft, 0), (self.synt_polyphase_synt_filter, 0))
        self.connect((self.synt_periodic_multiply_const, 0), (self.output_usrp_sink, 0))
        self.connect((self.synt_polyphase_rotator, 0), (self.synt_subcarrier_remapper, 0))
        self.connect((self.synt_polyphase_synt_filter, 0), (self.synt_periodic_multiply_const, 0))
        self.connect((self.synt_subcarrier_remapper, 0), (self.synt_fft, 0))
        self.connect((self.synt_subcarrier_remapper, 0), (self.vector_peak_probe_synt, 0))
        self.connect((self.z1_modem_sender, 0), (self.encode_ra_encoder, 0))


    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self.input_usrp_source.set_center_freq(self.center_freq, 0)
        self.marmote3_modem_control_0.set_center_freq(self.center_freq)
        self.output_usrp_sink.set_center_freq(self.center_freq, 0)

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels
        self.set_active_channels(random.sample(range(self.num_channels), 2))
        self.set_fbmc_fft_len(int(self.num_channels * 5 // 4))
        self.set_fbmc_tx_stride({16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[self.num_channels])
        self.set_subcarrier_map(list(range(self.fbmc_fft_len-self.num_channels//2, self.fbmc_fft_len)) + list(range(0, self.num_channels//2)))
        self.marmote3_modem_control_0.set_num_channels(self.num_channels)
        self.subcarrier_serializer2.set_enabled(list(range(self.num_channels)))

    def get_radio_id(self):
        return self.radio_id

    def set_radio_id(self, radio_id):
        self.radio_id = radio_id

    def get_rx_device(self):
        return self.rx_device

    def set_rx_device(self, rx_device):
        self.rx_device = rx_device

    def get_rx_gain(self):
        return self.rx_gain

    def set_rx_gain(self, rx_gain):
        self.rx_gain = rx_gain
        self.input_usrp_source.set_gain(self.rx_gain, 0)
        self.marmote3_modem_control_0.set_rx_gain(self.rx_gain)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_max_frame_time(float(self.max_frame_len) / (self.samp_rate / self.fbmc_tx_stride))
        self.input_usrp_source.set_samp_rate(self.samp_rate)
        self.marmote3_modem_control_0.set_samp_rate(self.samp_rate)
        self.output_usrp_sink.set_samp_rate(self.samp_rate)

    def get_tap_device(self):
        return self.tap_device

    def set_tap_device(self, tap_device):
        self.tap_device = tap_device

    def get_tx_device(self):
        return self.tx_device

    def set_tx_device(self, tx_device):
        self.tx_device = tx_device

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        self.marmote3_modem_control_0.set_tx_gain(self.tx_gain)
        self.output_usrp_sink.set_gain(self.tx_gain, 0)

    def get_tx_power(self):
        return self.tx_power

    def set_tx_power(self, tx_power):
        self.tx_power = tx_power
        self.encode_preamb_insert.set_power(self.tx_power)
        self.marmote3_modem_control_0.set_tx_power(self.tx_power)

    def get_zmq_command_port(self):
        return self.zmq_command_port

    def set_zmq_command_port(self, zmq_command_port):
        self.zmq_command_port = zmq_command_port

    def get_zmq_report_port(self):
        return self.zmq_report_port

    def set_zmq_report_port(self, zmq_report_port):
        self.zmq_report_port = zmq_report_port

    def get_max_data_len(self):
        return self.max_data_len

    def set_max_data_len(self, max_data_len):
        self.max_data_len = max_data_len
        self.set_max_code_len(2 * self.max_data_len)

    def get_max_code_len(self):
        return self.max_code_len

    def set_max_code_len(self, max_code_len):
        self.max_code_len = max_code_len
        self.set_max_symb_len(8 * self.max_code_len)

    def get_padding_len(self):
        return self.padding_len

    def set_padding_len(self, padding_len):
        self.padding_len = padding_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)

    def get_max_symb_len(self):
        return self.max_symb_len

    def set_max_symb_len(self, max_symb_len):
        self.max_symb_len = max_symb_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)

    def get_fbmc_fft_len(self):
        return self.fbmc_fft_len

    def set_fbmc_fft_len(self, fbmc_fft_len):
        self.fbmc_fft_len = fbmc_fft_len
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])
        self.set_subcarrier_map(list(range(self.fbmc_fft_len-self.num_channels//2, self.fbmc_fft_len)) + list(range(0, self.num_channels//2)))
        self.chan_periodic_multiply_const.set_vals(np.exp(-1.0j * np.pi / self.fbmc_fft_len * np.arange(0, 2 * self.fbmc_fft_len)))
        self.marmote3_modem_control_0.set_num_subcarriers(self.fbmc_fft_len)
        self.synt_periodic_multiply_const.set_vals(np.exp(1.0j * np.pi / self.fbmc_fft_len * np.arange(0, 2 * self.fbmc_fft_len)))

    def get_chirp_len(self):
        return self.chirp_len

    def set_chirp_len(self, chirp_len):
        self.chirp_len = chirp_len
        self.set_chirp(marmote3.get_chirp_taps(self.chirp_len))
        self.set_dc_chirp(self.chirp + np.ones(self.chirp_len) * 0.5)
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)

    def get_subcarrier_map(self):
        return self.subcarrier_map

    def set_subcarrier_map(self, subcarrier_map):
        self.subcarrier_map = subcarrier_map
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])

    def get_max_frame_len(self):
        return self.max_frame_len

    def set_max_frame_len(self, max_frame_len):
        self.max_frame_len = max_frame_len
        self.set_max_frame_time(float(self.max_frame_len) / (self.samp_rate / self.fbmc_tx_stride))

    def get_fbmc_tx_stride(self):
        return self.fbmc_tx_stride

    def set_fbmc_tx_stride(self, fbmc_tx_stride):
        self.fbmc_tx_stride = fbmc_tx_stride
        self.set_fbmc_rx_stride(self.fbmc_tx_stride//2)
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.set_max_frame_time(float(self.max_frame_len) / (self.samp_rate / self.fbmc_tx_stride))

    def get_chirp(self):
        return self.chirp

    def set_chirp(self, chirp):
        self.chirp = chirp
        self.set_dc_chirp(self.chirp + np.ones(self.chirp_len) * 0.5)
        self.dechirp_periodic_multiply_const.set_vals(np.conj(self.chirp))

    def get_variable_marmote3_modem_monitor_0(self):
        return self.variable_marmote3_modem_monitor_0

    def set_variable_marmote3_modem_monitor_0(self, variable_marmote3_modem_monitor_0):
        self.variable_marmote3_modem_monitor_0 = variable_marmote3_modem_monitor_0

    def get_subcarrier_inv(self):
        return self.subcarrier_inv

    def set_subcarrier_inv(self, subcarrier_inv):
        self.subcarrier_inv = subcarrier_inv

    def get_postamble(self):
        return self.postamble

    def set_postamble(self, postamble):
        self.postamble = postamble

    def get_max_frame_time(self):
        return self.max_frame_time

    def set_max_frame_time(self, max_frame_time):
        self.max_frame_time = max_frame_time

    def get_fbmc_tx_taps(self):
        return self.fbmc_tx_taps

    def set_fbmc_tx_taps(self, fbmc_tx_taps):
        self.fbmc_tx_taps = fbmc_tx_taps

    def get_fbmc_rx_taps(self):
        return self.fbmc_rx_taps

    def set_fbmc_rx_taps(self, fbmc_rx_taps):
        self.fbmc_rx_taps = fbmc_rx_taps

    def get_fbmc_rx_stride(self):
        return self.fbmc_rx_stride

    def set_fbmc_rx_stride(self, fbmc_rx_stride):
        self.fbmc_rx_stride = fbmc_rx_stride

    def get_dc_chirp(self):
        return self.dc_chirp

    def set_dc_chirp(self, dc_chirp):
        self.dc_chirp = dc_chirp

    def get_chirp_fft_len(self):
        return self.chirp_fft_len

    def set_chirp_fft_len(self, chirp_fft_len):
        self.chirp_fft_len = chirp_fft_len

    def get_active_channels(self):
        return self.active_channels

    def set_active_channels(self, active_channels):
        self.active_channels = active_channels
        self.subcarrier_allocator.set_active_channels(self.active_channels)




def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-f", "--center-freq", dest="center_freq", type=eng_float, default="1.0G",
        help="Set frequency [default=%(default)r]")
    parser.add_argument(
        "-n", "--num-channels", dest="num_channels", type=intx, default=16,
        help="Set number of channels (must be multiple of 4) [default=%(default)r]")
    parser.add_argument(
        "--radio-id", dest="radio_id", type=intx, default=-1,
        help="Set radio id [default=%(default)r]")
    parser.add_argument(
        "--rx-gain", dest="rx_gain", type=eng_float, default="20.0",
        help="Set RX gain [default=%(default)r]")
    parser.add_argument(
        "-r", "--samp-rate", dest="samp_rate", type=intx, default=2000000,
        help="Set sample rate [default=%(default)r]")
    parser.add_argument(
        "--tx-gain", dest="tx_gain", type=eng_float, default="10.0",
        help="Set TX gain [default=%(default)r]")
    parser.add_argument(
        "--tx-power", dest="tx_power", type=eng_float, default="0.0",
        help="Set TX power in dB [default=%(default)r]")
    parser.add_argument(
        "--zmq-command-port", dest="zmq_command_port", type=intx, default=7555,
        help="Set ZMQ command port [default=%(default)r]")
    parser.add_argument(
        "--zmq-report-port", dest="zmq_report_port", type=intx, default=7557,
        help="Set ZMQ report port [default=%(default)r]")
    return parser


def main(top_block_cls=simple_modem, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(center_freq=options.center_freq, num_channels=options.num_channels, radio_id=options.radio_id, rx_gain=options.rx_gain, samp_rate=options.samp_rate, tx_gain=options.tx_gain, tx_power=options.tx_power, zmq_command_port=options.zmq_command_port, zmq_report_port=options.zmq_report_port)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()

        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()

    tb.wait()


if __name__ == '__main__':
    main()
