#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Tx Noise
# Generated: Thu Sep  7 18:41:38 2017
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import sip
import sys
import time
from gnuradio import qtgui


class tx_noise(gr.top_block, Qt.QWidget):

    def __init__(self, device='192.168.40.2', freq=1e9, gui_update=0.1, samp_rate=25000000, subdev="A:0"):
        gr.top_block.__init__(self, "Tx Noise")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Tx Noise")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "tx_noise")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Parameters
        ##################################################
        self.device = device
        self.freq = freq
        self.gui_update = gui_update
        self.samp_rate = samp_rate
        self.subdev = subdev

        ##################################################
        # Variables
        ##################################################
        self.width = width = 0.1
        self.gain = gain = 16
        self.center = center = 0
        self.ampl = ampl = 10

        ##################################################
        # Blocks
        ##################################################
        self._width_range = Range(0.1, 1, 0.01, 0.1, 200)
        self._width_win = RangeWidget(self._width_range, self.set_width, 'width', "counter_slider", float)
        self.top_grid_layout.addWidget(self._width_win, 3,0,1,1)
        self._gain_range = Range(0, 32, 1, 16, 200)
        self._gain_win = RangeWidget(self._gain_range, self.set_gain, 'gain', "counter_slider", int)
        self.top_grid_layout.addWidget(self._gain_win, 0,0,1,1)
        self._center_range = Range(-0.5, 0.5, 0.01, 0, 200)
        self._center_win = RangeWidget(self._center_range, self.set_center, 'center', "counter_slider", float)
        self.top_grid_layout.addWidget(self._center_win, 2,0,1,1)
        self._ampl_range = Range(-50, 50, 1, 10, 200)
        self._ampl_win = RangeWidget(self._ampl_range, self.set_ampl, 'amplitude in db', "counter_slider", int)
        self.top_grid_layout.addWidget(self._ampl_win, 1,0,1,1)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join(("addr=" + device, "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_subdev_spec(subdev, 0)
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_center_freq(freq, 0)
        self.uhd_usrp_sink_0.set_gain(gain, 0)
        self.uhd_usrp_sink_0.set_antenna('TX/RX', 0)
        self.qtgui_number_sink_0 = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_HORIZ,
            1
        )
        self.qtgui_number_sink_0.set_update_time(gui_update)
        self.qtgui_number_sink_0.set_title("")

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        units = ['', '', '', '', '',
                 '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
                  ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        for i in xrange(1):
            self.qtgui_number_sink_0.set_min(i, 0)
            self.qtgui_number_sink_0.set_max(i, 2)
            self.qtgui_number_sink_0.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0.set_label(i, labels[i])
            self.qtgui_number_sink_0.set_unit(i, units[i])
            self.qtgui_number_sink_0.set_factor(i, factor[i])

        self.qtgui_number_sink_0.enable_autoscale(False)
        self._qtgui_number_sink_0_win = sip.wrapinstance(self.qtgui_number_sink_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_number_sink_0_win)
        self.qtgui_freq_sink_x_0 = qtgui.freq_sink_c(
        	1024, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	freq, #fc
        	samp_rate, #bw
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_freq_sink_x_0.set_update_time(gui_update)
        self.qtgui_freq_sink_x_0.set_y_axis(-100, 0)
        self.qtgui_freq_sink_x_0.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0.enable_grid(False)
        self.qtgui_freq_sink_x_0.set_fft_average(0.05)
        self.qtgui_freq_sink_x_0.enable_axis_labels(True)
        self.qtgui_freq_sink_x_0.enable_control_panel(False)

        if not True:
          self.qtgui_freq_sink_x_0.disable_legend()

        if "complex" == "float" or "complex" == "msg_float":
          self.qtgui_freq_sink_x_0.set_plot_pos_half(not True)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_freq_sink_x_0_win, 4,0,1,1)
        self.blocks_rms_xx_0 = blocks.rms_cf(0.00001)
        self.band_pass_filter_0 = filter.fir_filter_ccc(1, firdes.complex_band_pass(
        	1, samp_rate, max(-0.5, center - 0.5 * width) * samp_rate, min(0.5, center + 0.5 * width) * samp_rate, samp_rate/100, firdes.WIN_HAMMING, 6.76))
        self.analog_fastnoise_source_x_0 = analog.fastnoise_source_c(analog.GR_GAUSSIAN, 10 ** (0.05 * ampl), 0, 8192)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_fastnoise_source_x_0, 0), (self.band_pass_filter_0, 0))
        self.connect((self.band_pass_filter_0, 0), (self.blocks_rms_xx_0, 0))
        self.connect((self.band_pass_filter_0, 0), (self.qtgui_freq_sink_x_0, 0))
        self.connect((self.band_pass_filter_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.blocks_rms_xx_0, 0), (self.qtgui_number_sink_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "tx_noise")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_device(self):
        return self.device

    def set_device(self, device):
        self.device = device

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.uhd_usrp_sink_0.set_center_freq(self.freq, 0)
        self.qtgui_freq_sink_x_0.set_frequency_range(self.freq, self.samp_rate)

    def get_gui_update(self):
        return self.gui_update

    def set_gui_update(self, gui_update):
        self.gui_update = gui_update
        self.qtgui_number_sink_0.set_update_time(self.gui_update)
        self.qtgui_freq_sink_x_0.set_update_time(self.gui_update)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.qtgui_freq_sink_x_0.set_frequency_range(self.freq, self.samp_rate)
        self.band_pass_filter_0.set_taps(firdes.complex_band_pass(1, self.samp_rate, max(-0.5, self.center - 0.5 * self.width) * self.samp_rate, min(0.5, self.center + 0.5 * self.width) * self.samp_rate, self.samp_rate/100, firdes.WIN_HAMMING, 6.76))

    def get_subdev(self):
        return self.subdev

    def set_subdev(self, subdev):
        self.subdev = subdev

    def get_width(self):
        return self.width

    def set_width(self, width):
        self.width = width
        self.band_pass_filter_0.set_taps(firdes.complex_band_pass(1, self.samp_rate, max(-0.5, self.center - 0.5 * self.width) * self.samp_rate, min(0.5, self.center + 0.5 * self.width) * self.samp_rate, self.samp_rate/100, firdes.WIN_HAMMING, 6.76))

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_sink_0.set_gain(self.gain, 0)


    def get_center(self):
        return self.center

    def set_center(self, center):
        self.center = center
        self.band_pass_filter_0.set_taps(firdes.complex_band_pass(1, self.samp_rate, max(-0.5, self.center - 0.5 * self.width) * self.samp_rate, min(0.5, self.center + 0.5 * self.width) * self.samp_rate, self.samp_rate/100, firdes.WIN_HAMMING, 6.76))

    def get_ampl(self):
        return self.ampl

    def set_ampl(self, ampl):
        self.ampl = ampl
        self.analog_fastnoise_source_x_0.set_amplitude(10 ** (0.05 * self.ampl))


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "-d", "--device", dest="device", type="string", default='192.168.40.2',
        help="Set device [default=%default]")
    parser.add_option(
        "-f", "--freq", dest="freq", type="eng_float", default=eng_notation.num_to_str(1e9),
        help="Set frequency [default=%default]")
    parser.add_option(
        "-u", "--gui-update", dest="gui_update", type="eng_float", default=eng_notation.num_to_str(0.1),
        help="Set gui update [default=%default]")
    parser.add_option(
        "-r", "--samp-rate", dest="samp_rate", type="long", default=25000000,
        help="Set sample rate [default=%default]")
    parser.add_option(
        "-s", "--subdev", dest="subdev", type="string", default="A:0",
        help="Set subdev [default=%default]")
    return parser


def main(top_block_cls=tx_noise, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(device=options.device, freq=options.freq, gui_update=options.gui_update, samp_rate=options.samp_rate, subdev=options.subdev)
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
